<?php 
	session_start();
	error_reporting(0);
    require 'system/_core/csrf.php';
    $login = false;
    if(isset($_SESSION['limit'])&& isset($_SESSION['user_id'])){
        $usernama = $_SESSION['nama'];
        $nama = 'Hai, '.$_SESSION['nama'];
        $login = true;
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TAJAKO | FAQ</title>
    <script src="resource/js/head-faq.js"></script>
    <link href="resource/css/sweetalert.css" rel="stylesheet">

    <style>
        .cm{
            font-size:16px;
        }
        .cm_row{
            font-size:large;
        }
        .fans{
            margin-top:0px;
            padding: 5px;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>FAQ | <?php echo $nama?></h2>
                    <input type="hidden" id="usernama" value="<?php echo $usernama?>">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">
                            <strong>Frequently Asked Questions</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                    <?php 
                        if($login){
                            echo '<form action="system/_core/logout.php" method="post" style="width: auto;">
                                    <input type="hidden" id="logout" value="logout" name="logout">
                                    <button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-user-times"></i> Logout</button>';                                                     
                            print CSRF::tokenInput();
                            echo  '</form>';
                        }else{
                            echo '<a href="" id="login_user" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".login" data-backdrop="static" data-keyboard="false"><i class="fas fa-user-plus"></i> Login</a>';
                        }
                    ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content animated fadeInRight">

                        <div class="ibox-content m-b-sm border-bottom">
                            <div class="text-center p-lg">
                                <h2>If you don't find the answer to your question</h2>
                                <?php 
                                    if($login){
                                        echo '<button title="Create new cluster" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".faq" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> <span class="bold">New Question</span></button>';
                                    }else{
                                        echo '<button type="button" class="btn btn-default" >Please Login Your Account</button>';
                                    }
                                ?>
                                <p style="margin-top:10px;"><strong>Click Title to View Comments.</strong></p>
                            </div>
                        </div>
                        <div class="row" id="row_listfaq"></div>

                        <!-- <div class="faq-item">
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                    <a data-toggle="collapse" href="#faq1" class="faq-question">What It a long established fact that a reader ?</a>
                                    <p>Added by <strong>Alex Smith</strong> <i class="fa fa-clock-o"></i> Today 2:40 pm - 24.06.2014</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                    <span class="large font-bold">Comment </span><br/>
                                    <span class="large font-bold">42</span> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div id="faq1" class="panel-collapse collapse ">
                                        <div class="faq-answer">
                                            <p>
                                                It is a long established fact that a reader will be distracted by the
                                                readable content of a page when looking at its layout. The point of
                                                using Lorem Ipsum is that it has a more-or-less normal distribution of
                                                letters, as opposed to using 'Content here, content here', making it
                                                look like readable English.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  --><!-- end faq-item -->
                       
                        <?php include "modal.php";?>   
                    </div>
                </div>
            </div>
        <div class="footer">
            <div class="pull-right">
                By <strong>Fhazkard</strong>.
            </div>
            <div>
                <strong>Copyright</strong> Fhaz Framework &copy; 2019
            </div>
        </div>

        </div>
        </div>

    <script src="resource/js/foot-faq.js"></script>
    <script src="generate.js"></script>
</body>

</html>