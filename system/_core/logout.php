<?php 
	session_start();
	error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
 	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
include 'koneksi.php';
require 'csrf.php';

if(isset($_REQUEST['logout'])){	
	if(!CSRF::validatePost()) {
		unset($_SESSION['token']);
		unset($_SESSION['limit']);
		session_destroy();
		die('<script>
			swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
			function() {window.location = "../";});</script>');			
	}
	date_default_timezone_set('Asia/Jakarta');
	$timestamp = date('YmdHis');
	$nama = $_SESSION['nama'];

	//insert data log action to database
	$log = "$nama : Melakukan action Logout Dashboard.";
	$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
	$insert_log = $koneksi->prepare($query);
	$insert_log->bind_param("ss",$log,$timestamp);
	$insert_log->execute();
	$insert_log->close();

	unset($_SESSION['token']);
	unset($_SESSION['limit']);
	unset($_SESSION['user_id']);
	unset($_SESSION['nama']);
	unset($_SESSION['role']);
	unset($_SESSION['menu']);
	unset($_SESSION['parent']);
	session_destroy();	
	echo '<script>
		swal({title: "Success",text: "Logout Success!",type: "success"}, 
		function() {window.location = "../";});</script>';	
}else{
	unset($_SESSION['token']);
	unset($_SESSION['limit']);
	session_destroy();
	die('<script>
		swal({title: "Warning",text: "Access Page Denied!",type: "warning"}, 
		function() {window.location = "../";});</script>');
}
?>