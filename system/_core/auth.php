<?php 
	session_start();
	error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
  	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php

require 'koneksi.php';
require 'csrf.php';
require 'strep.php';

if(isset($_REQUEST['username']) && isset($_REQUEST['password'])){
	if(!CSRF::validatePost()) {
		unset($_SESSION['token']);
		session_destroy();
		die('<script>
			swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
			function() {window.location = "../";});</script>');		
	}

	$nama = $_REQUEST['username'];	
	$pass = $_REQUEST['password'];
	$nama = cek_string($nama);
	$pass = cek_string($pass);
	if(empty($nama) || empty($pass)){	
		die('<script>
			swal({title: "Warning",text: "You Must Input Username and Password!",type: "warning"}, 
			function() {window.location = "../";});</script>');
	}

	date_default_timezone_set('Asia/Jakarta');
	$timestamp = date('YmdHis');
	$nama = $koneksi->real_escape_string($nama);
	$pass = $koneksi->real_escape_string($pass);
	$pass = md5($pass);

	$ada_data = 0;
	$user_id = "";
	$role_id = 0;
	$role = "";
	$active_user = 0;
	$active_role = 0;
	$menu = [];
	$parent = [];

	$query = "SELECT user_id,user_nama,tbl_users.active AS ua,role_id,role_nama,tbl_roles.active AS ra ". 
	"FROM tbl_users JOIN tbl_roles ON tbl_roles.role_id = tbl_users.user_role WHERE user_nama=? AND pass=?";
	$select_data = $koneksi->prepare($query);
	$select_data->bind_param("ss",$nama,$pass);
	$select_data->execute();
	$result = $select_data->get_result();	
	while ($data = $result->fetch_object()) {
		$user_id = $data->user_id;
		$nama = $data->user_nama;
		$active_user = $data->ua;
		$role_id = $data->role_id;
		$role = $data->role_nama;
		$active_role = $data->ra;
		$ada_data ++;
	}
	$select_data->free_result();
	$select_data->close();

	if($ada_data == 0){
		die('<script>swal({title: "Failed",text: "Login Fail!",type: "error"}, function() {window.location = "../";});</script>');
	}

	if($role_id == 4){
		$verifikasi = 0;
		$query = "SELECT verifikasi FROM tbl_customers WHERE user_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$user_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$verifikasi = $data->verifikasi;
		}
		$select_data->free_result();
		$select_data->close();

		if($verifikasi == 0){
			die('<script>swal({title: "Failed",text: "Your Account Not Active, Please Check Your Email!",type: "error"}, function() {window.location = "../";});</script>');
		}
	}
	
	$query = "SELECT * FROM tbl_access JOIN tbl_menus ON tbl_access.menu_id = tbl_menus.menu_id WHERE role_id =? ORDER BY access DESC, access_ID, parent_display";
	$select_data = $koneksi->prepare($query);
	$select_data->bind_param("i",$role_id);
	$select_data->execute();
	$result = $select_data->get_result();	
	while ($data = $result->fetch_object()) {
		array_push($menu,$data);
	}
	$select_data->free_result();
	$select_data->close();

	if(count($menu) == 0){
		die('<script>swal({title: "Failed",text: "Account Dont Have Access!",type: "error"}, function() {window.location = "../";});</script>');
	}

	$query = "SELECT menu_icon,parent_display FROM tbl_menus WHERE parent_display IS NOT NULL OR parent_display='' GROUP BY parent_display ORDER BY  parent_display";
	$select_data = $koneksi->prepare($query);
	$select_data->execute();
	$result = $select_data->get_result();	
	while ($data = $result->fetch_object()) {
		array_push($parent,$data);
	}
	$select_data->free_result();
	$select_data->close();

	if($active_user == 1 && $active_role ==  1){
		$_SESSION['limit'] = time() + 10800;
		$_SESSION['user_id'] = $user_id;
		$_SESSION['nama'] = $nama;
		$_SESSION['role'] = $role;
		$_SESSION['menu'] = $menu;
		$_SESSION['parent'] = $parent;

		$log = "$nama : Melakukan action Login Dashboard.";
		$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
		$insert_log = $koneksi->prepare($query);
		$insert_log->bind_param("ss",$log,$timestamp);
		$insert_log->execute();
		$insert_log->close();

		$link = $menu[0]->menu_url;
		echo '<script>
			swal({title: "Success",text: "Login Success!",type: "success"}, 
			function() {window.location = "../../dashboard/'.$link.'/";});</script>';
	}else{
		die('<script>swal({title: "Failed",text: "Login Failed! Your Account Inactive State!",type: "error"}, function() {window.location = "../";});</script>');
	}
}else{
	die('<script>
		swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
		function() {window.location = "../";});</script>');
}
?>