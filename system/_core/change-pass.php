<?php 
	session_start();
	error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
require 'koneksi.php';
require 'csrf.php';
require 'strep.php';

if(isset($_REQUEST['module'])){
	$module = $_REQUEST['module'];
	if($module == 'tbl_users'){
		if(isset($_REQUEST['password']) && isset($_REQUEST['konfirm']) ){
			if(!CSRF::validatePost()) {
				unset($_SESSION['token']);
				unset($_SESSION['limit']);
				session_destroy();
				die('<script>
					swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
					function() {window.location = "../";});</script>');
			}

			$limit = $_SESSION['limit'];
			if (time() > $limit){	
				unset($_SESSION['token']);	
				unset($_SESSION['limit']);
				unset($_SESSION['user_id']);
				unset($_SESSION['nama']);
				unset($_SESSION['role']);
				unset($_SESSION['menu']);
				unset($_SESSION['parent']);
				session_destroy();
				die('<script>
					swal({title: "Warning",text: "Please Login Again!",type: "warning"}, 
					function() {window.location = "../";});</script>');
			}

			date_default_timezone_set('Asia/Jakarta');
			$timestamp = date('YmdHis');
			$user_id = $_SESSION['user_id'];
			$user = $_SESSION['nama'];
			$password = $_REQUEST['password'];
			$konfirm = $_REQUEST['konfirm'];
			$password = cek_string($password);
			$konfirm = cek_string($konfirm);
			
			if(empty($password) || empty($konfirm)){	
				die('<script>
				swal({title: "Warning",text: "All Data Must Be Filled!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";});</script>');
			}
			if($password != $konfirm){	
				die('<script>
				swal({title: "Warning",text: "Password With Confirmation Password Not Same!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";});</script>');
			}

			$user_id = $koneksi->real_escape_string($user_id);
			$password = $koneksi->real_escape_string($password);
			$password = md5($password);

			try {
				$koneksi->autocommit(FALSE); //turn on transactions
				$query = "UPDATE tbl_users SET pass=? WHERE user_id=? ";		
				$update_pass = $koneksi->prepare($query);
				$update_pass->bind_param("ss",$password,$user_id);
				$update_pass->execute();
				$update_pass->close();
	
				$log = "$user : Melakukan action Change Password.";
				$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
				$insert_log = $koneksi->prepare($query);
				$insert_log->bind_param("ss",$log,$timestamp);
				$insert_log->execute();
				$insert_log->close();
	
				$koneksi->autocommit(TRUE);
				echo '<script>
						swal({title: "Success",text: "Change Password Success!",type: "success"}, 
						function() {window.location = "../";});</script>';		
			  } catch(Exception $e) {
				$koneksi->rollback();
				echo '<script>
						swal({title: "Error",text: "Change Password Failed! ",type: "error"}, 
						function() {window.location = "../../dashboard/main/";});</script>';	
			  }										
		}else{
			die('<script>
				swal({title: "Warning",text: "Data Not Found!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";});</script>');
		}			
	}else{
		die('<script>
			swal({title: "Warning",text: "Module Not Correct!",type: "warning"}, 
			function() {window.location = "../../dashboard/main/";});</script>');	
	}
}else{
	die('<script>
			swal({title: "Warning",text: "Module Not Found!",type: "warning"}, 
			function() {window.location = "../../dashboard/main/";});</script>');	
}
?>
