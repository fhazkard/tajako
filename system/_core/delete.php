<?php 
	session_start();
	error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
require 'koneksi.php';
require 'csrf.php';

if(isset($_REQUEST['delete_id']) && isset($_REQUEST['module'])){
		if(!CSRF::validatePost()) {
			unset($_SESSION['token']);
			unset($_SESSION['limit']);
			session_destroy();
			die('<script>
				swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
				function() {window.location = "../";});</script>');
		}

		$limit = $_SESSION['limit'];
		if (time() > $limit){		
			unset($_SESSION['token']);	
			unset($_SESSION['limit']);
			unset($_SESSION['user_id']);
			unset($_SESSION['nama']);
			unset($_SESSION['role']);
			unset($_SESSION['menu']);
			unset($_SESSION['parent']);
			session_destroy();
			die('<script>
				swal({title: "Warning",text: "Please Login Again!",type: "warning"}, 
				function() {window.location = "../";});</script>');
		}

		$id = $_REQUEST['delete_id'];
		$module = $_REQUEST['module'];
		$query = "DELETE FROM $module WHERE tabel_id=? ";
		$delete_data = $koneksi->prepare($query);
		$delete_data->bind_param("s",$id);
		$delete_data->execute();
		
		if($delete_data){	
			echo '<script>
				swal({title: "Success",text: "Delete Data Success!",type: "success"}, 
				function() {window.location = "../../dashboard/'.$module.'/";});</script>';						
		}else{
			echo '<script>
				swal({title: "Error",text: "Delete Data Failed!",type: "error"}, 
				function() {window.location = "../../dashboard/'.$module.'/";});</script>';	
		}
		$delete_data->close();
	}else{
		die('<script>
			swal({title: "Warning",text: "Data Not Found!",type: "warning"}, 
			function() {window.location = "../../dashboard/'.$module.'/";});</script>');
	}
?>