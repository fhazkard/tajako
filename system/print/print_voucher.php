<?php
require __DIR__ . '/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\EscposImage;
session_start(); error_reporting(1);

if($_SERVER['REQUEST_METHOD']=='POST'){
    include '../_core/db.php';
	if(isset($_POST['token_key'])){
        date_default_timezone_set('Asia/Jakarta');
        $code = $_POST['code'];
        $expired = $_POST['expired'];
        $user = $_POST['user'];
        $ip =  $_POST['ip'];

        try {
            /* Initialize */
            $connector = new NetworkPrintConnector($ip, 9100);
            $printer = new Printer($connector);
            $printer -> initialize();
        
            $printer -> setFont(Printer::FONT_B);
            $printer -> selectPrintMode(Printer::MODE_FONT_B);
            $printer -> setTextSize(6,6);
            $printer -> setJustification(Printer::JUSTIFY_CENTER);
            $printer -> text("$code\n");
            $printer -> feed(3);
            $printer -> setTextSize(1,1);
            $printer -> text("Date Expire : $expired\n");
        
            /* Cut the receipt and open the cash drawer */
            $printer -> cut();
            $printer -> pulse();
            /* Always close the printer! On some PrintConnectors, no actual
            * data is sent until the printer is closed. */
            $printer -> close();

            //insert data log action to database
			$timestamp = date('YmdHis');
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES "; 
			$query = $query."('$user : Melakukan action Print Code Voucher dengan Code ($code).' ,'".$timestamp."') ";
			$insert_data = mysqli_query($koneksi, $query);
            echo json_encode(array('code'=>'200','note'=>'Success'));
        } catch (Exception $e) {
            echo json_encode(array('code'=>'500','note'=>$e->getMessage()));
        }

        
    }
}

