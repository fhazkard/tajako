<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$user_id = $_SESSION['user_id'];
		$role_nama = $_SESSION['role'];
		$menu = $_POST['menu'];
		if(!empty($token_key)){
			$check = 0;
			$query = "SELECT EXISTS(SELECT * FROM tbl_access JOIN tbl_roles ".
			"ON tbl_access.role_id = tbl_roles.role_id ".
			"JOIN tbl_menus ON tbl_access.menu_id= tbl_menus.menu_id ".
			" JOIN tbl_users ON tbl_users.user_id =? ".
			"WHERE menu_url=? AND role_nama=? AND access=2 AND tbl_roles.active=1 AND tbl_users.active=1) AS data";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("sss",$user_id,$menu,$role_nama);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
					$check =  $data->data;
			}
			$select_data->free_result();
			if($check == 0){
				die(json_encode(array('code'=>'402','note'=>'Access Denied.')));
			}
			echo json_encode(array('code'=>'200','note'=>'Success'));
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
