<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$slide_id = $_POST['slide_id'];;
		$sort = $_POST['sort'];
		$slogan = $_POST['slogan'];
		$url = $_POST['url'];
		$buton = $_POST['buton'];
		$link = $_POST['link'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_slide SET sort=?, slogan=?, url=?, buton=?, link=? WHERE slide_id=? ";		
			$update_slide = $koneksi->prepare($query);
			$update_slide->bind_param("issssi",$sort,$slogan,$url,$buton,$link,$slide_id);
			$update_slide->execute();
			$update_slide->close();
	
			$log = "$user : Melakukan action Edit Detail Slide dengan ID ($slide_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Update Detail Slide Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
