<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$sort = $_POST['sort'];
		$slogan = $_POST['slogan'];
		$url = $_POST['url'];
		$buton = $_POST['buton'];
		$link = $_POST['link'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_slide (sort,slogan,url,buton,link) VALUES (?,?,?,?,?)";		
			$insert_slide = $koneksi->prepare($query);
			$insert_slide->bind_param("issss",$sort,$slogan,$url,$buton,$link);
			$insert_slide->execute();
			$insert_slide->close();
	
			$log = "$user : Melakukan action Register Slide baru dengan Image Link ($link).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Slide Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
