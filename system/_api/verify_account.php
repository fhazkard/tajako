<?php
session_start(); error_reporting(1);
if($_SERVER['REQUEST_METHOD']=='GET'){
	require_once '../_core/koneksi.php';
	if(isset($_GET['email']) && isset($_GET['token'])){
		date_default_timezone_set('Asia/Jakarta');
		$email = $_GET['email'];
		$token = $_GET['token'];
		if(!empty($email) && !empty($token)){

			$check = 0;
			$query = "SELECT EXISTS (SELECT * FROM tbl_customers WHERE email = ? AND verifikasi = 1 ) AS data";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$email);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
					$check =  $data->data;
			}
			$select_data->free_result();
			if($check == 1){
				die( '<h2>Your Account already actived, Thank You.</h2>' );
			}

			$customer_id = 0;
			$query = "SELECT * FROM tbl_customers WHERE email = ?";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$email);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
					$customer_id =  $data->customer_id;
			}
			$select_data->free_result();

			try {
				$koneksi->autocommit(FALSE);
				$verifikasi = 1;
				$query = "UPDATE tbl_customers SET verifikasi = ? WHERE customer_id=? ";		
				$update_customer = $koneksi->prepare($query);
				$update_customer->bind_param("ii",$verifikasi,$customer_id);
				$update_customer->execute();
				$update_customer->close();

				$timestamp = date('YmdHis');
				$log = "$email : Melakukan action Verification Email.";
				$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
				$insert_log = $koneksi->prepare($query);
				$insert_log->bind_param("ss",$log,$timestamp);
				$insert_log->execute();
				$insert_log->close();

				$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
				echo '<h2>Your account has been active, Please Login Your Account Please <a  href="https://tajako.com">Here</a>. Thank You.</h2>';
			} catch(Exception $e) {
				$koneksi->rollback(); //remove all queries from queue if error (undo)
				echo '<h2>Your account fail to active, Please <a  href="https://tajako.com">Contact Us</a> Now.</h2>';
				//echo $e->getMessage();
			}					
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token', 'data'=>$email.' '.$token));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
