<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$faq = $_POST['faq'];
		$pass = $_POST['pass'];

		$user_id = $_SESSION['user_id'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}
		if($pass == "false"){
			die(json_encode(array('code'=>'402','note'=>'Please Login Your Account.')) );
		}

		$lasttime = '';
		$query = "SELECT timestamp FROM tbl_faq WHERE user_id=? ORDER BY faq_id DESC LIMIT 1";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$user_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$lasttime =  $data->timestamp;
		}
		$select_data->free_result();

		if($lasttime != ''){
			$lasttime = strtotime($lasttime);
			$lasttime = date('YmdHis',strtotime('+1 days',$lasttime));
		}
		if($timestamp <= $lasttime){
			die(json_encode(array('code'=>'403','note'=>'You Cannot Posting New Question After 24 Hours.')) );
		}

		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_faq (user_id,title,timestamp) VALUES (?,?,?)";		
			$insert_faq = $koneksi->prepare($query);
			$insert_faq->bind_param("sss",$user_id,$faq,$timestamp);
			$insert_faq->execute();
			$insert_faq->close();
	
			$log = "$user : Melakukan action Posting FAQ baru dengan Title ($faq).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Posting Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
