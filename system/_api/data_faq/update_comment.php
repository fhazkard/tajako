<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$faq_id = $_POST['faq_id'];
		$lists = $_POST['lists'];
		$baris = $_POST['baris'];
		$pass = $_POST['pass'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}
		if($pass == "false"){
			die(json_encode(array('code'=>'402','note'=>'Please Login Your Account.')) );
		}

		try {
			$koneksi->autocommit(FALSE); 
			
			$query = "UPDATE tbl_faq SET baris=?, comments=? WHERE faq_id=?";	
			$update_comment = $koneksi->prepare($query);
			$update_comment->bind_param("isi",$baris,$lists,$faq_id);			
			$update_comment->execute();
			$update_comment->close();

			$log = "$user : Melakukan action Post Comment pada FAQ ID ($faq_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); 
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); 
			echo json_encode(array('code'=>'500','note'=>'System Comment Error.'));
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
