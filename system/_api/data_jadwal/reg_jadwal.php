<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$konsultan = $_POST['konsultan'];
		$paket = $_POST['paket'];
		$hari = $_POST['hari'];
		$awal = $_POST['awal'];
		$akhir = $_POST['akhir'];
		$awal =  strtotime($awal);
        $akhir = strtotime($akhir);

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		if($awal > $akhir){
			die(json_encode(array('code'=>'402','note'=>'Jam Mulai Melebihi Jam Selesai.')) );
		}
		$awal =  date("His",$awal);
        $akhir = date("His",$akhir);

		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "INSERT INTO tbl_jadwal (konsultan_id,paket_id,hari_id,timestart,timeend,timestamp) VALUES (?,?,?,?,?,?) ";		
			$insert_jadwal = $koneksi->prepare($query);
			$insert_jadwal->bind_param("iiisss",$konsultan,$paket,$hari,$awal,$akhir,$timestamp);
			$insert_jadwal->execute();
			$insert_jadwal->close();

			$log = "$user : Melakukan action Register Jadwal untuk Konsultan ($konsultan) Pada Paket ($paket).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register Jadwal Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
