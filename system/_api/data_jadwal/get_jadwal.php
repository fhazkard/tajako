<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();
			$query = "SELECT jd.jadwal_id, jd.konsultan_id, jd.paket_id, jd.hari_id, jd.timestart, ".
			"jd.timeend, jd.used, jd.active, ks.nama, pk.paket_name, hr.hari_nama FROM tbl_jadwal AS jd ".
			"JOIN tbl_konsultan AS ks ON jd.konsultan_id = ks.konsultan_id ".
			"JOIN tbl_paket AS pk ON jd.paket_id = pk.paket_id ".
			"JOIN tbl_hari AS hr On jd.hari_id = hr.hari_id ".
			"ORDER BY ks.nama ";
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			if($web == 'true'){
				echo json_encode(array('data'=>$resultArray)); 
			}else{
				echo json_encode($resultArray);
			}
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
