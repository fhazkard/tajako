<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$konsultan_id = $_POST['konsultan_id'];
		$paket_id = $_POST['paket_id'];
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();
			$query = "SELECT ja.jadwal_id, ja.timestart, ja.timeend, ha.hari_nama ".
			"FROM tbl_jadwal AS ja ".
			"JOIN tbl_paket AS pa ON pa.paket_id = ja.paket_id ".
			"JOIN tbl_hari AS ha ON ha.hari_id = ja.hari_id ".
			"WHERE ja.konsultan_id=? AND ja.paket_id=? AND ja.used=0 AND ja.active=1 ORDER BY ja.hari_id, ja.timestart";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("ii",$konsultan_id,$paket_id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			echo json_encode($resultArray);
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
