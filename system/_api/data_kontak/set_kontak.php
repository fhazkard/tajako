<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$kontak_id = $_POST['kontak_id'];;
		$hp = $_POST['hp'];
		$alamat = $_POST['alamat'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_kontak SET hp=?, alamat=? WHERE kontak_id=? ";		
			$update_kontak = $koneksi->prepare($query);
			$update_kontak->bind_param("ssi",$hp,$alamat,$kontak_id);
			$update_kontak->execute();
			$update_kontak->close();
	
			$log = "$user : Melakukan action Edit Detail Kontak dengan ID ($kontak_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Update Detail Kontak Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
