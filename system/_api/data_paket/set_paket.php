<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$paket_id = $_POST['paket_id'];
		$sort = $_POST['sort'];
		$nama = $_POST['nama'];
		$harga = $_POST['harga'];
		$durasi = $_POST['durasi'];
		$info = $_POST['info'];
		$types = $_POST['type'];
		$active = $_POST['active'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		$nama = $koneksi->real_escape_string($nama);
		$harga = $koneksi->real_escape_string($harga);
		$durasi = $koneksi->real_escape_string($durasi);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "UPDATE tbl_paket SET sort=?, paket_name =?,harga =?,durasi=?,paket_info=?,types=?,active =? WHERE paket_id=? ";		
			$update_paket = $koneksi->prepare($query);
			$update_paket->bind_param("isdisiii",$sort,$nama,$harga,$durasi,$info,$types,$active,$paket_id);
			$update_paket->execute();
			$update_paket->close();

			$log = "$user : Melakukan action Edit Detail paket dengan ID paket ($paket_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Edit Detail paket Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
