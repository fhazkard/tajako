<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$sort = $_POST['sort'];
		$nama = $_POST['nama'];
		$harga = $_POST['harga'];
		$durasi = $_POST['durasi'];
		$info = $_POST['info'];
		$types = $_POST['type'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		$nama = $koneksi->real_escape_string($nama);
		$harga = $koneksi->real_escape_string($harga);
		$durasi = $koneksi->real_escape_string($durasi);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "INSERT INTO tbl_paket (sort,paket_name,harga,durasi,paket_info,types,timestamp) VALUES (?,?,?,?,?,?,?)";		
			$insert_paket= $koneksi->prepare($query);
			$insert_paket->bind_param("isdisis",$sort,$nama,$harga,$durasi,$info,$types,$timestamp);
			$insert_paket->execute();
			$insert_paket->close();

			$log = "$user : Melakukan action Register paket dengan Nama ($nama).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register paket Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
