<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$role_id = $_POST['role_id'];
		$ACCID = json_decode(stripslashes($_POST['ACCID']));
		$AC = json_decode(stripslashes($_POST['ACC']));
		$ID = json_decode(stripslashes($_POST['ID']));

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	

		$proses = 0;
		for($x = 0; $x < count($ID); $x++){
			$check = 0;
			$query = "SELECT EXISTS(SELECT * FROM tbl_access ".
			"WHERE menu_id=? AND role_id=?) AS data";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("ii",$ID[$x],$role_id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
					$check =  $data->data;
			}
			$select_data->free_result();

			if($check == 0){
				$query = "INSERT INTO tbl_access (menu_id,role_id,access) VALUES (?,?,2)";
			}
			if($check == 1){
				$query = "UPDATE tbl_access SET access=? WHERE access_id=?";
			}

			$error = '';
			try {
				$koneksi->autocommit(FALSE); //turn on transactions	
				$set_accesss = $koneksi->prepare($query);
				if($check == 0){
					$set_accesss->bind_param("ii",$ID[$x],$role_id);
				}
				if($check == 1){
					$set_accesss->bind_param("ii",$AC[$x],$ACCID[$x]);
				}				
				$set_accesss->execute();
				$set_accesss->close();

				$koneksi->autocommit(TRUE); 
				$proses++;
			  } catch(Exception $e) {
				$koneksi->rollback(); 
				$proses = 'error';
				$error =  $e->getMessage();
			  }
			  if($proses == 'error'){
					break;
					die(json_encode(array('code'=>'501','note'=>$error.' '.$ID[$x])));					
			  }
		}
		if($proses == count($ID)){
			echo json_encode(array('code'=>'200','note'=>'Success'));		
	 	}else{
			echo json_encode(array('code'=>'500','note'=>'Edit Detail Access Failed.'));
		 }
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
