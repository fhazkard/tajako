<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$role_id = $_POST['role_id'];
		$menu_id = $_POST['menu_id'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		$check = 0;
		$query = "SELECT EXISTS(SELECT * FROM tbl_access ".
		"WHERE menu_id=? AND role_id=?) AS data";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("ii",$menu_id,$role_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
				$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 1){
			die(json_encode(array('code'=>'402','note'=>'Duplicate Access.')));
		}

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_access (role_id,menu_id,access) VALUES (?,?,2)";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("ii",$role_id,$menu_id);
			$update_pass->execute();
			$update_pass->close();
	
			$log = "$user : Melakukan action Register Access baru dengan Role ID ($role_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register Access Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
