<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$role = $_POST['role_id'];
		if(!empty($token_key)){
			$resultArray = array();
 			$tempArray = array();
			$query = "SELECT access_id,tbl_menus.menu_id,tbl_menus.menu_url,access FROM tbl_access ".
			"JOIN tbl_menus ON tbl_access.menu_id = tbl_menus.menu_id ".
			"JOIN tbl_roles ON tbl_access.role_id = tbl_roles.role_id  AND tbl_roles.role_id = ? ";
            "WHERE tbl_roles.role_id NOT IN (1)";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$role);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			echo json_encode($resultArray);
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
