<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$nama = $_POST['nama'];
		$diskon = $_POST['diskon'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_typecustomer (type_name,type_diskon,timestamp) VALUES (?,?,?)";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("sss",$nama,$diskon,$timestamp);
			$update_pass->execute();
			$update_pass->close();
	
			$log = "$user : Melakukan action Register Type Customer baru dengan Nama ($nama).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register Type Customer Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
