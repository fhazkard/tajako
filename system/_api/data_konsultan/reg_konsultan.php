<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$user_nama = $_POST['username'];
		$nama = $_POST['nama'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$foto = $_POST['foto'];
		$point = $_POST['point'];
		$info = $_POST['info'];
		$user = $_SESSION['nama'];
		$pass = md5("123456");
		$timestamp = date('YmdHis');
		$user_id = make_token_uniq_ID($timestamp);

		$user_nama = $koneksi->real_escape_string($user_nama);
		$nama = $koneksi->real_escape_string($nama);
		$email = $koneksi->real_escape_string($email);
		$user_role = 3;
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "INSERT INTO tbl_users (user_id,user_nama,pass,user_role) VALUES (?,?,?,?)";		
			$insert_user = $koneksi->prepare($query);
			$insert_user->bind_param("sssi",$user_id,$user_nama,$pass,$user_role);
			$insert_user->execute();
			$insert_user->close();

			$query = "INSERT INTO tbl_konsultan (user_id,nama,hp,email,photo,info,points,timestamp) VALUES (?,?,?,NULLIF('".$email."',''),?,?,?,?) ";		
			$insert_konsultan = $koneksi->prepare($query);
			$insert_konsultan->bind_param("sssssis",$user_id,$nama,$hp,$foto,$info,$point,$timestamp);
			$insert_konsultan->execute();
			$insert_konsultan->close();

			$log = "$user : Melakukan action Register konsultan dengan Username ($user_nama) & Nama ($nama).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register konsultan Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
