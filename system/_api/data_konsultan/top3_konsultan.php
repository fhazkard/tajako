<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();

			$awal = date('Ym').'01000000';
			$awal =  strtotime($awal);
			$awal =  strtotime("-30 days",$awal);
			$akhir = strtotime("+29 days",$awal);
			$awal = date("Ymd",$awal);
			$akhir = date("Ymd",$akhir);

			$query = "SELECT konsultan_id AS id, SUM(price) AS profit FROM tbl_order WHERE status=4 ".
			"AND timestamp BETWEEN ? AND ? GROUP BY konsultan_id ORDER BY profit DESC LIMIT 3";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("ss",$awal,$akhir);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;

				$tempdata = array();
				$subquery = "SELECT * FROM tbl_konsultan WHERE konsultan_id=?";
				$get_data = $koneksi->prepare($subquery);
				$get_data->bind_param("i",$tempArray->id);
				$get_data->execute();
				$hasil = $get_data->get_result();	
				while ($datas = $hasil->fetch_object()) {
					$tempdata = $datas;
				}
				$get_data->free_result();

				array_push($resultArray, $tempdata);
			}
			$select_data->free_result();
			echo json_encode(array('data'=>$resultArray,'awal'=>$awal,'akhir'=>$akhir)); 
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
