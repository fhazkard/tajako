<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$konsultan_id = $_POST['konsultan_id'];
		$nama = $_POST['nama'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$foto = $_POST['foto'];
		$point = $_POST['point'];
		$info = $_POST['info'];
		$user = $_SESSION['nama'];
		$active = $_POST['active'];
		$timestamp = date('YmdHis');
		
		$user_nama = $koneksi->real_escape_string($user_nama);
		$nama = $koneksi->real_escape_string($nama);
		$email = $koneksi->real_escape_string($email);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "UPDATE tbl_konsultan SET nama =?,hp =?,email =NULLIF('".$email."',''),photo=?,info=?,points=?,active =? WHERE konsultan_id=? ";		
			$update_konsultan = $koneksi->prepare($query);
			$update_konsultan->bind_param("ssssiii",$nama,$hp,$foto,$info,$point,$active,$konsultan_id);
			$update_konsultan->execute();
			$update_konsultan->close();

			$log = "$user : Melakukan action Edit Detail konsultan dengan ID konsultan ($konsultan_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Edit Detail konsultan Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
