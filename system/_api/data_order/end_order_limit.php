<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$order_id = $_POST['order_id'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		$customer_id = 0;
		$query = "SELECT customer_id FROM tbl_order WHERE order_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$customer_id =  $data->customer_id;
		}
		$select_data->free_result();

		$konsultan_id = 0;
		$query = "SELECT konsultan_id FROM tbl_order WHERE order_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$konsultan_id =  $data->konsultan_id;
		}
		$select_data->free_result();

		$jadwal_id = 0;
		$query = "SELECT jadwal_id FROM tbl_order WHERE order_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$jadwal_id =  $data->jadwal_id;
		}
		$select_data->free_result();

		$types = 0;
		$query = "SELECT pa.types FROM tbl_order AS ord JOIN tbl_paket AS pa ON pa.paket_id = ord.paket_id WHERE ord.order_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$types =  $data->types;
		}
		$select_data->free_result();

		$check = 0;
		$query = "SELECT EXISTS(SELECT * FROM tbl_chat WHERE order_id=? ) AS data";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$check =  $data->data;
		}
		$select_data->free_result();
		$status = 3;
		if($check == 1){
			$status = 4;	
		}

		$old_status = "";
		$query = "SELECT status FROM tbl_order WHERE order_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$old_status =  $data->status;
		}
		$select_data->free_result();
		if($old_status == 1){
			$status = 4;	
		}

		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query1 = "UPDATE tbl_order SET status=? WHERE order_id=? ";		
			$update_order = $koneksi->prepare($query1);
			$update_order->bind_param("ii",$status,$order_id);
			$update_order->execute();
			$update_order->close();

			$query2 = "UPDATE tbl_chat SET status=1 WHERE order_id=? ";		
			$update_chat = $koneksi->prepare($query2);
			$update_chat->bind_param("i",$order_id);
			$update_chat->execute();
			$update_chat->close();

			$query3 = "UPDATE tbl_jadwal SET used=0 WHERE jadwal_id=? ";		
			$update_jadwal = $koneksi->prepare($query3);
			$update_jadwal->bind_param("i",$jadwal_id);
			$update_jadwal->execute();
			$update_jadwal->close();

			if($status == 4){
				$query4 = "UPDATE tbl_konsultan SET point=(point+$types) WHERE konsultan_id=? ";		
				$update_konsultan = $koneksi->prepare($query4);
				$update_konsultan->bind_param("i",$konsultan_id);
				$update_konsultan->execute();
				$update_konsultan->close();

				$query5 = "UPDATE tbl_customer SET point=(point+$types) WHERE customer_id=? ";		
				$update_customer = $koneksi->prepare($query5);
				$update_customer->bind_param("i",$customer_id);
				$update_customer->execute();
				$update_customer->close();
			}
	
			$log = "Auto System : Melakukan action Close Order Limit $status dengan ID ($order_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Process System Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
