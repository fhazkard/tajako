<?php
//untuk verification email customer ketika register

// Import phpmailer classes into the global namespace
// These must be at the top of your script, not inside a function
use phpmailer\phpmailer\phpmailer;
use phpmailer\phpmailer\Exception;
use phpmailer\phpmailer\smtp;

require '../phpmailer/exception.php';
require '../phpmailer/phpmailer.php';
require '../phpmailer/smtp.php';

// Instantiation and passing `true` enables exceptions
function send_infopayment($email,$konsultan,$kode,$price) {
	$mail = new phpmailer;
	try {
		//Server settings
		//$mail->SMTPDebug = 2;                                       // Enable verbose debug output
		$mail->isSMTP();                                            // Set mailer to use smtp
		$mail->Host       = 'tajako.com';  // Specify main and backup smtp servers
		$mail->SMTPAuth   = true;                                   // Enable smtp authentication
		$mail->Username   = 'support@tajako.com';                     // smtp username
		$mail->Password   = '@fhazkard21';                               // smtp password
		$mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
		$mail->Port       = 465;                                    // TCP port to connect to
	
		//Recipients
		$mail->setFrom('support@tajako.com', 'Information Payment Order');
		$mail->addAddress($email, 'Customer');     // Add a recipient
		//$mail->addAddress('ellen@example.com');               // Name is optional
		$mail->addReplyTo('admin@tajako.com', 'Confirmation Payment');
		$mail->addCC($konsultan);
		//$mail->addBCC('support@tajako.com');
	
		// Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	
		// Content
		//$token = md5( rand(0,1000) );
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'TAJAKO - Payment Detail';
		$mail->Body    = nl2br('<h2>Thanks for Request Order!</h2>'. "\r\n".
		'Your Order has been created. '. "\r\n".
		'Now you can make payment to our bank number in below:<br>'. "\r\n".
		'<b>Account Name: Agustianto'. "\r\n".
		'Bank Number: 852 019 8441'. "\r\n".
		'Bank Name: BCA'. "\r\n".
		'Price: Rp.'.number_format($price). "\r\n".
		'Order Code: '.$kode. " (Input Order Code In Form Transaction Information. MUST INCLUDE!)</b><br>\r\n".

		'After your payment completed, Please reply this email with transaction SCREENSHOT (attachment).'.  "\r\n".
		'You will get email when your order has been Approved by System.<br>'. "\r\n".
		'Thank You, Customer.');
	
		$mail->send();
		//echo 'Message has been sent';
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}
?>
