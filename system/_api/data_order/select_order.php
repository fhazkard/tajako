<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$role = $_SESSION['role'];
		$user_id = $_SESSION['user_id'];
		$timestamp = date('YmdHis');
		if(!empty($token_key)){
			$check = 0;
			$aray = [];
			$query = "";
			$kolom = "";
			$id = "";

			if($role == 'Customer' || $role == 'Konsultan'){

				if($role == 'Customer'){
				$query = "SELECT *, (SELECT EXISTS(SELECT * FROM tbl_customers WHERE user_id=?)) AS datacheck ".
				"FROM tbl_customers WHERE user_id=? ";
			}
			if($role == 'Konsultan'){
				$query = "SELECT *, (SELECT EXISTS(SELECT * FROM tbl_konsultan WHERE user_id=?)) AS datacheck ".
				"FROM tbl_konsultan WHERE user_id=? ";
			}
			
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("ss",$user_id,$user_id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$aray =  $data;
				$check = $data->datacheck;				
			}
			$select_data->free_result();
			if($check == 0){
				die(json_encode(array('code'=>'402','note'=>'Cannot Find Your Data Order.')) );
			}

			if($role == 'Customer'){
				$kolom = "ord.customer_id=?";
				$id = $aray->customer_id;
			}
			if($role == 'Konsultan'){
				$kolom = "ord.konsultan_id=?";
				$id = $aray->konsultan_id;
			}

			$resultArray = array();
			$tempArray = array();
			$query = "SELECT ord.order_id, ord.kode, ord.price, ord.status, ord.timestamp, ".
			"kon.nama AS konsultan, pa.paket_name, pa.durasi, pa.paket_info, ja.timestart, ja.timeend, ha.hari_nama ".
			"FROM tbl_order AS ord ".
			"JOIN tbl_customers AS cus ON cus.customer_id = ord.customer_id ".
			"JOIN tbl_konsultan AS kon ON kon.konsultan_id = ord.konsultan_id ".
			"JOIN tbl_paket AS pa ON pa.paket_id = ord.paket_id ".
			"JOIN tbl_jadwal AS ja ON ja.jadwal_id = ord.jadwal_id ".
			"JOIN tbl_hari AS ha ON ha.hari_id = ja.hari_id WHERE ".$kolom." AND ord.status != 3 AND ord.status != 4 ";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("i",$id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();

			}
			
			echo json_encode(array('code'=>'200','data'=>$resultArray));
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
