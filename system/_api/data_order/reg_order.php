<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	include 'send_infopayment_email.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$jadwal_id = $_POST['jadwal'];
		$konsultan_id = $_POST['konsultan_id'];
		$paket_id = $_POST['paket_id'];
		$payment = "Via Transfer Rekening Bank";

		$user_id = $_SESSION['user_id'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		$kode = make_kode_uniq($timestamp);

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}
		if(empty($jadwal_id)){
			die(json_encode(array('code'=>'402','note'=>'Jadwal Konsultasi Kosong.')) );
		}	
		if(empty($konsultan_id)){
			die(json_encode(array('code'=>'403','note'=>'Konsultan Empty.')) );
		}

		$check = 0;
		$query = "SELECT EXISTS(SELECT * FROM tbl_customers WHERE user_id=?) AS data ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$user_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 0){
			die(json_encode(array('code'=>'404','note'=>'Cannot Find Your Data Customer.')) );
		}

		$check = 1;
		while($check == 1){
			$query = "SELECT EXISTS(SELECT * FROM tbl_order WHERE kode=?) AS data ";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$kode);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$check =  $data->data;
			}
			$select_data->free_result();
			if($check == 0){
				break;
			}else{
				$kode = make_kode_uniq($timestamp);
			}
		}

		$konsultan= [];
		$query = "SELECT * FROM tbl_konsultan WHERE konsultan_id=? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$konsultan_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$konsultan =  $data;
		}
		$select_data->free_result();

		$cus= [];
		$query = "SELECT * FROM tbl_customers AS cus JOIN tbl_typecustomer AS tc ON tc.type_id = cus.types WHERE cus.user_id=? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$user_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$cus =  $data;
		}
		$select_data->free_result();

		$paket = [];
		$query = "SELECT * FROM tbl_paket AS pa JOIN tbl_typepaket AS tp ON tp.type_id = pa.types WHERE pa.paket_id=? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$paket_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$paket =  $data;
		}
		$select_data->free_result();

		$diskon_pkt = ($paket->harga*$paket->type_diskon)/100;
		$sisa_harga = $paket->harga - $diskon_pkt;
		$diskon_cus = ($sisa_harga*$cus->type_diskon)/100;
		$diskon = $diskon_pkt+$diskon_cus;
		$price = $sisa_harga - $diskon_cus;
		if($price < 0){
			$price = 0;
		}

		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_order (kode,customer_id,konsultan_id,paket_id,jadwal_id,payment,price,diskon,timestamp) VALUES (?,?,?,?,?,?,?,?,?)";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("siiiisdds",$kode,$cus->customer_id,$konsultan_id,$paket_id,$jadwal_id,$payment,$price,$diskon,$timestamp);
			$update_pass->execute();
			$update_pass->close();

			$query = "UPDATE tbl_jadwal SET used=1 WHERE jadwal_id=? ";
			$update_jadwal= $koneksi->prepare($query);
			$update_jadwal->bind_param("i",$jadwal_id);
			$update_jadwal->execute();
			$update_jadwal->close();
	
			$log = "$user : Melakukan action Request Paket Order baru dengan Kode ($kode).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			send_infopayment($cus->email,$konsultan->email,$kode,$price);

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Process System Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
