<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	include 'send_order_approved_email.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$order_id = $_POST['order_id'];;
		$status = $_POST['status'];
		$kode = $_POST['kode'];
		$statustext = $_POST['statustext'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}
		
		$order = [];
		$query = "SELECT * FROM tbl_customers AS cus ".
		"JOIN tbl_order AS ord ON ord.customer_id = cus.customer_id AND ord.order_id = ? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$order =  $data;
		}
		$select_data->free_result();


		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_order SET status=? WHERE order_id=? ";		
			$update_order = $koneksi->prepare($query);
			$update_order->bind_param("ii",$status,$order_id);
			$update_order->execute();
			$update_order->close();

			if($status == 3){
				$query = "UPDATE tbl_jadwal SET used=0 WHERE jadwal_id=? ";		
				$update_jadwal = $koneksi->prepare($query);
				$update_jadwal->bind_param("i",$order->jadwal_id);
				$update_jadwal->execute();
				$update_jadwal->close();
			}
	
			$log = "$user : Melakukan action Edit Status Order $statustext dengan kode ($kode).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			if($status == 1){
				send_order_approved($order->email,$order->nama);
			}

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Process Update Order System Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
