<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		/* setlocale(LC_ALL, 'IND'); */
		$token_key = $_POST['token_key'];
		$order_id = $_POST['order_id'];

		$user = $_SESSION['nama'];
		$start = date('His');
		$hari = strftime("%A");		
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		$check = 0;
		$jadwal_id = 0;
		$query = "SELECT jadwal_id, (SELECT EXISTS(SELECT * FROM tbl_order WHERE order_id=? AND status=1)) AS data ".
		"FROM tbl_order WHERE order_id=? AND status=1";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("ii",$order_id,$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$jadwal_id =  $data->jadwal_id;
			$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 0){
			die(json_encode(array('code'=>'402','note'=>'Your Order Cannot Approve By System')) );
		}

		$jadwal = [];
		$query = "SELECT * FROM tbl_jadwal WHERE jadwal_id=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$jadwal_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$jadwal =  $data;
		}
		$select_data->free_result();

		$check = 0;
		$durasi = "00:00:00";
		$query = "SELECT TIMEDIFF(ja.timeend,?) AS durasi, (SELECT EXISTS(SELECT * FROM tbl_jadwal AS ja ".
		"JOIN tbl_hari AS ha ON ha.hari_id = ja.hari_id ".
		"WHERE ja.jadwal_id=? AND ja.used=1 AND ja.active=1 AND ha.hari_nama =? AND ha.active =1)) AS data ".
		"FROM tbl_jadwal AS ja ".
		"JOIN tbl_hari AS ha ON ha.hari_id = ja.hari_id ".
		"WHERE ja.jadwal_id=? AND ja.used=1 AND ja.active=1 AND ha.hari_nama =? AND ha.active =1";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("sisis",$start,$jadwal_id,$hari,$jadwal_id,$hari);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$durasi =  $data->durasi;
			$check =  $data->data;
		}
		$select_data->free_result();

		if($check == 0){
			echo json_encode(array('code'=>'403','note'=>'Hari Jadwal Order Invalid.'));
		}

		$start = strtotime($start);
		$mulai = strtotime($jadwal->timestart."");
		$selesai = strtotime($jadwal->timeend."");
		if($start >= $mulai && $start <= $selesai){
			
		}else{
			die(json_encode(array('code'=>'404','note'=>'Waktu Jadwal Order Invalid','start'=>$start,'mulai'=>$mulai,'selesai'=>$selesai)) );
		}

		if($check == 1){
			$durasi = explode(':',$durasi);
			$mnt = $durasi[1]*1;
			$dtk = $durasi[2]*1;
			$durasi = 0;
			if($mnt > 0){
				$durasi = $mnt;
			}
			if($dtk > 30){
				$durasi = $durasi + 1;
			}
			echo json_encode(array('code'=>'200','note'=>'Success','durasi'=>$durasi));
		}	
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
