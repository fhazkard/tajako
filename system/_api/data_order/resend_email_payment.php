<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	include 'send_infopayment_email.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$order_id = $_POST['order_id'];;

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}
		
		$order = [];
		$query = "SELECT * FROM tbl_customers AS cus ".
		"JOIN tbl_order AS ord ON ord.customer_id = cus.customer_id AND ord.order_id = ? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$order =  $data;
		}
		$select_data->free_result();	

		if($order->status != 0){
			die(json_encode(array('code'=>'402','note'=>'Cannot Resend Email. Status Order Invalid!')) );
		}

		$konsultan = [];
		$query = "SELECT * FROM tbl_konsultan AS kn ".
		"JOIN tbl_order AS ord ON ord.konsultan_id = kn.konsultan_id AND ord.order_id = ? ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$konsultan =  $data;
		}
		$select_data->free_result();

		try {
			$koneksi->autocommit(FALSE); //turn on transactions
	
			$log = "$user : Melakukan action Edit Resend Email Info Payment Untuk Email ($order->email).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			send_infopayment($order->email,$konsultan->email,$order->kode,$order->price);

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Process Resend System Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
