<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		if(!empty($token_key)){
			$resultArray = array();
 			$tempArray = array();
			$query = "SELECT ord.order_id, ord.kode, ord.payment, ord.price, ord.status, ord.timestamp, ".
			"cus.nama AS customer, kon.nama AS konsultan, pa.paket_name, ja.timestart, ja.timeend, ha.hari_nama ".
			"FROM tbl_order AS ord ".
			"JOIN tbl_customers AS cus ON cus.customer_id = ord.customer_id ".
			"JOIN tbl_konsultan AS kon ON kon.konsultan_id = ord.konsultan_id ".
			"JOIN tbl_paket AS pa ON pa.paket_id = ord.paket_id ".
			"JOIN tbl_jadwal AS ja ON ja.jadwal_id = ord.jadwal_id ".
			"JOIN tbl_hari AS ha ON ha.hari_id = ja.hari_id WHERE ord.status != 4";
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			if($web == 'true'){
				echo json_encode(array('data'=>$resultArray)); 
			}else{
				echo json_encode($resultArray);
			}
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
