<?php
session_start(); error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$periode = $_POST['token_key'];
		$web = $_POST['web'];
		$extra = "";

		if($periode == "now"){
			$tgl_satu = date('Ym').'01000000';
			$extra = "WHERE ord.timestamp >='".$tgl_satu."' AND ord.status=4 ORDER BY ord.timestamp";
		}
		
		if($periode == "all"){
			$extra = "WHERE ord.status=4 ORDER BY ord.timestamp";
		}

		if($periode == "-7 days" || $periode == "-30 days"){
			$tgl_awal = date('Ymd').'235900';
			$tgl_akhir = strtotime($tgl_awal);
			$tgl_akhir = strtotime($periode,$tgl_akhir);
			$tgl_akhir =  date('Ymd',$tgl_akhir).'000000';
			$extra = "WHERE ord.timestamp >= '".$tgl_akhir."' AND ord.timestamp <= '".$tgl_awal."' AND ord.status=4 ORDER BY ord.timestamp";
		}
		if(empty($periode)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		$query = "SELECT ord.kode, ord.price, ord.status, ord.timestamp, ".
		"cus.nama AS cus_nama, kon.nama AS konsul_nama, pa.paket_name ".
		"FROM tbl_order AS ord ".
		"JOIN tbl_customers AS cus ON cus.customer_id = ord.customer_id ".
		"JOIN tbl_konsultan AS kon ON kon.konsultan_id = ord.konsultan_id ".
		"JOIN tbl_paket AS pa ON pa.paket_id = ord.paket_id ".$extra;

		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$resultArray = array();
			$tempArray = array();
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			$select_data->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('data'=>$resultArray)); 
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>$e->getMessage()));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
