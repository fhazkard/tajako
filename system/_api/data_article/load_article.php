<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$timestamp = date('YmdHis');
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();
			$timestamp = strtotime($timestamp);
			//$timestamp = date('YmdHis',strtotime('-7 days',$timestamp));
			$query = "SELECT * FROM tbl_article";
			$select_data = $koneksi->prepare($query);
			///$select_data->bind_param("s",$timestamp);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			echo json_encode(array('data'=>$resultArray)); 
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
