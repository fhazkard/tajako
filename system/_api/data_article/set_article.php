<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$article_id = $_POST['article_id'];
		$sort = $_POST['sort'];
		$title = $_POST['title'];
		$intro = $_POST['intro'];
		$img = $_POST['img'];
		$isi = $_POST['isi'];
		$sumber = $_POST['sumber'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_article SET sort=?, title=?, intro=?, img=?, isi=?, sumber=? WHERE article_id=? ";		
			$update_article = $koneksi->prepare($query);
			$update_article->bind_param("isssssi",$sort,$title,$intro,$img,$isi,$sumber,$article_id);
			$update_article->execute();
			$update_article->close();
	
			$log = "$user : Melakukan action Edit Detail Article dengan ID ($article_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Update Detail Article Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
