<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$order_id = $_POST['order_id'];
		$listchat = $_POST['listchat'];

		$timestamp = date('YmdHis');
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	

		$check = 0;
		$query = "SELECT EXISTS(SELECT * FROM tbl_chat WHERE order_id=? ) AS data";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$order_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 0){
			$query = "INSERT INTO tbl_chat (order_id, listchat,timestamp) VALUES (?,?,?)";		
		}
		if($check == 1){
			$query = "UPDATE tbl_chat SET listchat=? WHERE order_id=?";	
		}	

		try {
			$koneksi->autocommit(FALSE); 
			
			$send_chat = $koneksi->prepare($query);
			if($check == 0){
				$send_chat->bind_param("iss",$order_id,$listchat,$timestamp);
			}
			if($check == 1){
				$send_chat->bind_param("si",$listchat,$order_id);
			}				
			$send_chat->execute();
			$send_chat->close();

			$koneksi->autocommit(TRUE); 
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); 
			echo json_encode(array('code'=>'500','note'=>'System Send Error.'));
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
