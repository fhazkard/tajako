<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$role = $_SESSION['role'];
		$user_id = $_SESSION['user_id'];
		$timestamp = date('YmdHis');
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();
			$timestamp = strtotime($timestamp);
			$timestamp = date('YmdHis',strtotime('-7 days',$timestamp));
			$extra = "WHERE ch.status = 2";
			if($role == "Customer"){
				$extra = "WHERE ch.status = 1 AND ch.timestamp >= '".$timestamp."'  AND cus.user_id = '".$user_id."'";
			}
			if($role == "Konsultan"){
				$extra = "WHERE ch.status = 1 AND ch.timestamp >= '".$timestamp."' AND kon.user_id = '".$user_id."'";
			}
			if($role == "Admin" || $role == "System"){
				$extra = "WHERE ch.status = 1 AND ch.timestamp >= '".$timestamp."'";
			}

			$query = "SELECT ch.chat_id, ch.listchat, ch.status, ord.kode, ord.timestamp, ".
			"cus.nama AS cus_nama, kon.nama AS konsul_nama ".
			"FROM tbl_chat AS ch ".
			"JOIN tbl_order AS ord ON ord.order_id = ch.order_id ".
			"JOIN tbl_customers AS cus ON cus.customer_id = ord.customer_id ".
			"JOIN tbl_konsultan AS kon ON kon.konsultan_id = ord.konsultan_id ".$extra;
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			if($web == 'true'){
				echo json_encode(array('data'=>$resultArray)); 
			}else{
				echo json_encode($resultArray);
			}
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
