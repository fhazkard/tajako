<?php
session_start(); error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$query = "SELECT COUNT(tbl_rentduty.vehicle_id) AS qty, vehicle_nama AS nama ".
		"FROM tbl_rentduty JOIN tbl_vehicles ON tbl_vehicles.vehicle_id = tbl_rentduty.vehicle_id ".
		"GROUP BY tbl_rentduty.vehicle_id ORDER BY qty";
		if(!empty($token_key)){
			$resultArray = array();
 			$tempArray = array();
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			echo json_encode($resultArray);				
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
