<?php
session_start(); error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$tenant_id = $_POST['tenant_id'];
		$tbl = 'bill_'.$tenant_id;
		$web = $_POST['web'];
		
		$tbl_item ='itemorder_'.$tenant_id;
		$tgl_awal = date('Ym').'01000000';
		$thn = date('Y');
		$bln = date('m');
		$last_bln = $bln - 1;
		if($bln == '01'){
			$last_bln = 12;
			$thn = $thn - 1;
		}
		$periode_awal = $thn.'0'.$last_bln.'01000000';
		$periode_akhir = $thn.'0'.$last_bln.'31000000';
		
		if(!empty($token_key)){
			$tempArray = array();
			$query = "SELECT SUM(payment) AS total_bill, ".
			"(SELECT SUM(discount) FROM tbl_rentbill WHERE paydate >= '".$tgl_awal."') AS total_discount, ".
			"(SELECT COUNT(rent_status) FROM tbl_rentvehicle WHERE rent_status='Progress') AS order_progress, ".
			"(SELECT COUNT(rent_status) FROM tbl_rentvehicle WHERE rent_status='Complete' AND timestamp >= '".$tgl_awal."') AS order_complete, ".
			"(SELECT SUM(payment) FROM tbl_rentbill WHERE paydate >= '".$periode_awal."' AND paydate <= '".$periode_akhir."') AS prev_total_bill, ".
			"(SELECT SUM(discount) FROM tbl_rentbill WHERE paydate >= '".$periode_awal."' AND paydate <= '".$periode_akhir."') AS prev_total_discount, ".
			"(SELECT COUNT(rent_status) FROM tbl_rentvehicle WHERE rent_status='Complete' AND timestamp >= '".$periode_awal."' AND timestamp <= '".$periode_akhir."') AS prev_order_complete ".
			"FROM tbl_rentbill WHERE paydate >= '".$tgl_awal."' ";
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
			}
			$select_data->free_result();
			echo json_encode($tempArray);
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
