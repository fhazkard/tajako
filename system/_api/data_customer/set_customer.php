<?php
session_start(); error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$customer_id = $_POST['customer_id'];
		$nama = $_POST['nama'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$card = $_POST['card'];
		$jabatan = $_POST['jabatan'];
		$alamat = $_POST['alamat'];
		$types = $_POST['types'];
		$wn = $_POST['wn'];
		$point = $_POST['point'];
		$info = $_POST['info'];
		$blacklist = $_POST['blacklist'];
		$verifikasi = $_POST['verifikasi'];
		$active = $_POST['active'];

		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');

		$nama = $koneksi->real_escape_string($nama);
		$hp = $koneksi->real_escape_string($hp);
		$email = $koneksi->real_escape_string($email);
		$card = $koneksi->real_escape_string($card);
		$jabatan = $koneksi->real_escape_string($jabatan);
		
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "UPDATE tbl_customers SET card_id =NULLIF('".$card."',''), ".
			"wn=?,nama =?,hp =NULLIF('".$hp."',''),email =NULLIF('".$email."',''),types =?,points =?,verifikasi =?,active =? WHERE customer_id=? ";		
			$update_customer = $koneksi->prepare($query);
			$update_customer->bind_param("isiiiii",$wn,$nama,$types,$point,$verifikasi,$active,$customer_id);
			$update_customer->execute();
			$update_customer->close();

			$query = "UPDATE tbl_cusdetail SET jabatan =?,alamat =?,customer_info =?,blacklist =? WHERE customer_id=? ";		
			$update_detail = $koneksi->prepare($query);
			$update_detail->bind_param("sssii",$jabatan,$alamat,$info,$blacklist,$customer_id);
			$update_detail->execute();
			$update_detail->close();

			$log = "$user : Melakukan action Edit Detail Customer dengan ID Customer ($customer_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Edit Detail Customer Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
