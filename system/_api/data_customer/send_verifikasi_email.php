<?php
//untuk verification email customer ketika register

// Import phpmailer classes into the global namespace
// These must be at the top of your script, not inside a function
use phpmailer\phpmailer\phpmailer;
use phpmailer\phpmailer\Exception;
use phpmailer\phpmailer\smtp;

require '../phpmailer/exception.php';
require '../phpmailer/phpmailer.php';
require '../phpmailer/smtp.php';

// Instantiation and passing `true` enables exceptions
function send_verifikasi_email($email) {
	$mail = new phpmailer;
	try {
		//Server settings
		//$mail->SMTPDebug = 2;                                       // Enable verbose debug output
		$mail->isSMTP();                                            // Set mailer to use smtp
		$mail->Host       = 'tajako.com';  // Specify main and backup smtp servers
		$mail->SMTPAuth   = true;                                   // Enable smtp authentication
		$mail->Username   = 'support@tajako.com';                     // smtp username
		$mail->Password   = '@fhazkard21';                               // smtp password
		$mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
		$mail->Port       = 465;                                    // TCP port to connect to
	
		//Recipients
		$mail->setFrom('support@tajako.com', 'Register Account');
		$mail->addAddress($email, 'Customer');     // Add a recipient
		//$mail->addAddress('ellen@example.com');               // Name is optional
		//$mail->addReplyTo('info@example.com', 'Information');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');
	
		// Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	
		// Content
		$token = md5( rand(0,1000) );
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'TAJAKO - Email Verification';
		$mail->Body    = nl2br('Thanks for Register! '. "\r\n".
		'Your account has been created, you can login to our Dashboard with the following credentials '.
		'after you have activated your account by click the url below.<br>'.  "\r\n".
		'Please click this link to activate your account:'. "\r\n".
		'https://tajako.com/system/_api/verify_account.php?email='.$email.'&token='.$token);
	
		$mail->send();
		//echo 'Message has been sent';
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}
?>
