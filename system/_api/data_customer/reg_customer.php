<?php

session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	include 'send_verifikasi_email.php';
	if(isset($_POST['token_key']) ){

		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];	
		$usernama = $_POST['usernama'];
		$pass = $_POST['pass'];

		$nama = $_POST['nama'];
		$hp = $_POST['hp'];
		$email = $_POST['email'];
		$card = $_POST['card'];
		$jabatan = $_POST['jabatan'];
		$alamat = $_POST['alamat'];
		$types = $_POST['types'];
		$wn = $_POST['wn'];
		$info = $_POST['info'];
		$timestamp = date('YmdHis');

		$user_id = make_token_uniq_ID($usernama);

		$nama = $koneksi->real_escape_string($nama);
		$hp = $koneksi->real_escape_string($hp);
		$card = $koneksi->real_escape_string($card);
		$jabatan = $koneksi->real_escape_string($jabatan);
		$user_role = 4;
		$pass = md5($pass);

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	

		$check = 0;
		$query = "SELECT EXISTS(SELECT * FROM tbl_customers WHERE email=?) AS data ";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$email);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 1){
			die(json_encode(array('code'=>'402','note'=>'Email Already Used.')));
		}

		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "INSERT INTO tbl_users (user_id,user_nama,pass,user_role) VALUES (?,?,?,?)";		
			$insert_user = $koneksi->prepare($query);
			$insert_user->bind_param("sssi",$user_id,$usernama,$pass,$user_role);
			$insert_user->execute();
			$insert_user->close();

			$query = "INSERT INTO tbl_customers (user_id,card_id,wn,nama,hp,email,types,timestamp) VALUES ".
			"(?,NULLIF('".$card."',''),?,?,?,?,?,?)";		
			$insert_cus = $koneksi->prepare($query);
			$insert_cus->bind_param("sisssis",$user_id,$wn,$nama,$hp,$email,$types,$timestamp);
			$insert_cus->execute();
			$insert_cus->close();

			$customer_id = null;
			$query = "SELECT customer_id FROM tbl_customers WHERE user_id=? ORDER BY customer_id DESC LIMIT 1";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$user_id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$customer_id =  $data->customer_id;
			}
			$select_data->free_result();
			$select_data->close();

			$query = "INSERT INTO tbl_cusdetail (customer_id,jabatan,alamat,customer_info) VALUES (?,?,?,?) ";		
			$insert_cusdetail = $koneksi->prepare($query);
			$insert_cusdetail->bind_param("isss",$customer_id,$jabatan,$alamat,$info);
			$insert_cusdetail->execute();
			$insert_cusdetail->close();

			$log = "$nama : Melakukan action Register Account dengan Email ($email).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			send_verifikasi_email($email);

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'System Cannot Process Your Request.'));
			//echo json_encode(array('code'=>'500','note'=>$e->getMessage()));
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}else{
	echo json_encode(array('code'=>'501','note'=>'Bad Request URL'));
}
?>
