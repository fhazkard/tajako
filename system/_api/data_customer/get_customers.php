<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		$user_id = $_SESSION['user_id'];
		$role = $_SESSION['role'];
		if(!empty($token_key)){
			$resultArray = array();
			$tempArray = array();

			$query = "SELECT tn.customer_id,tn.card_id, ".
			"tn.wn, tn.nama, tn.hp, tn.email, tn.types, tn.saldo, tn.points, tn.verifikasi, tn.active, tp.type_name, ".
			"dt.jabatan, dt.alamat, dt.customer_info, dt.blacklist ".
			"FROM tbl_customers AS tn JOIN tbl_cusdetail AS dt ON dt.customer_id = tn.customer_id ".
			"JOIN tbl_typecustomer AS tp ON tp.type_id = tn.types";
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			if($web == 'true'){
				echo json_encode(array('data'=>$resultArray)); 
			}else{
				echo json_encode($resultArray);
			}
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
