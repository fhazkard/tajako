<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$display = $_POST['display'];
		$url = $_POST['url'];
		$icon = $_POST['icon'];
		$type = $_POST['type'];
		$parent = $_POST['parent'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		$display = $koneksi->real_escape_string($display);
		$url = $koneksi->real_escape_string($url);
		$icon = $koneksi->real_escape_string($icon);
		$parent = $koneksi->real_escape_string($parent);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "INSERT INTO tbl_menus (display,menu_url,menu_icon,menu_type,parent_display) VALUES (?,?,?,?,NULLIF('".$parent."',''))";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("sssi",$display,$url,$icon,$type);
			$update_pass->execute();
			$update_pass->close();
	
			$log = "$user : Melakukan action Register Menu baru dengan Nama ($display).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register Menu Failed.'));
			///echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
