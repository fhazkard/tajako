<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$menu_id = $_POST['menu_id'];;
		$display = $_POST['display'];
		$url = $_POST['url'];
		$icon = $_POST['icon'];
		$type = $_POST['type'];
		$parent = $_POST['parent'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		$display = $koneksi->real_escape_string($display);
		$url = $koneksi->real_escape_string($url);
		$icon = $koneksi->real_escape_string($icon);
		$parent = $koneksi->real_escape_string($parent);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_menus SET display=?, menu_url=?, menu_icon=?, menu_type=?, parent_display=NULLIF('".$parent."','') WHERE menu_id=? ";		
			$update_menu = $koneksi->prepare($query);
			$update_menu->bind_param("sssii",$display,$url,$icon,$type,$menu_id);
			$update_menu->execute();
			$update_menu->close();
	
			$log = "$user : Melakukan action Edit Detail Menu $display dengan ID ($menu_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Edit Detail Role Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
