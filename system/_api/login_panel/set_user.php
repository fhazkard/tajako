<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$user_id = $_POST['user_id'];;
		$user_nama = $_POST['nama'];
		$user_role = $_POST['role'];
		$user = $_SESSION['nama'];
		$timestamp = date('YmdHis');
		$user_nama = $koneksi->real_escape_string($user_nama);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions

			$query = "UPDATE tbl_users SET user_nama=?, user_role=? WHERE user_id=? ";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("sis",$user_nama,$user_role,$user_id);
			$update_pass->execute();
			$update_pass->close();
	
			$log = "$user : Melakukan action Edit Detail Account dengan ID ($user_id).";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Edit Detail Account Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
