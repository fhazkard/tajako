<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../function/make_token.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$user_nama = $_POST['nama'];
		$user_role = 4;
		$pass = md5($_POST['pass']);
		$timestamp = date('YmdHis');
		$user_id = make_token_uniq_ID($timestamp);
		$user_nama = $koneksi->real_escape_string($user_nama);
		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}	
		try {
			$koneksi->autocommit(FALSE); //turn on transactions
			$query = "INSERT INTO tbl_users (user_id,user_nama,pass,user_role) VALUES (?,?,?,?)";		
			$update_pass = $koneksi->prepare($query);
			$update_pass->bind_param("sssi",$user_id,$user_nama,$pass,$user_role);
			$update_pass->execute();
			$update_pass->close();
	
			$log = "$user_nama : Melakukan action Register Account Sendiri.";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();

			$koneksi->autocommit(TRUE); //turn off transactions + commit queued queries
			echo json_encode(array('code'=>'200','note'=>'Success'));
		  } catch(Exception $e) {
			$koneksi->rollback(); //remove all queries from queue if error (undo)
			echo json_encode(array('code'=>'500','note'=>'Register Account Failed.'));
			//echo $e->getMessage();
		  }				
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
