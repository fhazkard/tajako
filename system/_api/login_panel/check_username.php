<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$usernama = $_POST['usernama'];

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		$check = 0;
		$query = "SELECT EXISTS (SELECT * FROM tbl_users WHERE user_nama = ? ) AS data";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("s",$usernama);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
				$check =  $data->data;
		}
		$select_data->free_result();
		if($check == 1){
			die(json_encode(array('code'=>'402','note'=>'Duplicate Username.')));
		}else{
			echo json_encode(array('code'=>'200','note'=>'Ok'));
		}
	
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
