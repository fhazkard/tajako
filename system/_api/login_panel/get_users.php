<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	if(isset($_POST['token_key']) ){
		$token_key = $_POST['token_key'];
		$web = $_POST['web'];
		if(!empty($token_key)){
			$resultArray = array();
 			$tempArray = array();
			$query = "SELECT *, tbl_users.active AS ac FROM tbl_users JOIN ".
			"tbl_roles ON tbl_users.user_role = tbl_roles.role_id WHERE tbl_roles.role_id NOT IN (1)";
			$select_data = $koneksi->prepare($query);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$tempArray = $data;
				array_push($resultArray, $tempArray);
			}
			$select_data->free_result();
			if($web == 'true'){
				echo json_encode(array('data'=>$resultArray)); 
			}else{
				echo json_encode($resultArray);
			}
		}else{
			echo json_encode(array('code'=>'401','note'=>'Bad Token'));
		}					
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
