<?php
session_start(); 
error_reporting(0);
if($_SERVER['REQUEST_METHOD']=='POST'){
	require_once '../../_core/koneksi.php';
	require '../../_core/strep.php';
	if(isset($_POST['token_key']) ){
		date_default_timezone_set('Asia/Jakarta');
		$token_key = $_POST['token_key'];
		$usernama = $_POST['usernama'];
		$pass = $_POST['pass'];
		$timestamp = date('YmdHis');

		if(empty($token_key)){
			die(json_encode(array('code'=>'401','note'=>'Bad Token')) );
		}

		$usernama = cek_string($usernama);
		$pass = cek_string($pass);
		if(empty($usernama) || empty($pass)){
			die(json_encode(array('code'=>'402','note'=>'You Must Input Username and Password!')) );
		}

		$usernama = $koneksi->real_escape_string($usernama);
		$pass = $koneksi->real_escape_string($pass);
		$pass = md5($pass);

		$ada_data = 0;
		$user_id = "";
		$role_id = 0;
		$role = "";
		$active_user = 0;
		$active_role = 0;
		$menu = [];
		$parent = [];

		$query = "SELECT user_id,user_nama,tbl_users.active AS ua,role_id,role_nama,tbl_roles.active AS ra ". 
		"FROM tbl_users JOIN tbl_roles ON tbl_roles.role_id = tbl_users.user_role WHERE user_nama=? AND pass=?";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("ss",$usernama,$pass);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			$user_id = $data->user_id;
			$usernama = $data->user_nama;
			$active_user = $data->ua;
			$role_id = $data->role_id;
			$role = $data->role_nama;
			$active_role = $data->ra;
			$ada_data ++;
		}
		$select_data->free_result();
		$select_data->close();

		if($ada_data == 0){
			die(json_encode(array('code'=>'403','note'=>'Login Fail!')) );
		}

		if($role_id == 4){
			$verifikasi = 0;
			$query = "SELECT verifikasi FROM tbl_customers WHERE user_id=?";
			$select_data = $koneksi->prepare($query);
			$select_data->bind_param("s",$user_id);
			$select_data->execute();
			$result = $select_data->get_result();	
			while ($data = $result->fetch_object()) {
				$verifikasi = $data->verifikasi;
			}
			$select_data->free_result();
			$select_data->close();
	
			if($verifikasi == 0){
				die(json_encode(array('code'=>'404','note'=>'Your Account Not Active, Please Check Your Email!')) );
			}
		}

		$query = "SELECT * FROM tbl_access JOIN tbl_menus ON tbl_access.menu_id = tbl_menus.menu_id WHERE role_id =? ORDER BY access DESC, access_ID, parent_display";
		$select_data = $koneksi->prepare($query);
		$select_data->bind_param("i",$role_id);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			array_push($menu,$data);
		}
		$select_data->free_result();
		$select_data->close();

		if(count($menu) == 0){
			die(json_encode(array('code'=>'405','note'=>'Account Dont Have Access!')) );
		}

		$query = "SELECT menu_icon,parent_display FROM tbl_menus WHERE parent_display IS NOT NULL OR parent_display='' GROUP BY parent_display ORDER BY  parent_display";
		$select_data = $koneksi->prepare($query);
		$select_data->execute();
		$result = $select_data->get_result();	
		while ($data = $result->fetch_object()) {
			array_push($parent,$data);
		}
		$select_data->free_result();
		$select_data->close();

		if($active_user == 1 && $active_role ==  1){
			$_SESSION['limit'] = time() + 10800;
			$_SESSION['user_id'] = $user_id;
			$_SESSION['nama'] = $usernama;
			$_SESSION['role'] = $role;
			$_SESSION['menu'] = $menu;
			$_SESSION['parent'] = $parent;
	
			$log = "$usernama : Melakukan action Login Dashboard.";
			$query = "INSERT INTO tbl_logs (desk_log,timestamp) VALUES ( ?, ?) ";
			$insert_log = $koneksi->prepare($query);
			$insert_log->bind_param("ss",$log,$timestamp);
			$insert_log->execute();
			$insert_log->close();
	
			echo json_encode(array('code'=>'200','note'=>'Success'));
		}else{
			echo json_encode(array('code'=>'500','note'=>'Login Failed! Your Account Inactive State!'));
		}			
	}else{
		echo json_encode(array('code'=>'440','note'=>'Bad Request'));
	}
	$koneksi->close();
}
?>
