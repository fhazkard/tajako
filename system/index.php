<?php
	session_start();
	unset($_SESSION['token']);	
	unset($_SESSION['limit']);
	unset($_SESSION['user_id']);
	unset($_SESSION['nama']);
	unset($_SESSION['role']);
	unset($_SESSION['menu']);
	unset($_SESSION['parent']);
	session_destroy();
	die("<script>window.location = '../'</script>");
?>