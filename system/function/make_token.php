<?php
//untuk membuat token key
function make_token_key() {
	$text = "";
	$possible = "A0B1C2D3E4F5G6H7I8J9";
	for ($i = 0; $i < 20; $i++){
		$no =  rand(1,20);
		$teks = substr($possible,$no,1);
		$text = $text.''.$teks;
	}
	return $text;
}

function make_kode_uniq($keys) {
	$text = "";
	$possible = "02468".$keys."97531";
	for ($i = 0; $i < 8; $i++){
		$no =  rand(1,strlen($possible));
		$teks = substr($possible,$no,1);
		$text = $text.''.$teks;
	}
	return $text;
}

function make_token_uniq_ID($keys) {
	$text = "";
	$possible = "A0B1C2D3E4F5G6H7I8J9".$keys."acbedfhgjiklponmqutrswvxyz";
	for ($i = 0; $i < 50; $i++){
		$no =  rand(0,strlen($possible));
		$teks = substr($possible,$no,1);
		$text = $text.''.$teks;
	}
	return $text;
}
function make_code_uniq_ID($keys) {
	$text = "";
	$possible = "A0B1C2D3E4F5G6H7I8J9".$keys."ZXVNLQWRTYUP";
	while(strlen($text) < 9){
		$no =  rand(0,strlen($possible));
		$teks = substr($possible,$no,1);
		$text = $text.''.$teks;
		if(strlen($text) == 8){
			$left = substr($text, 0, 4);
			$right = substr($text, 4, 6);
			return $left.'-'.$right;
			break;
		}
	}
}
?>