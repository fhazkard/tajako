function formatTableCustomer ( data ) {
	// `d` is the original data object for the row
	var table_start ='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
		'<thead>'+
		'<tr>'+
		'<th>Card ID</th>'+
		'<th>Jabatan</th>'+
		'<th>Alamat</th>'+
		'<th>Info</th>'+
		'<th>Point</th>'+
		'<th>Verifikasi</th>'+
		'<th>Blacklist</th>'+
		'</tr>'+
		'</thead><tbody>';
	var table_row = '';
	var table_end = '</tbody></table>';
	var bl = 'No';
	var vr = "<span style='font-weight: bold;color:red;'>No</span>";
	if(data.blacklist == 1){
		bl = 'Yes';
	}
	if(data.verifikasi == 1){
		vr = "<span style='font-weight: bold;color:green;'>Yes</span>";
	}
	//for(var x = 0; x < data.qty; x++){
	//	var item = 'item'+x;
		table_row = table_row+
		'<tr class="row_detail">'+
			'<td>'+data.card_id+'</td>'+
			'<td>'+data.jabatan+'</td>'+
			'<td>'+data.alamat+'</td>'+
			'<td>'+data.customer_info+'</td>'+
			'<td>'+data.points+' Point</td>'+
			'<td>'+vr+'</td>'+
			'<td>'+bl+'</td>'+
        '</tr>';
	//}
    return table_start+table_row+table_end;
}

function getDate(sp){
	today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //As January is 0.
	var yyyy = today.getFullYear();
	var time = today.getHours() +today.getMinutes() + today.getSeconds();
	
	if(dd<10) dd='0'+dd;
	if(mm<10) mm='0'+mm;
	return (mm+sp+dd+sp+yyyy+' '+time);
};

var formatStandar = {
	exportOptions: {
		format: {
			body: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				return column > 4 ?
					data.replace( /[$,Rp.]/g, '' ) :
					data;
			},
			footer: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				if(column === 5){
					return column === 5 ?
					data.replace( /[$,Rp.]/g, '' ) :
					data;
				}else{
					return column < 4 ?
					data.replace(data, '' ) :
					data;
				}
			}
		}
	}
};

var formatPDF = {
	exportOptions: {
		format: {
			footer: function ( data, column, row, node ) {
				// Strip $ from salary column to make it numeric
				return column < 4 ?
				data.replace(data, '' ) :
				data;				
			}
		}
	}
};