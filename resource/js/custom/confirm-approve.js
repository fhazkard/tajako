function confirm_approve(rent_id){
    var user = $('#username_id').text();
    $('#rentorder_'+rent_id).on('click', function(e) {
    e.preventDefault();
    swal({
        title: "Confirmation Approve This Rent Order",
        text: "Are You Sure Want Approve This Rent Order?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, Im Sure!',
        cancelButtonText: "No, Cancel!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
    function(isConfirm) {
        if (isConfirm) {
            swal({
                title: "Processing Order...",
                text: "Please wait for while...",
                imageUrl: "../../resource/img/ajax-loader.gif",
                showConfirmButton: false,
                allowOutsideClick: false
              });
            $.ajax({
                type: "POST",
                url: "http://103.246.0.8:9876/api_one/rent/rent_order/set_rentorder.php",
                data: {'token_key':123, 'rent_id':rent_id,'web':'true','user':user},
                success: function(msg) {
                    var data = JSON.parse(msg);
                    if(data.note=="Success"){
                        swal({title: "Success",text: "Approve Rent Order Success!",type: "success"}, 
                        function() {window.location = "../../dashboard/rent_order/";
                        });               
                    }else{
                        swal({title: "Failed",text: "Approve Rent Order Failed! - "+data.note,type: "error"}, 
                        function() {window.location = "../../dashboard/rent_order/";
                        });
                    }
                }
            });
        }
        else {
            swal("Canceled", "You Cancel Confirmation!", "error");
        }
    });
});
}
