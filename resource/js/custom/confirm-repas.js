function confirm_repas(id){
    var user = $('#username_id').text();
    var url = path+"/system/_api/login_panel/repas.php";
    $('#form_repas_login_'+id).on('click', function(e) {
        e.preventDefault();
        swal({
            title: "Confirmation Reset Password",
            text: "Are You Sure To Reset Password This Account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Sure!',
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'token_key':123, 'id':id,'user':user},
                    success: function(msg) {
                        var data = JSON.parse(msg);
                        if(data.note=="Success"){
                            swal({title: "Success",text: "Reset Password Success!",type: "success"}, 
                            function() {window.location = window.location.href;
                            });
                        }else{
                            swal({title: "Failed",text: "Reset Password Failed!",type: "error"}, 
                            function() {window.location = window.location.href;
                            });
                        }
                    }
                });
            }
            else {
                swal("Cancelled", "You Cancel Confirmation!", "error");
            }
        });
    });
}
	