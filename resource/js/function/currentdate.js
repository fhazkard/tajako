function currentToday() {
	var today = new Date();
	var dd = addZero(today.getDate());
	var mm = addZero(today.getMonth()+1);
	var yyyy = today.getFullYear();
	return today = yyyy+'-'+mm+'-'+dd;
	//document.getElementById("datefield").setAttribute("max", today);
}

function currentFullToday() {
	var today = new Date();
	var h = addZero(today.getHours());
	var m = addZero(today.getMinutes());
	var d = addZero(today.getDate());
	var mm = today.getMonth();
	var yyyy = today.getFullYear();
	var date = new Date(yyyy, mm, 01);  // 2009-11-10
	var month = date.toLocaleString('default', { month: 'long' });

	return today = d+' '+month+' '+yyyy+' '+h+':'+m;
}

function addZero(i) {
	if (i < 10) {
	  i = "0" + i;
	}
	return i;
}