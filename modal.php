<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="display:grid;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Register New Account</h4>
            </div>
            <div class="modal-body">
				<form id="form_register_account" method="POST" class="form-horizontal form-label-left">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="user_nama" class="form-control" placeholder="Input Username Login" required autofocus onblur="this.value=removeSpaces(this.value);" title="Input Username No Space">
								<small style="color:red;">*Required</small>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Password:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="password" id="user_pass" class="form-control" placeholder="Input Strong Password" required autofocus title="Please Use Symbol Character">
								<small style="color:red;">*Required</small> 
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Full Name:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="nama_add" class="form-control" placeholder="Input Full Name" required autofocus maxlength=30 minlength=4 >
								<small style="color:red;">*Required</small>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Mobile Phone:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="tel" id="hp_add" class="form-control" placeholder="Input Mobile Phone (08xxxxxx~)" required autofocus  maxlength=20 minlength=8 pattern="[0-9]+" title="Input Number Only">
								<small style="color:red;">*Required</small>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="email" id="email_add" class="form-control" placeholder="Input Valid Email" required autofocus maxlength=50 minlength=8 title="You Must Verification Email!">
								<small style="color:red;">*Required</small>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Card Number:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="tel" id="card_add" class="form-control" placeholder="Input Card ID Number" autofocus  maxlength=25 minlength=8 pattern="[0-9]+" title="Input Number Only">
							</div>						
						</div>
							
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:left;">Type:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="type_add" class="form-control select_box"></select>
							</div>						
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:left;">Country:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="wn_add" class="form-control">
									<option value="0">Indonesia</option>
									<option value="1">Another Country</option>
								</select>
							</div>						
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:left;">Pekerjaan:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="jabatan_add" class="form-control" placeholder="Input Pekerjaan" required autofocus maxlength=40 minlength=4 >
								<small style="color:red;">*Required</small>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:left;">Alamat:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="alamat_add" class="form-control" rows="3" style="resize: none;"></textarea>
							</div>						
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-right:0px;text-align:left;">Info:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="info_add" class="form-control" rows="3" style="resize: none;"></textarea>
							</div>						
						</div>	
					</div>	
            </div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
					<button type="submit" class="btn btn-success">Register Account</button>
			</div>
				</form>
		</div>
	</div>
</div>

<div class="modal fade login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Login Your Account</h4>
            </div>
            <div class="modal-body">
				<form id="form_login" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="userlogin" class="form-control" placeholder="Username" autofocus required maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Password:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="password" id="passlogin" class="form-control" placeholder="Password" required maxlength="16" minlength="4">
						</div>						
					</div>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Cancel</button>
				<button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Login</button>
			</div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade faq" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Posting Question</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_faq" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Question:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="faq" class="form-control" rows="4" style="resize: none;" placeholder="Please Input Your Question Here..." required maxlength="256" minlength="8"></textarea>
						</div>						
					</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Cancel</button>
				<button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Post Question</button>
			</div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade comments" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Comment Question</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_comments" class="form-horizontal form-label-left">
					<input type="hidden" id="faq_id">	
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Comment:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="comments" class="form-control" rows="2" style="resize: none;" placeholder="Please Input Your Comment Here..." required maxlength="128" minlength="4"></textarea>
						</div>						
					</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Cancel</button>
				<button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Post Comment</button>
			</div>
                </form>
        </div>
    </div>
</div>
