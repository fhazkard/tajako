var path = null;
var COMMENT = [];

$(document).ready(function(){
	path = getpath();
	loadType();
	LoadSlide();
	LoadArticle();
	LoadKonsultan();
	LoadPaket();
	LoadKontak();
	Get_ListFAQ();
});

$(document).on("click", "#link_comment", function () {
	var faq_id = $(this).data('faq_id');
	$(".comments #faq_id").val( faq_id );
	COMMENT = [];
	var komentar = $(this).data('komentar');
	if(komentar != "" || komentar.length != 0){
		COMMENT.push(komentar);
	}
});

function loadType(){
	var url = path+"/system/_api/type_customer/select_type.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Type Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#type_add').append($('<option>', { 
					value: item.type_id,
					text : item.type_name 
				}));
			});	
		}
	});
}

function Get_ListFAQ(){
	var url = path+"/system/_api/data_faq/get_faq.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data FAQ Null: ", err.message);
			}
			if(data.data.length != 0){
				$('.listfaq').remove();
				data.data.forEach(el => {
					var row_faq = el.faq_id;
					var year        = el.timestamp.substring(0,4);
					var month       = el.timestamp.substring(4,6);
					var day         = el.timestamp.substring(6,8);
					var hour         = el.timestamp.substring(8,10);
					var minute         = el.timestamp.substring(10,12);
					var date = new Date(year, month-1, 01);
					month = date.toLocaleString('default', { month: 'long' });
					var tgl = day+' '+month+' '+year+' '+hour+':'+minute;

					var komentar = [];
					try {
						komentar = JSON.parse(el.comments);
					}
					catch(err) {
						//console.log("Data komentar Null: ", err.message);
					}
					//console.log(komentar);

					var awal = '<div class="faq-item listfaq">';
					var akhir = '</div></div></div></div>';
					var isi_faq = '<div class="row">'+
							'<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">'+
								'<a data-toggle="collapse" href="#'+row_faq+'" class="faq-question">'+el.title+'</a>'+
								'<p>Created by <strong>'+el.user_nama+'</strong> - '+tgl+' <i class="fas fa-calendar-check"></i></p>'+
							'</div>'+
							'<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">'+
								'<a href="" id="link_comment" class="faq-question cm" data-toggle="modal" data-target=".comments" data-backdrop="static" data-keyboard="false" data-faq_id="'+el.faq_id+'" data-faq_id="'+el.faq_id+'" data-komentar="'+komentar+'" ><span class="large font-bold">Comment Now</span></a><br/>'+
								'<span class="large font-bold cm_row">'+el.baris+' <i class="fas fa-comment-dots"></i></span>'+
							'</div>'+
						'</div>'+
						'<div class="row">'+
							'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
								'<div id="'+row_faq+'" class="panel-collapse collapse ">';
									
					var isi_komen = '';
					komentar.forEach(item => {
						isi_komen = isi_komen+'<div class="faq-answer fans">'+item+'</div>';
					});
					
					var panel_faq = awal+isi_faq+isi_komen+akhir;
					$('#row_listfaq').append(panel_faq);		
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function LoadSlide(){
	var url = path+"/system/_api/data_slide/get_slide.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Slide Null: ", err.message);
			}
			var ls = 0;
			if(data.data.length != 0){
				$('.listslide').remove();
				data.data.forEach(el => {
					//var row_id = 'row'+el.slide_id;
					var actives = "";
					if(el.sort == 1){
						actives = "active";
					}
					var slogan        = el.slogan.replace(/@/gi,"<br/>");
					var link = '\''+ el.link+  '\'';

					var slide = '<div class="item '+actives+'">'+
						'<div class="container">'+
							'<div class="carousel-caption blank">'+
								'<h1>'+slogan+'</h1>'+
								'<p><a class="btn btn-lg btn-primary" href="'+el.url+'" role="button">'+el.buton+'</a></p>'+
							'</div>'+
						'</div>'+
							'<div class="header-back" style="background: url('+link+') 50% 0 no-repeat;"></div>'+
						'</div>';

					$('#row_slide').append(slide);	
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function LoadArticle(){
	var url = path+"/system/_api/data_article/get_article.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Article Null: ", err.message);
			}
			var ls = 0;
			if(data.data.length != 0){
				$('.listarticle').remove();
				data.data.forEach(el => {
					var row_id = 'row'+el.article_id;
					var year        = el.timestamp.substring(0,4);
					var month       = el.timestamp.substring(4,6);
					var day         = el.timestamp.substring(6,8);
					var hour         = el.timestamp.substring(8,10);
					var minute         = el.timestamp.substring(10,12);
					var date = new Date(year, month-1, 01);
					month = date.toLocaleString('default', { month: 'long' });
					var tgl = day+' '+month+' '+year+' '+hour+':'+minute;
					var x = ls%2;
					var div_atas = "";
					var div_bwh = "pull-right";
					if(x==0){
						div_atas = 'pull-right'
						div_bwh = '';					
					}

					var article = '<div  class="row features-block listarticle">'+
							'<div class="col-lg-6 features-text wow fadeInLeft '+div_atas+'" style="margin-bottom: 10px;">'+
								'<small>'+tgl+' | Author : '+el.create_by+'</small>'+
								'<h2>'+el.title+'</h2>'+
								'<p>'+el.intro+'</p>'+
								'<a data-toggle="collapse" href="#'+row_id+'" class="btn btn-primary">Read more</a>'+
							'</div>'+
							'<div class="col-lg-6 text-right wow fadeInRight" style="display:grid;">'+
								'<img src="'+el.img+'" alt="tajako.com" class="img-responsive '+div_bwh+'">'+
							'</div>'+
						'</div>'+
						'<div class="row features-block">'+
							'<div id="'+row_id+'" class="col-lg-12 col-md-12 panel-collapse collapse" >'+
								'<div class="article_teks">'+
									'<p>'+el.isi+' <a href="'+el.sumber+'" style="font-style: italic;">Sumber</a></p>'+
								'</div>'+
							'</div>'+
						'</div>';

					$('#row_article').append(article);	
					ls++;	
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function LoadKonsultan(){
	var url = path+"/system/_api/data_konsultan/top3_konsultan.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Konsultan Null: ", err.message);
			}
			var ls = 0;
			if(data.data.length != 0){
				$('.row_konsultan').remove();
				data.data.forEach(el => {
					//var row_id = 'row'+el.slide_id;
					var info        = el.info.split("@");

					var konsultan = '<div class="col-sm-4 wow bounceInUp row_konsultan">'+
					'<div class="team-member">'+
						'<img src="'+el.photo+'" class="img-responsive img-circle img-small" alt="">'+
						'<h4><span class="navy">'+el.nama+'</span></h4>'+
						'<p>'+info[0]+'</p>'+
						'</div>'+
					'</div>';

					$('#list_konsultan').append(konsultan);	
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function LoadPaket(){
	var url = path+"/system/_api/data_paket/load_paket.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Paket Null: ", err.message);
			}
			var ls = 0;
			if(data.data.length != 0){
				$('.item_paket').remove();
				data.data.forEach(el => {
					//var row_id = 'row'+el.slide_id;
					var selek = "";
					if(el.sort == 2){
						selek = "selected";
					}
					var info        = el.paket_info.split("@");

					var paket = '<div class="col-lg-4 wow zoomIn item_paket">'+
					'<ul class="pricing-plan list-unstyled '+selek+'">'+
						'<li class="pricing-title">'+el.paket_name+'</li>'+
						'<li class="pricing-desc">'+info[0]+'</li>'+
						'<li class="pricing-price"><span>Rp. '+numeral(el.harga).format('0,0')+'</span> / '+el.durasi+' Menit</li>'+
						'<li>'+info[1]+'</li>'+                   
						'<li><a class="btn btn-primary btn-xs" href="#" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false">Register</a></li>'+
					'</ul>'+
				'</div>';

					$('#row_paket').append(paket);	
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function LoadKontak(){
	var url = path+"/system/_api/data_kontak/get_kontak.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Kontak Null: ", err.message);
			}
			var ls = 0;
			if(data.data.length != 0){
				$('.listkontak').remove();
				data.data.forEach(el => {
					//var row_id = 'row'+el.kontak_id;
					var hp        = el.hp.split("@");
					var mobile = "";
					hp.forEach(el => {
						mobile = mobile+'<abbr title="HandPhone">Contact / WhatsApp : </abbr>'+el+'<br/>';
					});

					var kontak = '<div class="listkontak">'+
					'<div class="col-lg-3 col-lg-offset-3">'+
						'<address>'+
							'<strong><span class="navy">Tajako.com</span></strong><br/>'+mobile+
						'</address>'+
					'</div>'+
					'<div class="col-lg-4">'+
						'<strong><span class="navy">Address :</span></strong><br/>'+
						'<p class="text-color">'+el.alamat+'</p>'+
					'</div>'+
				'</div>';

					$('#row_kontak').append(kontak);	
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function removeSpaces(string) {
	return string.split(' ').join('');
}

$('#user_nama').focusout(function(){
	var usernama = $('.modal-body #user_nama').val();
	var url = path+"/system/_api/login_panel/check_username.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','usernama':usernama},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Check Null: ", err.message);
			}
			if(data['code'] == "402"){
				swal({title: "Error",text: "Username Already Used!",type: "error"});
				console.log("Data Username Duplicate: ", data);
			}
		}
	});
});

$('#form_register_account').submit(function(){
	swal({
		title: "Register Account...",
		text: "Please wait for while...",
		imageUrl: "resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	var usernama = $('.modal-body #user_nama').val();
	var pass = $('.modal-body #user_pass').val();

	var nama = $('.modal-body #nama_add').val();
	var hp = $('.modal-body #hp_add').val();
	var email = $('.modal-body #email_add').val();
	var card = $('.modal-body #card_add').val();
	var jabatan = $('.modal-body #jabatan_add').val();
	var alamat = $('.modal-body #alamat_add').val();
	var types = $('.modal-body #type_add').val();
	var wn = $('.modal-body #wn_add').val();
	var info = $('.modal-body #info_add').val();
	var url = path+"/system/_api/data_customer/reg_customer.php";

	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'usernama':usernama,'pass':pass,'nama':nama,'hp':hp,'email':email,'card':card,
		'jabatan':jabatan,'alamat':alamat,'types':types,'wn':wn,'info':info},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Account Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register Account Success. Please Check Your Email!",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register Account Failed - "+data.note,type: "error"}/* , 
						function() {window.location = window.location.href;
						} */);
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_login').submit(function(){
	var usernama = $('.login #userlogin').val();
	var pass = $('.login #passlogin').val();
	var url = path+"/system/_api/login_panel/login_user.php";

	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'usernama':usernama,'pass':pass},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data User Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Login Account Success.",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Login Account Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_tambah_faq').submit(function(){
	var faq = $('.faq #faq').val();
	var pass = $('#logout').val();
	if(pass === undefined){
		pass = false;
	}else{
		pass = true;
	}
	var url = path+"/system/_api/data_faq/reg_faq.php";

	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'faq':faq,'pass':pass},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data FAQ Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Add New Question Success.",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Add New Question Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_tambah_comments').submit(function(){
	var faq_id = $('#faq_id').val();
	var pass = $('#logout').val();
	var comments = $('.comments #comments').val();
	var usernama = $('#usernama').val();
	if(pass === undefined){
		pass = false;
	}else{
		pass = true;
	}

	var tgl_komen = currentFullToday();
	var row_1 = usernama+' - '+tgl_komen;
	var komen = '<p><strong>'+row_1+'</strong></p><p>'+comments+'</p>';
	//var lists = [];
	//lists.push(komen)
	COMMENT.push(komen);
	var baris = COMMENT.length;

	var url = path+"/system/_api/data_faq/update_comment.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'faq_id':faq_id,'pass':pass,'lists':JSON.stringify(COMMENT),'baris':baris},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Comment Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Post New Comment Success.",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Post New Comment Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});
