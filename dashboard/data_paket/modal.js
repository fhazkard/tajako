var path = null;

$(document).on("click", "#edit_paket", function () {
	var paket_id = $(this).data('paket_id');
	$(".modal-body #paket_id").val( paket_id );

	var sort = $(this).data('sort');
	$(".modal-body #sort_edit").val( sort );
					
	var nama = $(this).data('nama');
	$(".modal-body #nama_edit").val( nama );
					
	var harga = $(this).data('harga');
	$(".modal-body #harga_edit").val( harga );

	var durasi = $(this).data('durasi');
	$(".modal-body #durasi_edit").val( durasi );

	var type = $(this).data('type');
	$(".modal-body #type_edit").val( type );

	var info = $(this).data('info');
	$(".modal-body #info_edit").val( info );

	var active = $(this).data('active');
	$(".modal-body #active").val( active );
});

$('#form_tambah_paket').submit(function(){
	var sort = $('.modal-body #sort_add').val();
	var nama = $('.modal-body #nama_add').val();
	var harga = $('.modal-body #harga_add').val();
	var durasi = $('.modal-body #durasi_add').val();
	var type = $('.modal-body #type_add').val();
	var info = $('.modal-body #info_add').val();
	var url = path+"/system/_api/data_paket/reg_paket.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','sort':sort,'nama':nama,'harga':harga,'durasi':durasi,'type':type,'info':info},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data paket Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register paket Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register paket Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_paket').submit(function(){
	var id = $('.modal-body #paket_id').val();
	var sort = $('.modal-body #sort_edit').val();
	var nama = $('.modal-body #nama_edit').val();
	var harga = $('.modal-body #harga_edit').val();
	var durasi = $('.modal-body #durasi_edit').val();
	var type = $('.modal-body #type_edit').val();
	var info = $('.modal-body #info_edit').val();
	var active = $('.modal-body #active').val();
	var url = path+"/system/_api/data_paket/set_paket.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','paket_id':id,'sort':sort,'nama':nama,'harga':harga,'durasi':durasi,'type':type,'info':info,'active':active},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data paket Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data paket Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data paket Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	loadType();
	$("input[type='number']").inputSpinner();
});

function loadType(){
	var url = path+"/system/_api/type_paket/select_type.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Type Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#type_add').append($('<option>', { 
					value: item.type_id,
					text : item.type_name 
				}));
				$('#type_edit').append($('<option>', { 
					value: item.type_id,
					text : item.type_name 
				}));
			});	
		}
	});
}
	
function loadTable(){
	var url = path+"/system/_api/data_paket/get_paket.php";
	$('#tabel_paket').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "paket_id",
			"visible":false
		},{
			"data": "sort"
		},{
			"data": "paket_name"
		},{
			"data": "harga",
			render: function ( data, type, row ) {
				return 'Rp. '+numeral(data).format('0,0');
			}	
		},{
			"data": "durasi",
			render: function ( data, type, row ) {
				return data+' Menit';
			}			
		},{
			"data": "type_name"			
		},{
			"data": "paket_info"			
		},{
			"data": "active",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:red;'>Inactive</span>";
				}else{
					return "<span style='font-weight: bold;color:green;'>Active</span>";
				}
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_paket' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-paket_id='"+data.paket_id+"'  data-info='"+data.paket_info+"' data-type='"+data.types+"' data-active='"+data.active+"' "+
					"data-nama='"+data.paket_name+"' data-harga='"+data.harga+"' data-durasi='"+data.durasi+"' data-sort='"+data.sort+"' > "+
					"<i class='fas fa-edit'></i> Edit</a> ";
					return edit;

			}
		}]
	});
}
