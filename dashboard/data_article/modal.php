<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add New Article</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_article" method="POST" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Urutan:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="sort_add" class="form-control" placeholder="Input No Urutan" required autofocus min="0" max="5" value=0 step="1">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Title Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="title_add" class="form-control" placeholder="Input Title Article" required autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Intro Article:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="intro_add" class="form-control" rows="3" style="resize: none;" placeholder="Please Input Intro Article Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Image Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="img_add" class="form-control" placeholder="Input URL Image Article" autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Detail Article:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="isi_add" class="form-control" rows="5" style="resize: none;" placeholder="Please Input Detail Article Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Source Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="sumber_add" class="form-control" placeholder="Input URL Source Article" autofocus >
						</div>						
					</div>
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Add Article</button>
			</div>
				</form>
		</div>
	</div>
</div>


				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Article</h4>
			</div>
			
            <div class="modal-body">
				<form id="form_edit_article" method="POST" class="form-horizontal form-label-left">
					<input id="article_id" type="hidden"> 
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Urutan:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="sort_edit" class="form-control" placeholder="Input No Urutan" required autofocus min="0" max="5" value=0 step="1">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Title Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="title_edit" class="form-control" placeholder="Input Title Article" required autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Intro Article:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="intro_edit" class="form-control" rows="3" style="resize: none;" placeholder="Please Input Intro Article Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Image Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="img_edit" class="form-control" placeholder="Input URL Image Article" autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Detail Article:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="isi_edit" class="form-control" rows="5" style="resize: none;" placeholder="Please Input Detail Article Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Source Article:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="sumber_edit" class="form-control" placeholder="Input URL Source Article" autofocus >
						</div>						
					</div>
			</div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Article</button>
			</div>
                </form>
		</div>
	</div>
</div>
		<script src="modal.js"></script>