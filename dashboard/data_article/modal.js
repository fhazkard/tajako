var path = null;

$(document).on("click", "#edit_article", function () {
	var article_id = $(this).data('article_id');
	$(".edit #article_id").val( article_id );

	var sort = $(this).data('sort');
	$(".edit #sort_edit").val( sort );

	var title = $(this).data('title');
	$(".edit #title_edit").val( title );

	var intro = $(this).data('intro');
	$(".edit #intro_edit").val( intro );

	var img = $(this).data('img');
	$(".edit #img_edit").val( img );
					
	var isi = $(this).data('isi');
	$(".edit #isi_edit").val( isi );

	var sumber = $(this).data('sumber');
	$(".edit #sumber_edit").val( sumber );

});

$('#form_tambah_article').submit(function(){
	var sort = $('.tambah #sort_add').val();
	var title = $('.tambah #title_add').val();
	var intro = $('.tambah #intro_add').val();
	var img = $('.tambah #img_add').val();
	var isi = $('.tambah #isi_add').val();
	var sumber = $('.tambah #sumber_add').val();

	var url = path+"/system/_api/data_article/reg_article.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','sort':sort,'title':title,'intro':intro,'img':img,'isi':isi,'sumber':sumber},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Article Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register Article Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register Article Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_article').submit(function(){
	var article_id = $('.edit #article_id').val();
	var sort = $('.edit #sort_edit').val();
	var title = $('.edit #title_edit').val();
	var intro = $('.edit #intro_edit').val();
	var img = $('.edit #img_edit').val();
	var isi = $('.edit #isi_edit').val();
	var sumber = $('.edit #sumber_edit').val();

	var url = path+"/system/_api/data_article/set_article.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','sort':sort,'article_id':article_id,'title':title,'intro':intro,'img':img,'isi':isi,'sumber':sumber},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Article Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data Article Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Article Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	$("input[type='number']").inputSpinner();
});

function loadTable(){
	var url = path+"/system/_api/data_article/load_article.php";
	$('#tabel_article').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 5, // your case first column
				"className": "text-center",
				"width": "5%"
		   },{
			"targets": 2, // your case first column
			"className": "text-center",
			"width": "10%"
	   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "article_id",
			"visible":false
		},{
			"data": "sort"
		},{
			"data": "timestamp",
			render: function ( data, type, row ) {
				var year        = data.substring(0,4);
				var month       = data.substring(4,6);
				var day         = data.substring(6,8);
				var hour         = data.substring(8,10);
				var minute         = data.substring(10,12);
				return year+'-'+month+'-'+day+' '+hour+':'+minute;
			}		
		},{
			"data": "title"
		},{
			"data": "intro"		
		},{ "data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_article' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-img='"+data.img+"' data-sort='"+data.sort+"' data-isi='"+data.isi+"' data-sumber='"+data.sumber+"' "+
					"data-article_id='"+data.article_id+"' data-title='"+data.title+"' data-intro='"+data.intro+"' > "+
					"<i class='fas fa-edit'></i> Edit</a> ";
					return edit;
			}
		}]
	});
}
