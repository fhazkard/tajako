<?php 
  session_start(); 
  error_reporting(0);
  include "../header.php";
?>
    <style>
        .product-imitation{
            padding: 60px 30px 60px 30px;
            font-size:40px;
        }
        .product-name{
            font-size:18px;
        }
        .text-muted{
            font-size:14px;
        }
        .label_new_order{
            font-size:20px;
            float:left;
            margin-left: 0px;
        }
        .btn_confirm{
            width: -webkit-fill-available;
        }
    </style> 

        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="font-bold">Live Order Dasboard</h2>
                </div>
            </div>
            <hr/>
            
            <div class="row" id="row_listorder"></div><!-- row -->
            
        <?php include "modal.php";?>
        </div><!-- right col --> 
      </div><!-- main container -->
    </div><!-- body container -->
  
        <script src="../../resource/js/foot-menu.js"></script>
        <script src="generate.js"></script>
        <script src="chat.js"></script>
    </body>
</html>
     