var path = null;
var ORDERID = "";
var LOAD = false;

$(document).on("click", "#confirm_order", function () {
	var types = $(this).data('types');
	paket_id = $(this).data('paket_id');
	select_id = "#"+$(this).data('select_id');
	var id = $(select_id).val();
	var url = path+"/system/_api/data_jadwal/select_jadwal.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','types':types,'konsultan_id':id},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Jadwal Null: ", err.message);
			}
			$('#jadwal').children('option').remove();
			$.each(data, function (i, item) {
				$('#jadwal').append($('<option>', { 
					value: item.jadwal_id,
					text : 'Hari '+item.hari_nama+', Start: '+item.timestart+' - End: '+item.timeend
				}));
			});	
		}
	});		
});

$(document).ready(function(){
	path = getpath();
	Get_ListOrder();
	var getData = setInterval(Get_ListOrder, 30000);
	//window.clearInterval(timerVariable)
});

function Get_ListOrder(){
	SELECTID = [];
	var url = path+"/system/_api/data_order/select_order.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				//console.log("Data List Order Null: ", err.message);
			}
			if(data.code == "200" && data.data != null){
				$('.box_order').remove();
				data.data.forEach(element => {
					//var order_id = "order_"+element.order_id;
					var question = element.paket_info.split('@');
					var jam = element.timestart.substring(0,2);
					var menit = element.timestart.substring(2,4);
					var start = jam+":"+menit;
					jam = element.timeend.substring(0,2);
					menit = element.timeend.substring(2,4);
					var end = jam+":"+menit;
					var jadwal = element.hari_nama+", Start: "+start+" - End: "+end;
					var year        = element.timestamp.substring(0,4);
					var month       = element.timestamp.substring(4,6);
					var day         = element.timestamp.substring(6,8);
					var hour         = element.timestamp.substring(8,10);
					var minute         = element.timestamp.substring(10,12);
					var date = new Date(year, month-1, 01);
					month = date.toLocaleString('default', { month: 'long' });
					var date_order = day+' '+month+' '+year+' '+hour+':'+minute;
					var id = "";
					var btn = "btn-warning";
					var status = "";
					var check = false;
					if(element.status == 0){
						status = "<span style='font-weight: bold;color:orange;'>Waiting Payment</span>";
					}
					if(element.status == 1){
						id = "chat_time";
						btn = "btn-primary";
						check = true;
						status = "<span style='font-weight: bold;color:green;'>Approved</span>";
					}
					if(element.status == 2){
						status = "<span style='font-weight: bold;color:blue;'>Pending</span>";
					}
					if(element.status == 3){
						status = "<span style='font-weight: bold;color:red;'>Cancel</span>";
					}
					
					var panel_order = "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 box_order'>"+
							"<div class='ibox'>"+
								"<div class='ibox-content product-box'>"+
								"<span class='label label-warning label_new_order'>"+element.kode+"</span>"+
									"<div class='product-imitation'>"+element.paket_name+"<br>"+element.konsultan+"</div>"+ 								
									"<div class='product-desc'>"+
										"<h3 align='center'>"+status+"</h3>"+
										"<span class='product-price'>Rp. "+numeral(element.price).format('0,0')+"</span>"+
										"<a href='#' class='product-name'>Durasi "+element.durasi+" Menit</a>"+ 
										"<a href='#' class='product-name'>"+question[1]+"</a>"+ 
										"<a href='#' class='product-name'>Jadwal: "+jadwal+"</a>"+ 										
										"<div class='m-t text-righ'>"+
											"<small class='text-muted'>Date Order: "+date_order+"</small>"+ 
										"</div>"+
										"<div class='m-t text-right'>"+
											"<button type='button' id='"+id+"' data-order_id='"+element.order_id+"' class='btn btn-md btn-outline "+btn+" btn_confirm' data-check='"+check+"'><i class='fas fa-comments' ></i> CHAT</button>"+
										"</div>"+
									"</div>"+
								"</div>"+  
							"</div>"+
						"</div>";
						$('#row_listorder').append(panel_order);		
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}


$('#form_order_paket').submit(function(){
	var jadwal = $('.modal-body #jadwal').val();
	var id = $(select_id).val();
	if(jadwal == null){
		swal({title: "Warning",text: "Jadwal Konsultasi Tidak Tersedia.",type: "warning"}, 
		function() {window.location = window.location.href;
		});
	}
	swal({
		title: "Request Order...",
		text: "Please wait for while...",
		imageUrl: "../../resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	var url = path+"/system/_api/data_order/reg_order.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','jadwal':jadwal,'konsultan_id':id,'paket_id':paket_id},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Order Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Check Your Email (Delay 5 Minute), Please Make Payment Soon.",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Request Paket Order Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).on("click", "#chat_time", function () {
	ORDERID = $(this).data('order_id');
	var check = $(this).data('check');
	if(check==1){
		var url = path+"/system/_api/data_order/check_order.php";
		$.ajax({
			type: "POST",
			url: url,
			data: {'token_key':123,'web':'true','order_id':ORDERID},
			success: function(msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				}
				catch(err) {
					console.log("Data Order Null: ", err.message);
				}
				if(data.code == "200"){				
					LISTCHAT = [];
					MENIT = data.durasi-1;
					if(MENIT < 0){
						end_order_limit();
						swal({title: "Chat Room Expired",text: "Time up! Cannot Access Anymore.",type: "warning"}, 
							function() {window.location = window.location.href;
						});
					}
					if(MENIT >= 0){
						$("#modal_chat").modal({
							backdrop: "static",
							keyboard: false
						});
						load_chat_awal();
						LOAD = true;
						timer = setInterval(function(){ 
							SetDurasi();
						}, 1000);
						get_chat = setInterval(function(){ 
							if(LOAD){
								load_chat();
							}
						}, 1000);
					}				
				}else{
					swal({title: "Chat Consultan Denied",text: "Chat Time Not Available - "+data.note,type: "warning"}, 
						function() {window.location = window.location.href;
						});
				}
			}
		});	
	}		
});

$(document).keypress(
	function(event){
	  if (event.which == '13') {
		event.preventDefault();
		send_chat();
	  }
  });


