var LISTCHAT = [];
var get_chat = null;
var timer = null;
var temp_length = null;
var MENIT = 0;
var DETIK = 60;

$("#modal_chat").on('shown.bs.modal', function(){
    var div = document.getElementById("chat_panel");
	$("#chat_panel").animate({ scrollTop: div.scrollHeight }, "slow");	
});

function load_chat_awal(){
	var url = path+"/system/_api/data_chat/load_chat.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'order_id':ORDERID},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Chat Null: ", err.message);
			}
			$('#chat_panel').children('.span_row').remove();
			if(data.length != 0){
				var detail = JSON.parse(data);
				LISTCHAT = detail;
				temp_length = LISTCHAT.length;	
				$.each(LISTCHAT, function (i, item) {
					$('#chat_panel').append(item);
					var nama = $("#nama_user_login").text();
					var nama_chat = $('#nama_'+nama).text();
					if(nama == nama_chat){
						nama = "row_"+nama;
						$('#chat_panel .'+nama).addClass("row_chat_user");
					}
				});				
			}				
		}
	});	
}

function load_chat(){
		var url = path+"/system/_api/data_chat/load_chat.php";
		$.ajax({
			type: "POST",
			url: url,
			data: {'token_key':123,'order_id':ORDERID},
			success: function(msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				}
				catch(err) {
					console.log("Data Chat Null: ", err.message);
				}
				$('#chat_panel').children('.span_row').remove();
				if(data.length == 0 && LISTCHAT.length != 0){
					window.clearInterval(get_chat)
					window.clearInterval(timer);
					closing_chat();
					swal({title: "Chat Room Closed",text: "Closing! Cannot Access Anymore.",type: "warning"}, 
							function() {window.location = window.location.href;
						});
				}
				if(data.length != 0){
					var detail = JSON.parse(data);
					LISTCHAT = detail;	
					$.each(LISTCHAT, function (i, item) {
						$('#chat_panel').append(item);
						var nama = $("#nama_user_login").text();
						var nama_chat = $('#nama_'+nama).text();
						if(nama == nama_chat){
							nama = "row_"+nama;
							$('#chat_panel .'+nama).addClass("row_chat_user");
						}
					});	
				}			
			}
		});
		if(temp_length != LISTCHAT.length){
			var objDiv = document.getElementById("chat_panel");
			$("#chat_panel").animate({ scrollTop: objDiv.scrollHeight }, "slow");			
		}
		temp_length = LISTCHAT.length;		
}

function send_chat(){
	var nama = $("#nama_user_login").text();
	var chat_text = $("#send_panel").val();
	var d = new Date();
	var h = addZero(d.getHours());
	var m = addZero(d.getMinutes());
	var waktu = h+":"+m;

	if(chat_text != ""){
		var temp = "";
		var check = chat_text.search("http");
		if(check == 0){
			temp = '<a href="'+chat_text+'" target="_blank"><b style="color:orange;">Click Me To Link!</b></a>';
			check = FindString(chat_text);
			if(check != -1){
				temp = '<a href="'+chat_text+'" target="_blank"><img src="'+chat_text+'" style="width:25%;"></img></a>';
			}
		}
		if(temp != ""){
			chat_text = temp;
		}
		
		var kelas = "span_row row_"+nama;
		var nama_chat = "nama_"+nama;

		var row = '<div class="'+kelas+'"><span class="line1" id="'+nama_chat+'">'+nama+'</span>'+
		'<span class="line1" id="waktu_chat">'+waktu+'</span><i id="icon_send" class="fas fa-check-double">'+
		'</i></br>'+'<span id="isi_chat">'+chat_text+'</span></div>';
		var aray_chat = [];
		aray_chat.push(row)
		LISTCHAT.push(aray_chat);

		var url = path+"/system/_api/data_chat/send_chat.php";
		$.ajax({
			type: "POST",
			url: url,
			data: {'token_key':123,'web':'true','order_id':ORDERID,'listchat':JSON.stringify(LISTCHAT)},
			success: function(msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				}
				catch(err) {
					console.log("Data Chat Null: ", err.message);
				}
				if(data.code != "200"){
					console.log("Send Chat Failed: "+data.note);
				}
			}
		});
	}else{
		swal({title: "Chat Denied",text: "Your Chat Text Empty.",type: "warning"});
	}
	$("#send_panel").val("");	
}

function end_chat(){
	var url = path+"/system/_api/data_order/end_order.php";
		$.ajax({
			type: "POST",
			url: url,
			data: {'token_key':123,'web':'true','order_id':ORDERID},
			success: function(msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				}
				catch(err) {
					console.log("Data Order Null: ", err.message);
				}
				if(data.code == "200"){
					swal({title: "Close Chat Room",text: "Closing Success.",type: "success"}, 
					function() {window.location = window.location.href;
					});
				}else{
					console.log("End Chat: "+data.note);
				}
			}
	});
}

function end_order_limit(){
	var url = path+"/system/_api/data_order/end_order_limit.php";
		$.ajax({
			type: "POST",
			url: url,
			data: {'token_key':123,'web':'true','order_id':ORDERID},
			success: function(msg) {
				var data = [];
				try {
					data = JSON.parse(msg);
				}
				catch(err) {
					console.log("Data Order Null: ", err.message);
				}
				if(data.code == "200"){
					console.log(data.note);
				}else{
					console.log("End Chat: "+data.note);
				}
			}
	});
}

$(document).on("click", "#send_btn", function () {
	send_chat();
});

$(document).on("click", "#exit_btn", function () {
	window.clearInterval(get_chat)
	window.clearInterval(timer);
	closing_chat();
});

$(document).on("click", "#end_btn", function () {
	swal({
		title: "Confirmation Closing Chat Room",
		text: "Are You Sure To Close This Chat Room?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Yes, Sure!',
		cancelButtonText: "No, Cancel!",
		closeOnConfirm: false,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {			
			window.clearInterval(get_chat)
			window.clearInterval(timer);
			$("#modal_chat").hide();	
			end_chat();	
			closing_chat();
		}
		else {
			swal("Canceled", "You Cancel Confirmation!", "error");
		}
	});
});


function closing_chat(){
	LOAD = false;
	ORDERID = "";
	temp_length = null;
}

function SetDurasi(){
	DETIK--;
	if(DETIK == 0){
		MENIT--;
		DETIK = 59;
	}
	var dtk = DETIK;
	if(DETIK < 10){
		dtk = "0"+dtk;
	}
	var mnt = MENIT;
	if(MENIT < 10){
		mnt = "0"+mnt;
	}
	$("#durasi").text("Time Limit: "+mnt+":"+dtk);
	if(MENIT < 0){	
		window.clearInterval(timer);
		window.clearInterval(get_chat)
		$("#modal_chat").hide();
		end_chat();
		closing_chat();
	}	
}