var path = null;

$(document).on("click", "#edit_konsultan", function () {
	var konsultan_id = $(this).data('konsultan_id');
	$(".modal-body #konsultan_id").val( konsultan_id );
					
	var nama = $(this).data('nama');
	$(".modal-body #edit_nama_konsultan").val( nama );
					
	var hp = $(this).data('hp');
	$(".modal-body #edit_hp_konsultan").val( hp );

	var email = $(this).data('email');
	$(".modal-body #edit_email_konsultan").val( email );

	var foto = $(this).data('photo');
	$(".modal-body #foto_edit").val( foto );

	var point = $(this).data('point');
	$(".modal-body #edit_point_konsultan").val( point );

	var info = $(this).data('info');
	$(".modal-body #edit_info").val( info );

	var active = $(this).data('active');
	$(".modal-body #active").val( active );
});

$('#form_tambah_konsultan').submit(function(){
	var username = $('.modal-body #add_username_konsultan').val();
	var nama = $('.modal-body #add_nama_konsultan').val();
	var hp = $('.modal-body #add_hp_konsultan').val();
	var email = $('.modal-body #add_email_konsultan').val();
	var foto = $('.modal-body #foto_add').val();
	var point = $('.modal-body #add_point_konsultan').val();
	var info = $('.modal-body #add_info').val();
	var url = path+"/system/_api/data_konsultan/reg_konsultan.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','username':username,'nama':nama,'hp':hp,'email':email,'foto':foto,'point':point,'info':info},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data konsultan Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register konsultan Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register konsultan Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_konsultan').submit(function(){
	var konsultan_id = $('.modal-body #konsultan_id').val();
	var nama = $('.modal-body #edit_nama_konsultan').val();
	var hp = $('.modal-body #edit_hp_konsultan').val();
	var email = $('.modal-body #edit_email_konsultan').val();
	var foto = $('.modal-body #foto_edit').val();
	var point = $('.modal-body #edit_point_konsultan').val();
	var info = $('.modal-body #edit_info').val();
	var active = $('.modal-body #active').val();
	var url = path+"/system/_api/data_konsultan/set_konsultan.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','konsultan_id':konsultan_id,'nama':nama,'hp':hp,'email':email,"foto":foto,'point':point,'info':info,'active':active},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data konsultan Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Save konsultan Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit konsultan Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	$("input[type='number']").inputSpinner();
});
	
function loadTable(){
	var url = path+"/system/_api/data_konsultan/get_konsultan.php";
	$('#tabel_konsultan').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "konsultan_id",
			"visible":false
		},{
			"data": "nama"
		},{
			"data": "hp"		
		},{
			"data": "email"			
		},{
			"data": "points",
			render: function ( data, type, row ) {
				return data+" Point";
			}
		},{
			"data": "info"			
		},{
			"data": "active",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:red;'>Inactive</span>";
				}else{
					return "<span style='font-weight: bold;color:green;'>Active</span>";
				}
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_konsultan' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-konsultan_id='"+data.konsultan_id+"' data-active='"+data.active+"' data-info='"+data.info+"' "+
					"data-nama='"+data.nama+"' data-hp='"+data.hp+"' data-photo='"+data.photo+"' data-email='"+data.email+"' data-point='"+data.points+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
					return edit;
			}
		}]
	});
}
