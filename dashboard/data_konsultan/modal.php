<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add New Konsultan</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_konsultan" method="POST" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="add_username_konsultan" class="form-control" placeholder="Input Username" required autofocus maxlength=16 minlength=4 onblur="this.value=removeSpaces(this.value);">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Full Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="add_nama_konsultan" class="form-control" placeholder="Input Full Name" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Mobile Phone:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="tel" id="add_hp_konsultan" class="form-control" placeholder="Input Mobile Phone (08xxxxxx~)" required autofocus  maxlength=20 minlength=8 pattern="[0-9]+" title="Input Number Only">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Konsultan Email:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="email" id="add_email_konsultan" class="form-control" placeholder="Input Email" required autofocus maxlength=50 minlength=8 title="Input Valid Email">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Link Photo:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="foto_add" class="form-control" rows="1" style="resize:none;"></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Konsultan Point:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="number" id="add_point_konsultan" class="form-control" placeholder="Input Point" required autofocus min="0" max="100000" value=0>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Info:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="add_info" class="form-control" rows="3" style="resize:none;"></textarea>
						</div>						
					</div>
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Add Konsultan</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Konsultan</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_konsultan" method="POST" class="form-horizontal form-label-left">
					<input id="konsultan_id" type="hidden"> 
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Full Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="edit_nama_konsultan" class="form-control" placeholder="Input Full Name" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Mobile Phone:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="tel" id="edit_hp_konsultan" class="form-control" placeholder="Input Mobile Phone (08xxxxxx~)" required autofocus  maxlength=20 minlength=8 pattern="[0-9]+" title="Input Number Only">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Konsultan Email:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="email" id="edit_email_konsultan" class="form-control" placeholder="Input Email" required autofocus maxlength=50 minlength=8 title="Input Valid Email">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Link Photo:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="foto_edit" class="form-control" rows="1" style="resize:none;"></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Konsultan Point:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="number" id="edit_point_konsultan" class="form-control" placeholder="Input Point" required autofocus min="0" max="100000">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Info:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="edit_info" class="form-control" rows="3" style="resize:none;"></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Status:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select id="active" class="form-control">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>						
					</div>
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Konsultan</button>
			</div>
                </form>
        </div>
    </div>
</div>
		<script src="modal.js"></script>