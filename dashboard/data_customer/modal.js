var path = null;

$(document).on("click", "#edit_customer", function () {
	var customer_id = $(this).data('customer_id');
	$(".modal-body #customer_id").val( customer_id );

	var card_id = $(this).data('card_id');
	$(".modal-body #card_edit").val( card_id );

	var nama = $(this).data('nama');
	$(".modal-body #nama_edit").val( nama );

	var hp = $(this).data('hp');
	$(".modal-body #hp_edit").val( hp );
					
	var email = $(this).data('email');
	$(".modal-body #email_edit").val( email );

	var jabatan = $(this).data('jabatan');
	$(".modal-body #jabatan_edit").val( jabatan );

	var alamat = $(this).data('alamat');
	$(".modal-body #alamat_edit").val( alamat );

	var types = $(this).data('types');
	$(".modal-body #type_edit").val( types );

	var saldo = $(this).data('saldo');
	$(".modal-body #saldo_edit").val( saldo );

	var wn = $(this).data('wn');
	$(".modal-body #wn_edit").val( wn );

	var point = $(this).data('point');
	$(".modal-body #point_edit").val( point );

	var info = $(this).data('info');
	$(".modal-body #info_edit").val( info );

	var blacklist = $(this).data('blacklist');
	$(".modal-body #blacklist").val( blacklist );

	var verifikasi = $(this).data('verifikasi');
	$(".modal-body #verifikasi").val( verifikasi );

	var active = $(this).data('active');
	$(".modal-body #active").val( active );
});

$('#form_tambah_customer').submit(function(){
	var user_nama = $('.modal-body #user_nama').val();
	var nama = $('.modal-body #nama_add').val();
	var hp = $('.modal-body #hp_add').val();
	var email = $('.modal-body #email_add').val();
	var card = $('.modal-body #card_add').val();
	var jabatan = $('.modal-body #jabatan_add').val();
	var alamat = $('.modal-body #alamat_add').val();
	var types = $('.modal-body #type_add').val();
	var wn = $('.modal-body #wn_add').val();
	var point = $('.modal-body #point_add').val();
	var info = $('.modal-body #info_add').val();
	var url = path+"/system/_api/data_customer/reg_cus.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','nama':nama,'hp':hp,'email':email,'card':card,
		'jabatan':jabatan,'alamat':alamat,'types':types,'username':user_nama,
		'wn':wn,'point':point,'info':info},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Customer Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register Customer Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register Customer Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_customer').submit(function(){
	var customer_id = $('.modal-body #customer_id').val();
	var nama = $('.modal-body #nama_edit').val();
	var hp = $('.modal-body #hp_edit').val();
	var email = $('.modal-body #email_edit').val();
	var card = $('.modal-body #card_edit').val();
	var jabatan = $('.modal-body #jabatan_edit').val();
	var alamat = $('.modal-body #alamat_edit').val();
	var types = $('.modal-body #type_edit').val();
	var wn = $('.modal-body #wn_edit').val();
	var point = $('.modal-body #point_edit').val();
	var info = $('.modal-body #info_edit').val();
	var blacklist = $('.modal-body #blacklist').val();
	var verifikasi = $('.modal-body #verifikasi').val();
	var active = $('.modal-body #active').val();
	var url = path+"/system/_api/data_customer/set_customer.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','nama':nama,'hp':hp,'email':email,'card':card,
		'jabatan':jabatan,'alamat':alamat,'types':types,
		'wn':wn,'point':point,'info':info,'verifikasi':verifikasi,
		'customer_id':customer_id,'blacklist':blacklist,'active':active},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Customer Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data Customer Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Customer Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	loadType();
	$("input[type='number']").inputSpinner();
});

function loadType(){
	var url = path+"/system/_api/type_customer/select_type.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Type Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#type_add').append($('<option>', { 
					value: item.type_id,
					text : item.type_name 
				}));
				$('#type_edit').append($('<option>', { 
					value: item.type_id,
					text : item.type_name 
				}));
			});	
		}
	});
}

function loadTable(){
	var url = path+"/system/_api/data_customer/get_customers.php";
	$('#tabel_customers').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[1,"desc"]],
		'columnDefs': [
			{
				"targets": 9, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"className":      'details-control',
			"orderable":      false,
			"data":           null,
			"defaultContent": ''
		},{
			"data": "customer_id",
			"visible":false
		},{
			"data": "wn",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "WNI";
				}else{
					return "WNA";
				}
			}
		},{
			"data": "nama"
		},{
			"data": "hp"		
		},{
			"data": "email"			
		},{
			"data": "type_name"
		},{
			"data": "saldo"
		},{
			"data": "active",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:red;'>Inactive</span>";
				}else{
					return "<span style='font-weight: bold;color:green;'>Active</span>";
				}
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_customer' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-info='"+data.customer_info+"' data-blacklist='"+data.blacklist+"' "+
					"data-wn='"+data.wn+"' data-point='"+data.points+"' data-verifikasi='"+data.verifikasi+"' "+
					"data-jabatan='"+data.jabatan+"' data-alamat='"+data.alamat+"' data-saldo='"+data.saldo+"' "+
					"data-customer_id='"+data.customer_id+"' data-card_id='"+data.card_id+"' data-active='"+data.active+"' "+
					"data-nama='"+data.nama+"' data-hp='"+data.hp+"' data-email='"+data.email+"' data-types='"+data.types+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
					return edit;
			}
		}]
	});
}

$('#tabel_customers tbody').on('click', 'td.details-control', function () {
	var table = $('#tabel_customers').DataTable()
	var tr = $(this).closest('tr');
	var row = table.row( tr );

	if ( row.child.isShown() ) {
		// This row is already open - close it
		row.child.hide();
		tr.removeClass('shown');
	}
	else {
		// Open this row
		row.child( formatTableCustomer(row.data()) ).show();
		tr.addClass('shown');
	}
} );