<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add New Customer</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_customer" method="POST" class="form-horizontal form-label-left">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="user_nama" class="form-control" placeholder="Input Username Login" required autofocus onblur="this.value=removeSpaces(this.value);" title="Input Username No Space">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Full Name:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="nama_add" class="form-control" placeholder="Input Full Name" required autofocus maxlength=30 minlength=4 >
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Mobile Phone:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="tel" id="hp_add" class="form-control" placeholder="Input Mobile Phone (08xxxxxx~)" required autofocus  maxlength=20 minlength=8 pattern="[0-9]+" title="Input Number Only">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="email" id="email_add" class="form-control" placeholder="Input Email" required autofocus maxlength=50 minlength=8 title="Input Valid Email">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Card Number:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="tel" id="card_add" class="form-control" placeholder="Input Card ID Number" autofocus  maxlength=25 minlength=8 pattern="[0-9]+" title="Input Number Only">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Alamat:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="alamat_add" class="form-control" rows="2" style="max-width:426px;max-height:150px;"></textarea>
							</div>						
						</div>		
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="type_add" class="form-control select_box"></select>
							</div>						
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Warga Negara:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="wn_add" class="form-control">
									<option value="0">WNI</option>
									<option value="1">WNA</option>
								</select>
							</div>						
						</div>	
						<div class="form-group" style="margin-bottom:5px;">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Point:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="point_add" class="form-control" placeholder="Input Point" autofocus min="0" max="100000" value="0">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Jabatan:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="jabatan_add" class="form-control" placeholder="Input Jabatan" autofocus maxlength=40 minlength=4 >
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Info:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="info_add" class="form-control" rows="2" style="max-width:426px;max-height:150px;"></textarea>
							</div>						
						</div>	
					</div>	
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Add Customer</button>
			</div>
				</form>
		</div>
	</div>
</div>


				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Customer</h4>
			</div>
			
            <div class="modal-body">
				<form id="form_edit_customer" method="POST" class="form-horizontal form-label-left">
					<input id="customer_id" type="hidden"> 
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Full Name:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="nama_edit" class="form-control" placeholder="Input Full Name" required autofocus maxlength=30 minlength=4 >
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Mobile Phone:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="tel" id="hp_edit" class="form-control" placeholder="Input Mobile Phone (08xxxxxx~)" required autofocus  maxlength=20 minlength=8 pattern="[0-9]+" title="Input Number Only">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Email:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="email" id="email_edit" class="form-control" placeholder="Input Email" required autofocus maxlength=50 minlength=8 title="Input Valid Email">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Card Number:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="tel" id="card_edit" class="form-control" placeholder="Input Card ID Number" autofocus  maxlength=25 minlength=8 pattern="[0-9]+" title="Input Number Only">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Jabatan:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<input type="text" id="jabatan_edit" class="form-control" placeholder="Input Jabatan" autofocus maxlength=40 minlength=4 >
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Alamat:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="alamat_edit" class="form-control" rows="3" style="max-width:426px;max-height:150px;"></textarea>
							</div>						
						</div>	
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="type_edit" class="form-control"></select>
							</div>						
						</div>	
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Warga Negara:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="wn_edit" class="form-control">
									<option value="0">WNI</option>
									<option value="1">WNA</option>
								</select>
							</div>						
						</div>	
						<div class="form-group" style="margin-bottom:5px;">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Point:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="point_edit" class="form-control" placeholder="Input Point" autofocus min="0" max="100000" value="0">
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Blacklist:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="blacklist" class="form-control">
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
							</div>		
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Status:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="active" class="form-control">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Verifikasi:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="verifikasi" class="form-control">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>						
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Info:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<textarea  id="info_edit" class="form-control" rows="2" style="max-width:426px;max-height:150px;"></textarea>
							</div>						
						</div>
							
					</div>
			</div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Customer</button>
			</div>
                </form>
		</div>
	</div>
</div>
		<script src="modal.js"></script>