	<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title">Confirm Order</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_order" method="POST" class="form-horizontal form-label-left">
					<input id="order_id" type="hidden"> 		
					<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Order Status:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
								<select id="status" class="form-control">
									<option value="0">Waiting Payment</option>
									<option value="1">Confirm</option>
									<option value="2">Pending</option>
									<option value="3">Cancel</option>
								</select>
							</div>						
					</div>					
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Confirm</button>
			</div>
                </form>
        </div>
    </div>
</div>
		<script src="modal.js"></script>