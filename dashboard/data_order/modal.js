var path = null;
var kode = null;

$(document).on("click", "#edit_order", function () {
	var order_id = $(this).data('order_id');
	$(".modal-body #order_id").val( order_id );

	kode = $(this).data('kode');

	var status = $(this).data('status');
	$(".modal-body #status").val( status );
});


$('#form_edit_order').submit(function(){
	var id = $('.modal-body #order_id').val();
	var status = $('.modal-body #status').val();
	var statustext = $("#status option:selected").html();

	var url = path+"/system/_api/data_order/set_order.php";

	swal({
		title: "Processing Order...",
		text: "Please wait for while...",
		imageUrl: "../../resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','order_id':id,'status':status,'kode':kode,'statustext':statustext},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Order Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Save Status Order Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Save Status Order Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	//$("input[type='number']").inputSpinner();
});
	
function loadTable(){
	var url = path+"/system/_api/data_order/get_order.php";
	$('#tabel_order').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 10, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "order_id",
			"visible":false
		},{
			"data": "timestamp",
			render: function ( data, type, row ) {
				var year        = data.substring(0,4);
				var month       = data.substring(4,6);
				var day         = data.substring(6,8);
				var hour         = data.substring(8,10);
				var minute         = data.substring(10,12);
				var date_order = year+'-'+month+'-'+day+' '+hour+':'+minute;	
				return date_order;
			}
		},{
			"data": "kode"
		},{
			"data": "paket_name"
		},{
			"data": "customer"			
		},{
			"data": "konsultan"			
		},{
			"data": "hari_nama",
			render: function ( data, type, row ) {
				var jam = row.timestart.substring(0,2);
				var menit = row.timestart.substring(2,4);
				var start = jam+":"+menit;
				jam = row.timeend.substring(0,2);
				menit = row.timeend.substring(2,4);
				var end = jam+":"+menit;
				return data+', Start: '+start+' - End: '+end;
			}				
		},{
			"data": "payment"			
		},{
			"data": "price",
			render: function ( data, type, row ) {
				return 'Rp. '+numeral(data).format('0,0');
			}				
		},
		{
			"data": "status",
			render: function ( data, type, row ) {
				if(data == 0){
					return "<span style='font-weight: bold;color:orange;'>Waiting Payment</span>";
				}
				if(data == 1){
					return "<span style='font-weight: bold;color:green;'>Confirm</span>";
				}
				if(data == 2){
					return "<span style='font-weight: bold;color:blue;'>Pending</span>";
				}
				if(data == 3){
					return "<span style='font-weight: bold;color:red;'>Cancel</span>";
				}
			}				
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var confirm = "<a id='edit_order' class='btn btn-info btn-xs last' data-toggle='modal' data-target='.edit' data-backdrop='static' "+
				"data-order_id='"+data.order_id+"' data-kode='"+data.kode+"' data-status='"+data.status+"' "+
				"data-keyboard='false' style='margin:auto;margin-bottom:4px;'><i class='fas fa-edit'></i> Confirm</a>";
				var resend = "<a id='resend_email' class='btn btn-warning btn-xs last' "+
				"data-order_id='"+data.order_id+"' "+
				"style='margin:auto;margin-bottom:2px;'><i class='fas fa-envelope-square'></i> Resend</a>";
				return confirm+resend;
			}
		}]
	});
}


$(document).on("click", "#resend_email", function (e) {
	var order_id = $(this).data('order_id');
	var url = path+"/system/_api/data_order/resend_email_payment.php";
	e.preventDefault();
        swal({
            title: "Confirmation Resend Email",
            text: "Are You Sure To Send Email Payment From This Order?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, Sure!',
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
				swal({
					title: "Resend Email...",
					text: "Please wait for while...",
					imageUrl: "../../resource/img/ajax-loader.gif",
					showConfirmButton: false,
					allowOutsideClick: false
				});
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {'token_key':123, 'order_id':order_id},
                    success: function(msg) {
                        var data = [];
						try {
							data = JSON.parse(msg);
						}
						catch(err) {
							console.log("Data Order Null: ", err.message);
						}
						if(data.note == "Success"){
							swal({title: "Success",text: "Resend Email Order Success",type: "success"}, 
									function() {window.location = window.location.href;
									});
						}else{
							swal({title: "Error",text: "Resend Email Order Failed - "+data.note,type: "error"}, 
									function() {window.location = window.location.href;
									});
							console.log("Error getting documents: ", data);
						}
                    }
                });
            }
            else {
                swal("Canceled", "You Cancel Confirmation!", "error");
            }
        });
});
