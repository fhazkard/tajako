var CB = [];
var ACCID = [];
var MENUID = [];
var ACC = [];
var path = null;

$('#form_save_access').submit(function(){
	swal({
		title: "Saving...",
		text: "Please wait for while...",
		imageUrl: "../../resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	var id = $('#select_role').val();
	for(var x = 0; x < CB.length; x++){
		var cb = $(CB[x]).prop('checked');
		if(cb){
			ACC.push(2);
		}else{
			ACC.push(0);
		}
	}	
	var url = path+"/system/_api/access_panel/set_access.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'role_id':id,'ACC':JSON.stringify(ACC),'ID':JSON.stringify(MENUID),'ACCID':JSON.stringify(ACCID)},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Access Null: ", err.message);
			}
			if(data.note== "Success"){
				swal({title: "Success",text: "Save Access Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Save Access Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadMenus();
	loadRoles();
});

function loadMenus(){
	var url = path+"/system/_api/menu_panel/select_menus.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Menu Null: ", err.message);
			}
			$.each(data, function (i, item) {
				MENUID.push(item.menu_id);
			});
		}
	});
}

function loadRoles(){
	var url = path+"/system/_api/role_panel/select_roles.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Role Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#select_role').append($('<option>', { 
					value: item.role_id,
					text : item.role_nama 
				}));
			});
			var id = $('#select_role').val();
			loadAccess(id);
		}
	});
}

$("#select_role").change(function() {
	var id = $('#select_role').val();
	loadAccess(id);
});

function loadAccess(id){
	CB = [];
	ACCID = [];
	var url = path+"/system/_api/access_panel/get_access.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','role_id':id},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Access Null: ", err.message);
			}
			$.each(data, function (i, item) {
				var id_cb = '#cb_'+item.menu_url				
				if(item.access == 2){
					$(id_cb).bootstrapToggle('on')				
				}else{
					$(id_cb).bootstrapToggle('off')
				}	
				CB.push(id_cb);		
				ACCID.push(item.access_id);
			});
			if(data.length == 0){
				$('.cb_access').bootstrapToggle('off')
			}
		}
	});
}
