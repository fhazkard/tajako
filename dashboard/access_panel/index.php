<?php 
  session_start(); 
  error_reporting(0);
  include "../header.php";
?>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
	 
      <div class="right_col" role="main">
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                  <div class="x_title">          
                    <h2>Data Access Panel</h2>
                    <input id="nama-dokumen" type="hidden" value="Data Access Panel">
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Select Role:</label>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  style="padding-left:0px;padding-right:0px;">
                      <select id="select_role" class="form-control"></select>
                    </div>	
                    <div class="clearfix"></div>

                    <div class="x_content" style="padding-left:0px;">
                      <form id="form_save_access" style="display:grid;">
                        <div class="form-group">
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Main:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_main" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>						
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Order:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_order" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Paket:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_paket" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Customer:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_customer" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Konsultan:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_konsultan" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Jadwal:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_jadwal" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                        </div>	

                        <div class="form-group" >
                         <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Hari:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_hari" class="cb_access" type="checkbox" data-toggle="toggle"> 
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Type Customer:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_type_customer" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Type Paket:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_type_paket" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Logs:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_logs" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Login Panel:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_login_panel" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>		
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Access Panel:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_access_panel" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                        </div>

                        <div class="form-group" >
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Role Panel:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_role_panel" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Menu Panel:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_menu_panel" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Live Order:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_live_order" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Billing:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_billing" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">History Chat:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_history_chat" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Article:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_article" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                        </div>

                        <div>
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Kontak:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_kontak" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>	
                          <label class="control-label col-lg-1 col-md-1 col-sm-1 col-xs-12" style="padding-left:0px;text-align:left;">Data Slide:</label>
                          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <input id="cb_data_slide" class="cb_access" type="checkbox" data-toggle="toggle">  
                          </div>
                        </div>

                        <div class="clearfix"></div>
                          <button type="submit" class="btn btn-success">Save Access</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          </div><!-- right row -->
<?php include "modal.php";?> 

        </div><!-- right col --> 
    </div><!-- main container -->
  </div><!-- body container -->
  
        <script src="../../resource/js/foot-menu.js"></script>
    </body>
</html>
	  