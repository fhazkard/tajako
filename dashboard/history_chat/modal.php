	<style>
		#chat_panel{
            width: -webkit-fill-available;
			height: 500px;
			min-height: 300px;
			max-height: 500px;
			background: lightblue;
			padding:5px;
			overflow-y: scroll;
        }
		#send_btn{
			margin-right:0px;
		}
		#message_panel{
			padding-left:0px;
		}
		#isi_chat{
			color:black;
		}
		.line1{
			font-weight: bold;
			color:navy;
			margin-right: 30px;
			padding:10px 0px 10px 0px;
		}
		.span_row{
			background:white;
			padding:10px;
			border-radius: 10px;
			margin-bottom: 10px;
		}
		#waktu_chat{
			margin-right: 10px;
		}
		#icon_send{
			color: limegreen;
		}
		.modal-title {
			padding-left:0px;padding-right:0px;
			font-size:20px;
		}
		.row_chat_user{
			text-align: right;
    		background: bisque;
		}
    </style> 

<div class="modal fade" id="modal_chat"  tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
				<label class="control-label modal-title col-lg-6 col-md-6 col-md-6 col-sm-6 col-xs-6" style="text-align:left;">Tajako Chat</label>
            </div>
            <div class="modal-body">
				<form id="form_order_paket" method="POST" class="form-horizontal form-label-left">
					<div class="form-group">
						<div id="chat_panel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">											
						</div>					
					</div>				
            </div>
			<div class="modal-footer">
				<button type="button" id="exit_btn" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<!-- <button type="button" id="end_btn" class="btn btn-danger" style="margin-right:15px;">End Chat</button>		 -->
			</div>
                </form>
        </div>
    </div>
</div>
