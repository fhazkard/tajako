var path = null;

$(document).on("click", "#show_chat", function () {
	var chat_id = $(this).data('chat_id');
	$(".modal-body #chat_id").val( chat_id );

	var listchat = $(this).data('listchat');
	$('#chat_panel').children('.span_row').remove();
	$.each(listchat, function (i, item) {
		$('#chat_panel').append(item);
		var nama = $("#nama_user_login").text();
		var nama_chat = $('#nama_'+nama).text();
		if(nama == nama_chat){
			nama = "row_"+nama;
			$('#chat_panel .'+nama).addClass("row_chat_user");
		}
	});	
});

$(document).ready(function(){
	path = getpath();
	loadTable();
});

function loadTable(){
	var url = path+"/system/_api/data_chat/get_chat.php";
	$('#tabel_history_chat').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 6, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "chat_id",
			"visible":false
		},{
			"data": "timestamp",
			render: function ( data, type, row ) {
				var year        = data.substring(0,4);
				var month       = data.substring(4,6);
				var day         = data.substring(6,8);
				var hour         = data.substring(8,10);
				var minute         = data.substring(10,12);
				var date_order = year+'-'+month+'-'+day+' '+hour+':'+minute;	
				return date_order;
			}
		},{
			"data": "kode"
		},{
			"data": "cus_nama"
		},{
			"data": "konsul_nama"
		},{
			"data": "status",
			render: function ( data, type, row ) {
				return "<span style='font-weight: bold;color:green;'>Complete</span>";			
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var list_action = "<a id='show_chat' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='#modal_chat' data-backdrop='static' "+
					"data-keyboard='false' style='margin-bottom:0px;float:left;'"+
					"data-chat_id='"+data.chat_id+"' data-listchat='"+data.listchat+"' > "+
					"<i class='fas fa-eye'></i> View</a> ";
				return list_action; 
			}
		}]
	});
}
