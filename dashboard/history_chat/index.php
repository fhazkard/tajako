<?php 
  session_start(); 
  error_reporting(0);
  include "../header.php";
?>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
	 
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
			
            <div class="x_panel">
                <div class="x_title">          
					<h2>History Chat</h2>
					<input id="nama-dokumen" type="hidden" value="History Chat">
					<div class="clearfix"></div>
                </div>
            <div class="x_content">
                <!-- <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus-square" ></i> Add New Hari</a> -->
				<div class="table-responsive">
				<table id="tabel_history_chat" class="table table-striped table-bordered table-hover dataTables-dashboard">
          <thead>
            <tr>
              <th>Chat ID</th>
              <th>Order Date</th>
              <th>Kode</th>
              <th>Customer</th>
              <th>Konsultan</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
					<tbody>					              		  
          </tbody>
        </table>
			</div>
        </div>
	</div>
</div>
</div><!-- right row -->
        <?php include "modal.php";?> 
        <script src="modal.js"></script>


        </div><!-- right col --> 
      </div><!-- main container -->
    </div><!-- body container -->
  
        <script src="../../resource/js/foot-menu.js"></script>
    </body>
</html>
	  