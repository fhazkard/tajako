<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add Account Login</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_account" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" name="user_nama" id="user_nama_add" class="form-control" placeholder="Masukan Username Account" required autofocus maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select name="user_role" id="user_role_add" class="form-control"></select>
						</div>						
					</div>
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Add Account</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Account Login</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_account" class="form-horizontal form-label-left">
					<input id="user_id" type="hidden" name="user_id"> 
					<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Username:</label>
							<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="user_nama_edit" name="user_nama" class="form-control" placeholder="Masukan Username Account" required autofocus maxlength="30" minlength="4" onblur="this.value=removeSpaces(this.value);">
							</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Role:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select name="user_role" id="user_role_edit" class="form-control"></select>
						</div>						
					</div>
			</div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save</button>
			</div>
                </form>
        </div>
    </div>
</div>
		
		<script src="modal.js"></script>
		<script src="../../resource/js/custom/confirm-suspend.js" type="text/javascript"></script>
		<script src="../../resource/js/custom/confirm-repas.js" type="text/javascript"></script>
		<script src="../../resource/js/custom/confirm-actived.js" type="text/javascript"></script>