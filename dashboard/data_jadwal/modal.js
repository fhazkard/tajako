var path = null;
var roles = "";

$(document).on("click", "#edit_jadwal", function () {
	var jadwal_id = $(this).data('jadwal_id');
	$(".modal-body #jadwal_id").val( jadwal_id );
					
	var konsultan_id = $(this).data('konsultan_id');
	$(".modal-body #edit_konsultan").val( konsultan_id );
					
	var paket_id = $(this).data('paket_id');
	$(".modal-body #edit_paket").val( paket_id );

	var hari_id = $(this).data('hari_id');
	$(".modal-body #edit_hari").val( hari_id );

	var timestart = $(this).data('timestart')+"";
	var jam = timestart.substring(0,2);
	var menit = timestart.substring(2,4);
	timestart = jam+":"+menit;
	$(".modal-body #edit_mulai").val( timestart );

	var timeend = $(this).data('timeend')+"";
	var jam = timeend.substring(0,2);
	var menit = timeend.substring(2,4);
	timeend = jam+":"+menit;
	$(".modal-body #edit_akhir").val( timeend );

	var active = $(this).data('active');
	$(".modal-body #active").val( active );
});

$('#form_tambah_jadwal').submit(function(){
	var konsultan = $('.modal-body #add_konsultan').val();
	var paket = $('.modal-body #add_paket').val();
	var hari = $('.modal-body #add_hari').val();
	var awal = $('.modal-body #add_mulai').val();
	var akhir = $('.modal-body #add_akhir').val();
	var url = path+"/system/_api/data_jadwal/reg_jadwal.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','konsultan':konsultan,'paket':paket,'hari':hari,'awal':awal,'akhir':akhir},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data jadwal Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register jadwal Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register jadwal Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_jadwal').submit(function(){
	var jadwal_id = $('.modal-body #jadwal_id').val();
	var konsultan = $('.modal-body #edit_konsultan').val();
	var paket = $('.modal-body #edit_paket').val();
	var hari = $('.modal-body #edit_hari').val();
	var awal = $('.modal-body #edit_mulai').val();
	var akhir = $('.modal-body #edit_akhir').val();
	var active = $('.modal-body #active').val();
	var url = path+"/system/_api/data_jadwal/set_jadwal.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','jadwal_id':jadwal_id,'konsultan':konsultan,'paket':paket,'hari':hari,'awal':awal,'akhir':akhir,'active':active},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data jadwal Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Save jadwal Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit jadwal Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	roles = $('#role_nama').val();
	path = getpath();
	
	if(roles != "Konsultan"){
		var url = path+"/system/_api/data_jadwal/get_jadwal.php";
		loadTable(true,url);
		loadKonsultan();
		loadPaket();
		loadHari();
	}else{
		var url = path+"/system/_api/data_jadwal/get_data.php";
		loadTable(false,url);
	}
});

function loadKonsultan(){
	var url = path+"/system/_api/data_konsultan/select_konsultan.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Konsultan Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#add_konsultan').append($('<option>', { 
					value: item.konsultan_id,
					text : item.nama 
				}));
				$('#edit_konsultan').append($('<option>', { 
					value: item.konsultan_id,
					text : item.nama 
				}));
			});	
		}
	});
}
	
function loadPaket(){
	var url = path+"/system/_api/data_paket/select_paket.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Paket Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#add_paket').append($('<option>', { 
					value: item.paket_id,
					text : item.paket_name 
				}));
				$('#edit_paket').append($('<option>', { 
					value: item.paket_id,
					text : item.paket_name 
				}));
			});	
		}
	});
}

function loadHari(){
	var url = path+"/system/_api/data_hari/select_hari.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Hari Null: ", err.message);
			}
			$.each(data, function (i, item) {
				$('#add_hari').append($('<option>', { 
					value: item.hari_id,
					text : item.hari_nama 
				}));
				$('#edit_hari').append($('<option>', { 
					value: item.hari_id,
					text : item.hari_nama
				}));
			});	
		}
	});
}

function loadTable(kolom,url){
	$('#tabel_jadwal').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 8, 
				"className": "text-center",
				"width": "5%",
				"visible": kolom
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "jadwal_id",
			"visible":false
		},{
			"data": "nama"
		},{
			"data": "paket_name"		
		},{
			"data": "hari_nama"			
		},{
			"data": "timestart",
			render: function ( data, type, row ) {
				var jam = data.substring(0,2);
				var menit = data.substring(2,4);
				return jam+":"+menit;
			}	
		},{
			"data": "timeend",
			render: function ( data, type, row ) {
				var jam = data.substring(0,2);
				var menit = data.substring(2,4);
				return jam+":"+menit;
			}				
		},{
			"data": "used",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:green;'>No</span>";
				}else{
					return "<span style='font-weight: bold;color:red;'>Yes</span>";
				}
			}			
		},{
			"data": "active",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:red;'>Inactive</span>";
				}else{
					return "<span style='font-weight: bold;color:green;'>Active</span>";
				}
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_jadwal' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-jadwal_id='"+data.jadwal_id+"' data-active='"+data.active+"' data-konsultan_id='"+data.konsultan_id+"' data-used='"+data.used+"' "+
					"data-paket_id='"+data.paket_id+"' data-hari_id='"+data.hari_id+"' data-timestart='"+data.timestart+"' data-timeend='"+data.timeend+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
				if(roles == "Konsultan"){
					edit = "";
				}
				return edit;
			}
		}]
	});
}
