<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add New Menu</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_menu" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Display Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="display_add" class="form-control" placeholder="Input Display Name" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Url Menu:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="url_add" class="form-control" placeholder="Input Url Menu" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Icon Menu:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="icon_add" class="form-control" placeholder="Input Icon Menu" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>		
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type Menu</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select id="type_menu_add" class="form-control">
								<option value="1">Parent</option>
								<option value="2">Child</option>
							</select>
						</div>						
					</div>		
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Parent Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="parent_add" class="form-control" placeholder="Input Parent Name" autofocus maxlength="45" minlength="4">
						</div>						
					</div>	
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Add Menu</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Detail Menu</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_menu" class="form-horizontal form-label-left">
					<input id="menu_id" type="hidden">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Display Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="display_edit" class="form-control" placeholder="Input Display Name" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Url Menu:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="menu_url_edit" class="form-control" placeholder="Input Url Menu" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Icon Menu:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="menu_icon_edit" class="form-control" placeholder="Input Icon Menu" required autofocus maxlength="45" minlength="4">
						</div>						
					</div>		
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type Menu</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select id="menu_type_edit" class="form-control">
								<option value="1">Parent</option>
								<option value="2">Child</option>
							</select>
						</div>						
					</div>		
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Parent Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="parent_edit" class="form-control" placeholder="Input Parent Name" autofocus maxlength="45" minlength="4">
						</div>						
					</div>			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
                </form>
        </div>
    </div>
</div>
		
<script src="modal.js"></script>