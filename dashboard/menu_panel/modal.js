var path = null;

$(document).on("click", "#edit_detail_menu", function () {
	var menu_id = $(this).data('menu_id');
	$(".modal-body #menu_id").val( menu_id );
					
	var display = $(this).data('display');
	$(".modal-body #display_edit").val( display );
					
	var menu_url = $(this).data('menu_url');
	$(".modal-body #menu_url_edit").val( menu_url );

	var menu_icon = $(this).data('menu_icon');
	$(".modal-body #menu_icon_edit").val( menu_icon );

	var menu_type = $(this).data('menu_type');
	$(".modal-body #menu_type_edit").val( menu_type );

	var parent = $(this).data('parent');
	$(".modal-body #parent_edit").val( parent );
});

$('#form_tambah_menu').submit(function(){
	var display = $('.modal-body #display_add').val();
	var url_add = $('.modal-body #url_add').val();
	var icon = $('.modal-body #icon_add').val();
	var type = $('.modal-body #type_menu_add').val();
	var parent = $('.modal-body #parent_add').val();
	var url = path+"/system/_api/menu_panel/reg_menu.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'display':display,'url':url_add,'icon':icon,'type':type,'parent':parent},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Menu Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Add New Menu Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Add New Menu Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_menu').submit(function(){
	var id = $('.modal-body #menu_id').val();
	var display = $('.modal-body #display_edit').val();
	var url_add = $('.modal-body #menu_url_edit').val();
	var icon = $('.modal-body #menu_icon_edit').val();
	var type = $('.modal-body #menu_type_edit').val();
	var parent = $('.modal-body #parent_edit').val();
	var url = path+"/system/_api/menu_panel/set_menu.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'menu_id':id,'display':display,'url':url_add,'icon':icon,'type':type,'parent':parent},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Menu Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data Menu Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Menu Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
});

function loadTable(){
	var url = path+"/system/_api/menu_panel/get_menus.php";
	$('#tabel_menu_panel').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		//"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets":4, // your case first column
				"className": "text-center",
				"width": "7%"
		   },{
			"targets":6, // your case first column
			"className": "text-center",
			"width": "5%"
	   }],
		//"data" : data,
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "menu_id",
			"visible":false
		},{
			"data": "display"
		},{
			"data": "menu_url"
		},{
			"data": "menu_icon"
		},{
			"data": "menu_type",
			render: function ( data, type, row ) {
				if(data=="1"){
					return "Parent";
				}else{
					return "Child";
				}
			}
		},{
			"data": "parent_display"
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var list_action = "<a id='edit_detail_menu' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='margin-bottom:0px;float:left;'"+
					"data-parent='"+data.parent_display+"' data-menu_type='"+data.menu_type+"' data-menu_icon='"+data.menu_icon+"'"+
					"data-menu_id='"+data.menu_id+"' data-display='"+data.display+"' data-menu_url='"+data.menu_url+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
				return list_action; 
			}
		}]
	});
}