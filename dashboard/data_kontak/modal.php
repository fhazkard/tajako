		<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Kontak</h4>
			</div>
			
            <div class="modal-body">
				<form id="form_edit_kontak" method="POST" class="form-horizontal form-label-left">
					<input id="kontak_id" type="hidden"> 
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Handphone:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="hp_edit" class="form-control" rows="3" style="resize: none;" placeholder="Please Input List Handphone Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Alamat:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="alamat_edit" class="form-control" rows="5" style="resize: none;" placeholder="Please Input Alamat Here..." required></textarea>
						</div>						
					</div>
			</div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Kontak</button>
			</div>
                </form>
		</div>
	</div>
</div>
		<script src="modal.js"></script>