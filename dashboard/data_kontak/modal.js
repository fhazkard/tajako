var path = null;

$(document).on("click", "#edit_kontak", function () {
	var kontak_id = $(this).data('kontak_id');
	$(".edit #kontak_id").val( kontak_id );

	var hp = $(this).data('hp');
	$(".edit #hp_edit").val( hp );

	var alamat = $(this).data('alamat');
	$(".edit #alamat_edit").val( alamat );

});

$('#form_edit_kontak').submit(function(){
	var kontak_id = $('.edit #kontak_id').val();
	var hp = $('.edit #hp_edit').val();
	var alamat = $('.edit #alamat_edit').val();

	var url = path+"/system/_api/data_kontak/set_kontak.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','kontak_id':kontak_id,'hp':hp,'alamat':alamat},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Kontak Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data Kontak Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Kontak Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
});

function loadTable(){
	var url = path+"/system/_api/data_kontak/load_kontak.php";
	$('#tabel_kontak').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 3, // your case first column
				"className": "text-center",
				"width": "5%"
		}],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "kontak_id",
			"visible":false
		},{
			"data": "hp",
			render: function ( data, type, row ) {
				var hp = data.split("@");
				var shows = "";
				hp.forEach(el => {
					shows = shows+el+", ";
				})
				return shows;
			}
		},{
			"data": "alamat"		
		},{ "data": null,
		render: function ( data, type, row ) {
			var edit = "<a id='edit_kontak' href='#' class='btn btn-info btn-xs last' "+
				"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
				"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
				"data-kontak_id='"+data.kontak_id+"' data-hp='"+data.hp+"' data-alamat='"+data.alamat+"' > "+
				"<i class='fas fa-edit'></i> Edit</a> ";
				return edit;
		}
		}]
	});
}
