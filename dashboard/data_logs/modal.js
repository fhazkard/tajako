var path = null;

$(document).ready(function(){
	path = getpath();
	loadTable();
});
	
function loadTable(){
	var url = path+"/system/_api/data_log/get_logs.php";
	$('#tabel_logs').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "id_log",
			"visible":false
		},{
			"data": "desk_log"
		}, {
			"data": "timestamp",
			render: function ( data, type, row ) {
				var year        = data.substring(0,4);
				var month       = data.substring(4,6);
				var day         = data.substring(6,8);
				var hour         = data.substring(8,10);
				var minute         = data.substring(10,12);
				return year+'-'+month+'-'+day+' '+hour+':'+minute;
			}
		}
		]
	});
}