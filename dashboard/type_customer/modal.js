var path = null;

$(document).on("click", "#edit_type_customer", function () {
	var type_id = $(this).data('type_id');
	$(".modal-body #type_id").val( type_id );
					
	var type_name = $(this).data('type_name');
	$(".modal-body #nama_type_edit").val( type_name );

	var type_diskon = $(this).data('type_diskon');
	$(".modal-body #diskon_type_edit").val( type_diskon );

});

$('#form_tambah_typecustomer').submit(function(){
	var nama = $('.modal-body #nama_type_add').val();
	var diskon = $('.modal-body #diskon_type_add').val();
	var url = path+"/system/_api/type_customer/reg_type.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'nama':nama,'diskon':diskon},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Type Paket Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Add New Type Paket Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Add New Type Paket Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_typecustomer').submit(function(){
	var id = $('.modal-body #type_id').val();
	var nama = $('.modal-body #nama_type_edit').val();
	var diskon = $('.modal-body #diskon_type_edit').val();
	var url = path+"/system/_api/type_customer/set_type.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'type_id':id,'nama':nama,'diskon':diskon},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Type Paket Null: ", err.message);
			}
			if(data.note== "Success"){
				swal({title: "Success",text: "Edit Data Type Paket Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Type Paket Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	$("input[type='number']").inputSpinner();
});

function loadTable(){
	var url = path+"/system/_api/type_customer/get_type.php";
	$('#tabel_type_customer').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 3, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "type_id",
			"visible":false
		},{
			"data": "type_name"
		},{
			"data": "type_diskon",
			render: function ( data, type, row ) {
				return data+"%";
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var list_action = "<a id='edit_type_customer' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='margin-bottom:0px;float:left;'"+
					"data-type_id='"+data.type_id+"' data-type_name='"+data.type_name+"' data-type_diskon='"+data.type_diskon+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
				return list_action; 
			}
		}]
	});
}