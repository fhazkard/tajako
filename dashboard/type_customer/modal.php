<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add Type Customer</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_typecustomer" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type Customer Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="nama_type_add" class="form-control" placeholder="Input Type Customer Name" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Diskon:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="diskon_type_add" class="form-control" placeholder="Input Persen Diskon" required autofocus data-decimals="1" min="0" max="100" value="0" step="0.1">
						</div>						
					</div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Add Type Customer</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Type Customer</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_typecustomer" class="form-horizontal form-label-left">
					<input id="type_id" type="hidden">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Type Customer Name:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="nama_type_edit" class="form-control" placeholder="Input Type Customer Name" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Diskon:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
						<input type="number" id="diskon_type_edit" class="form-control" placeholder="Input Persen Diskon" required autofocus data-decimals="1" min="0" max="100" value="0" step="0.1">
						</div>						
					</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
                </form>
        </div>
    </div>
</div>
		
<script src="modal.js"></script>