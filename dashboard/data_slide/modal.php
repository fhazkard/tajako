<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add New Slide</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_slide" method="POST" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Urutan:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="sort_add" class="form-control" placeholder="Input No Urutan" required autofocus min="0" max="3" value=0 step="1">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Slogan:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="slogan_add" class="form-control" rows="1" style="resize: none;" placeholder="Please Input Slogan Text Here..." required maxlength="60"></textarea>
							<small style="color:red;">*(Max 60 Word)</small>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Url Tombol:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="url_add" class="form-control" rows="2" style="resize: none;" placeholder="Please Input Url Tombol Text Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Text Tombol:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="buton_add" class="form-control" placeholder="Input Text Tombol" autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Link Image:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="link_add" class="form-control" rows="3" style="resize: none;" placeholder="Please Input Link Image Here..." required></textarea>
							<small style="color:red;">*(1920px x 500px)</small>
						</div>						
					</div>
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Add Slide</button>
			</div>
				</form>
		</div>
	</div>
</div>


				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Data Slide</h4>
			</div>
			
            <div class="modal-body">
				<form id="form_edit_slide" method="POST" class="form-horizontal form-label-left">
					<input id="slide_id" type="hidden"> 
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Urutan:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="number" id="sort_edit" class="form-control" placeholder="Input No Urutan" required autofocus min="0" max="3" value=0 step="1">
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Slogan:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="slogan_edit" class="form-control" rows="1" style="resize: none;" placeholder="Please Input Slogan Text Here..." required maxlength="60"></textarea>
							<small style="color:red;">*(Max 60 Word)</small>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Url Tombol:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="url_edit" class="form-control" rows="2" style="resize: none;" placeholder="Please Input Url Tombol Text Here..." required></textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Text Tombol:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="buton_edit" class="form-control" placeholder="Input Text Tombol" autofocus >
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Link Image:</label>
						<div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<textarea  id="link_edit" class="form-control" rows="3" style="resize: none;" placeholder="Please Input Link Image Here..." required></textarea>
							<small style="color:red;">*(1920px x 500px)</small>
						</div>						
					</div>
			</div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Save Article</button>
			</div>
                </form>
		</div>
	</div>
</div>
		<script src="modal.js"></script>