var path = null;

$(document).on("click", "#edit_slide", function () {
	var slide_id = $(this).data('slide_id');
	$(".edit #slide_id").val( slide_id );

	var sort = $(this).data('sort');
	$(".edit #sort_edit").val( sort );

	var slogan = $(this).data('slogan');
	$(".edit #slogan_edit").val( slogan );

	var url = $(this).data('url');
	$(".edit #url_edit").val( url );

	var buton = $(this).data('buton');
	$(".edit #buton_edit").val( buton );
					
	var link = $(this).data('link');
	$(".edit #link_edit").val( link );

});

$('#form_tambah_slide').submit(function(){
	var sort = $('.tambah #sort_add').val();
	var slogan = $('.tambah #slogan_add').val();
	var url_buton = $('.tambah #url_add').val();
	var buton = $('.tambah #buton_add').val();
	var link = $('.tambah #link_add').val();

	var url = path+"/system/_api/data_slide/reg_slide.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','sort':sort,'slogan':slogan,'url':url_buton,'buton':buton,'link':link},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Slide Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Register Slide Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Register Slide Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_slide').submit(function(){
	var slide_id = $('.edit #slide_id').val();
	var sort = $('.edit #sort_edit').val();
	var slogan = $('.edit #slogan_edit').val();
	var url_buton = $('.edit #url_edit').val();
	var buton = $('.edit #buton_edit').val();
	var link = $('.edit #link_edit').val();

	var url = path+"/system/_api/data_slide/set_slide.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','slide_id':slide_id,'sort':sort,'slogan':slogan,'url':url_buton,'buton':buton,'link':link},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Slide Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Edit Data Slide Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Slide Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
	$("input[type='number']").inputSpinner();
});

function loadTable(){
	var url = path+"/system/_api/data_slide/load_slide.php";
	$('#tabel_slide').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 6, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "slide_id",
			"visible":false
		},{
			"data": "sort"	
		},{
			"data": "slogan"
		},{
			"data": "url"
		},{
			"data": "buton"
		},{
			"data": "link"		
		},{ 
			"data": null,
			render: function ( data, type, row ) {
				var edit = "<a id='edit_slide' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='width:95%;height:25px;margin:auto;margin-bottom:2px;' "+
					"data-url='"+data.url+"' data-buton='"+data.buton+"' data-link='"+data.link+"' "+
					"data-slide_id='"+data.slide_id+"' data-sort='"+data.sort+"' data-slogan='"+data.slogan+"' > "+
					"<i class='fas fa-edit'></i> Edit</a> ";
					return edit;
			}
		}]
	});
}
