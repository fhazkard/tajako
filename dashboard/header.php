<?php  session_start();
require '../../system/_core/csrf.php';
if(isset($_SESSION['limit'])&& isset($_SESSION['user_id']))
{
	$limit = $_SESSION['limit'];
	$user_id = $_SESSION['user_id'];
	$nama = $_SESSION['nama'];
	$role = $_SESSION['role'];
	$menus = $_SESSION['menu'];
	$parent = $_SESSION['parent'];

	if (time() > $limit){		
		unset($_SESSION['limit']);
		unset($_SESSION['user_id']);
		unset($_SESSION['nama']);
		unset($_SESSION['role']);
		unset($_SESSION['menu']);
		unset($_SESSION['parent']);
		session_destroy();
		die("<script>window.location = '../../system/_core/timeout.php'</script>");	
	}
}else{
	die("<script>window.location = '../../system/_core/timeout.php'</script>");
}
?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<!-- Meta, title, favicons -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tajako - Dashboard Panel</title>
		<link rel="icon" href="../../resource/img/icontitle.ico">
		<script src="../../resource/js/head-menu.js"></script> 
		<style>
			.widget_order{
				clear:none;
			}
			.headerbar{
				margin-top:10px;
			}
			.label_new_order{
				font-size:20px;
				float:left;
			}
			.ibox-title .label {
				float: left;
				margin-left: 0px;
			}
			.footer-custom{
				display: inline-flex;
				background:  white;
				border-top: 1px solid #e7eaec;
				bottom: 0;
				padding: 10px 20px;
				position: absolute;
			}
		</style> 
	</head>

	<body class="nav-md">
		<div class="container body">
    		<div class="main_container">

				<div class="col-md-3 left_col">
					<div class="left_col scroll-view">
						<div class="navbar nav_title" style="border: 0;">
							<a href="../../dashboard/main/" class="site_title"><i class="fab fa-slack"></i> <span>Tajako</span></a>
						</div>
						<div class="clearfix"></div>
						<!-- menu prile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="../../resource/img/user.jpg" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<h3 style="margin-bottom:5px;">Welcome,</h3>
								<h3 id="nama_user_login"><?php echo $nama?></h3>
								<h3 style="margin-top:5px;"><?=date('d - M - Y')?></h3>
								<input type="hidden" id="role_nama" value="<?php echo	$role;?>">
							</div>
						</div>
						<!-- /menu prile quick info -->
						<br />
          
						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
							<div class="menu_section">
								<br> 
								<div class="clearfix"></div>
								<ul class="nav side-menu">
								<?php
								$html = "";
								$start_parent = "";
								$end_parent = "</ul></li>";
								$array_child = [];
								//print_r($menus);
								foreach ($menus as $menu) {
									if($menu->menu_type == 1 && $menu->access  == 2){
										$html = "<li><a href='../$menu->menu_url/'><i class='$menu->menu_icon'></i> $menu->display </a></li>";
										echo $html;
									}
									if($menu->menu_type == 2 && $menu->access  == 2){
										array_push($array_child,$menu);
									}
								}

								for($x = 0; $x < count($parent); $x++){
									if(count($array_child) > 0){
										$html = "";
										$display =  $parent[$x]->parent_display;
										$icon =  $parent[$x]->menu_icon;
										$start_parent = "<li><a><i class='$icon'></i> $display <span class='chevron'>".
												"<i class='fas fa-chevron-circle-down'></i></span></a>".
												"<ul class='nav child_menu' style='display: none'>";

										for($z = 0; $z < count($array_child); $z++){
											$pr_dis = $array_child[$z]->parent_display;
											if($display == $pr_dis){
												$url = $array_child[$z]->menu_url;
												$dis = $array_child[$z]->display;
												$html = $html."<li><a href='../$url/'> $dis </a></li>";
											}
										}
										if($html != ""){
											echo $start_parent.$html.$end_parent;
										}			
									}						
								}

								?>
								</ul><!-- side-menu -->
							</div><!-- menu_section -->
						</div><!-- sidebar menu -->
					</div><!--scroll-view -->
				</div><!-- col-md-3 left_col -->

				<!-- top navigation -->
				<div class="top_nav">
					<div class="nav_menu">
						<nav role="navigation">
							<div class="nav toggle">
								<a id="menu_toggle"><i class="animated bounceIn fa fa-bars"></i></a>
							</div>
            				<ul class="nav navbar-nav navbar-right">
								<li><!-- li -->
									<a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="margin-bottom:0px;">
										Setting <span class="fa fa-cog fa-lg" style="display:inline-block !important; margin-bottom:0px;"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
										<li>
											<a  data-toggle="modal" data-target=".ganti_pass" data-backdrop="static" data-keyboard="false" style='padding-left:15px;'>  Change Password</a>
										</li>
										<li>
											<form action="../../system/_core/logout.php" method="post" style="width: auto;">                      
												<input id="logout" type="submit" name="logout" value="Log Out">
												<?php print CSRF::tokenInput(); ?> 
											</form>
										</li>
									</ul>
								</li><!-- li -->								
							</ul>
						</nav>
					</div><!-- nav menu -->
				</div><!-- top navigation -->   

				<div class="animated bounceInUp modal fade ganti_pass" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span>
								</button>
								<h4 class="modal-title">Change Password</h4>
							</div>
							<form method="POST" action="../../system/_core/change-pass.php" class="form-horizontal form-label-left">
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">New Password:</label>
										<div class="col-md-8 col-sm-8 col-xs-12"  style="padding-left:0px;padding-right:0px;">
										<input type="password" name="password" class="form-control" placeholder="Input New Password" required maxlength="10" minlength="6" autofocus>
									</div>						
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 col-sm-4 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Confirmation Password:</label>
										<div class="col-md-8 col-sm-8 col-xs-12"  style="padding-left:0px;padding-right:0px;">
										<input type="password" name="konfirm" class="form-control" placeholder="Input Same Password" required maxlength="10" minlength="6">
										</div>						
									</div>
									<?php print CSRF::tokenInput(); ?>
								</div>
								<div class="modal-footer">
									<input id="module" type="hidden" name="module" value="tbl_users"> 
									<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</form>
						</div>
					</div>
				</div><!-- top navigation -->   
<script src="../load.js"></script>