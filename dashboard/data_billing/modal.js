var path = null;

$(document).ready(function(){
	path = getpath();
	loadTable('now');
});
	
function loadTable(time){
	var nama_dokumen = $('#nama_dokumen').val()+' '+getDate('-');
	var url = path+"/system/_api/data_order/get_billing.php";
	$('#tabel_data_billing').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		"bDeferRender": true,
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':time,'web':'true'},
		},
		"columns": [{
			"data": "timestamp",
			render: function ( data, type, row ) {
				var year        = data.substring(0,4);
				var month       = data.substring(4,6);
				var day         = data.substring(6,8);
				var hour         = data.substring(8,10);
				var minute         = data.substring(10,12);
				return year+'-'+month+'-'+day+' '+hour+':'+minute;
			}
		}, {
			"data": "kode"
		}, {
			"data": "cus_nama"
		}, {
			"data": "konsul_nama"
		}, {
			"data": "paket_name"
		},{
			"data": "price",
			render: function ( data, type, row ) {
				return 'Rp. '+numeral(data).format('0,0');
			}
		}],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;

			var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

			total = api.column( 5 ).data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
				}, 0 );
				
			$('#tfoot_total_databill').text(
				'Rp. '+numeral(total).format('0,0'));
		},
		dom: 'Bfrtip',
		buttons: [
			{extend: 'copy'},
			$.extend( true, {}, formatStandar, {
				extend: 'excelHtml5',title: nama_dokumen, footer: true,orientation: 'landscape'
            } ),
            $.extend( true, {}, formatPDF, {
                extend: 'pdfHtml5',title: nama_dokumen, footer: true,orientation: 'landscape'
            } )
		]
	});
}