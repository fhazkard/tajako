<?php 
  session_start(); 
  error_reporting(0);
  include "../header.php";
?>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
	
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
			
            <div class="x_panel">
              <div class="x_title">          
                <h2>Data Billing</h2>
                <input id="nama_dokumen" type="hidden" value="Data Billing">
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
              <a href="#" class="btn btn-success btn-sm" onclick="loadTable('-7 days');">7 Days <i class="fa fa-table"></i> </a>
              <a href="#" class="btn btn-success btn-sm" onclick="loadTable('-30 days');">30 Days <i class="fa fa-table"></i> </a>
              <a href="#" class="btn btn-success btn-sm" onclick="loadTable('all');">All Time <i class="fa fa-table"></i> </a>
			          <div class="table-responsive">	
                  <table id="tabel_data_billing" class="table table-striped table-bordered table-hover dataTables-dashboard">
                    <thead>
                      <tr>
                        <th>Date Order</th>
                        <th>Kode</th>
                        <th>Customer</th>
                        <th>Konsultan</th>
                        <th>Paket</th>
                        <th>Price</th>
                      </tr>
                    </thead>
					          <tbody>					              		  
                    </tbody>
                    <tfoot>
                      <tr>
                        <th colspan="5" style="text-align:right">Grand Total:</th>
                        <th id="tfoot_total_databill"></th>
                      </tr>
                    </tfoot>
                  </table>
			</div>
        </div>
	</div>

	 

</div>
    </div><!-- right row -->
    <?php include "modal.php";?>
	
	  <?php include "../footer.php";?>

        </div><!-- right col --> 
      </div><!-- main container -->
    </div><!-- body container -->
  
        <script src="../../resource/js/foot-menu.js"></script>
    </body>
</html>
	  
	  
	  	
	  