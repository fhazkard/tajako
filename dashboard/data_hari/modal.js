var path = null;

$(document).on("click", "#edit_hari", function () {
	var hari_id = $(this).data('hari_id');
	$(".modal-body #hari_id").val( hari_id );
					
	var hari_nama = $(this).data('hari_nama');
	$(".modal-body #hari_edit").val( hari_nama );

	var active = $(this).data('active');
	$(".modal-body #active").val( active );

});

$('#form_tambah_hari').submit(function(){
	var nama = $('.modal-body #hari_add').val();
	var url = path+"/system/_api/data_hari/reg_hari.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'nama':nama},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Hari Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Add New Hari Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Add New Hari Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$('#form_edit_hari').submit(function(){
	var id = $('.modal-body #hari_id').val();
	var nama = $('.modal-body #hari_edit').val();
	var active = $('.modal-body #active').val();
	var url = path+"/system/_api/data_hari/set_hari.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'hari_id':id,'nama':nama,'active':active},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Hari Null: ", err.message);
			}
			if(data.note== "Success"){
				swal({title: "Success",text: "Edit Data Hari Success",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Edit Data Hari Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
	return false;
});

$(document).ready(function(){
	path = getpath();
	loadTable();
});

function loadTable(){
	var url = path+"/system/_api/data_hari/get_hari.php";
	$('#tabel_hari').DataTable({
		destroy: true,
		"paging": true,
		"searching": true,
		"ordering": true,
		"autoWidth": true,
		"processing": true,
		"order":[[0,"desc"]],
		'columnDefs': [
			{
				"targets": 3, // your case first column
				"className": "text-center",
				"width": "5%"
		   }],
		'ajax' : {						
			url: url,
			type: "POST",
			data: {'token_key':123,'web':'true'},
		},
		"columns": [{
			"data": "hari_id",
			"visible":false
		},{
			"data": "hari_nama"
		},{
			"data": "active",
			render: function ( data, type, row ) {
				if(data=="0"){
					return "<span style='font-weight: bold;color:red;'>Inactive</span>";
				}else{
					return "<span style='font-weight: bold;color:green;'>Active</span>";
				}
			}
		},
		{ "data": null,
			render: function ( data, type, row ) {
				var list_action = "<a id='edit_hari' href='#' class='btn btn-info btn-xs last' "+
					"data-toggle='modal' data-target='.edit' data-backdrop='static' "+
					"data-keyboard='false' style='margin-bottom:0px;float:left;'"+
					"data-hari_id='"+data.hari_id+"' data-hari_nama='"+data.hari_nama+"' data-active='"+data.active+"'> "+
					"<i class='fas fa-edit'></i> Edit</a> ";
				return list_action; 
			}
		}]
	});
}