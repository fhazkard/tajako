<div class="modal fade tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="">Add Hari</h4>
            </div>
            <div class="modal-body">
				<form id="form_tambah_hari" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Nama Hari:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="hari_add" class="form-control" placeholder="Input Nama Hari" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Add Hari</button>
			</div>
				</form>
		</div>
	</div>
</div>
				

				<!-- Modal untuk Edit -->
<div class="modal fade edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Edit Hari</h4>
            </div>
            <div class="modal-body">
				<form id="form_edit_hari" class="form-horizontal form-label-left">
					<input id="hari_id" type="hidden">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Nama Hari:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<input type="text" id="hari_edit" class="form-control" placeholder="Input Nama Hari" required autofocus maxlength=30 minlength=4 >
						</div>						
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Status:</label>
						<div class="col-md-5 col-sm-5 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select id="active" class="form-control">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>						
					</div>		
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
                </form>
        </div>
    </div>
</div>
		
<script src="modal.js"></script>