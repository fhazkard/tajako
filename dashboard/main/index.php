<?php 
  session_start(); 
  error_reporting(0);
  include "../header.php";
?>
<style>
        .product-imitation{
            padding: 60px 30px 60px 30px;
            font-size:50px;
        }
        .product-name{
            font-size:18px;
        }
        .text-muted{
            font-size:14px;
        }
        .label_new_order{
            font-size:20px;
            float:left;
            margin-left: 0px;
        }
        .btn_confirm{
            width: -webkit-fill-available;
        }
    </style> 

        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="font-bold">Welcome To Tajako Dashboard</h2>
                    <h3 id="teks_step" class="font-bold" style="font-style:italic"></h3>
                </div>
            </div>
            <hr/>
            
            <?php 
                if($role == "Customer"){?> 
                    <div class="row" id="row_paketorder"></div>
                    <div class="row" id="row_konsultan"></div>
                    <div class="row" id="row_jadwal">
                        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 animated bounceInDown '>
                            <div class='ibox'>
                                <div class='ibox-content product-box'>
                                    <button id='back_tokonsultan' class='btn btn-md btn-outline btn-warning'><i class='fas fa-arrow-left'></i> BACK</button>
                                    <div class='product-imitation' style='padding:0px 30px 30px 30px;font-size: large;'>
                                        <h2 style="font-weight:400;color:#23c6c8">Pilih Jadwal:</h2> 
                                        <select id="jadwal" class="form-control selek jadwal">
                                        </select>
                                    </div>
                                    <div class='text-righ' style='padding:30px'>
										<button id='order_now' class='btn btn-md btn-outline btn-primary btn_confirm'><i class="fas fa-shopping-cart"></i> ORDER NOW</button>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php }?>  
            <!-- row -->
            
                    <!-- <div class='col-lg-12 headerbar'>
                        <div class='animated bounceInUp'>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-primary float-right '>4 Seat Stock</span><br>
                                <h4 id='seatstock_4'>4 Seat</h4>
                            </div>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-primary float-right'>6 Seat Stock</span><br>
                                <h4 id='seatstock_6'>6 Seat </h4>
                            </div>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-primary float-right'>8 Seat Stock</span><br>
                                <h4 id='seatstock_8'>8 Seat</h4>
                            </div>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-info float-right'>4 Seat Duty</span><br>
                                <h4 id='seatduty_4'>4 Seat</h4>
                            </div>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-info float-right'>6 Seat Duty</span><br>
                                <h4 id='seatduty_6'>6 Seat</h4>
                            </div>
                            <div class='ibox-title col-md-2 widget_order'>
                                <span class='label label-info float-right'>8 Seat Duty</span><br>
                                <h4 id='seatduty_8'>8 Seat</h4>
                            </div>
                        </div>
                    </div> -->
                    
                    <!-- <div class='col-lg-6'>
                        <div class='ibox animated bounceInUp'>
                            <div class='col-md-6 ibox-content widget_order'>
                                <h1 class='no-margins' id='total_bill_rent'>Rp. 0</h1>
                                <div id="compare_total_bill_rent"></div>
                                <small>Total Billing</small>
                            </div>
                            <div class='col-md-6 ibox-content widget_order'>
                                <h1 class='no-margins' id='total_discount_rent'>Rp. 0</h1>
                                <div id="compare_total_discount_rent"></div>
                                <small>Total Discount</small>
                            </div>
                            
                        </div>
                    </div> -->

               <!--  <div class='col-lg-6'>
                    <div class='ibox animated bounceInUp'>
                        <div class='col-md-6 ibox-content widget_order'>
                            <h1 class='no-margins' id='total_order_progress_rent'>0</h1>
                            <div id="compare_total_order_progress_rent"></div>
                            <small>Total Order - On Progress</small>
                        </div>
                        <div class='col-md-6 ibox-content widget_order'>
                            <h1 class='no-margins' id='total_order_complete_rent'>0</h1>
                            <div id="compare_total_order_complete_rent"></div>
                            <small>Total Order - Completed</small>
                        </div>
                    </div>
                </div> -->

                <!-- Graph CBP RENT -->
                <!-- <div class='col-lg-12'>
                    <div class='ibox animated bounceInUp'>
                        <div id="graph_bill_rent"></div>
                    </div>
                </div>
                <div class='col-lg-12'>
                    <div class='ibox animated bounceInUp'>
                        <div id="graph_order_vehicle"></div>
                    </div>
                </div> -->
            

        <?php include "modal.php";?>
        </div><!-- right col --> 
      </div><!-- main container -->
    </div><!-- body container -->
  
        <script src="../../resource/js/foot-menu.js"></script>
        <script src="generate.js"></script>
        <script src="graph.js"></script> 	
    </body>
</html>
     