var path = null;
var paket_id = "";
var konsultan_id = "";
var step1 = "";
var step2 = "";

$(document).on("click", "#next_tokonsultan", function () {
	paket_id = $(this).data('paket_id');
	var nama = $(this).data('paket_name');
	step1 = "Paket : "+nama;
	$('#teks_step').html(step1);

	$('#row_paketorder').hide();
	$('#row_konsultan').show();
});

$(document).on("click", "#next_tojadwal", function () {
	konsultan_id = $(this).data('konsultan_id');
	var nama = $(this).data('nama');
	step2 = " / Konsultan: "+nama;
	$('#teks_step').html(step1+step2);

	load_jadwal(paket_id,konsultan_id);
	$('#row_konsultan').hide();
	$('#row_jadwal').show();
});

$(document).on("click", "#order_now", function (e) {
	e.preventDefault();
	order_now();
});

$(document).on("click", "#back_topaket", function () {
	paket_id = "";
	$('#teks_step').html("");

	$('#row_konsultan').hide();
	$('#row_paketorder').show();
});

$(document).on("click", "#back_tokonsultan", function () {
	konsultan_id = "";
	$('#teks_step').html(step1);

	$('#row_jadwal').hide();
	$('#row_konsultan').show();
});

$(document).ready(function(){
	path = getpath();
	Get_PaketOrder();
	Get_konsultan();

	$('#row_konsultan').hide();
	$('#row_jadwal').hide();
});

function Get_PaketOrder(){
	var url = path+"/system/_api/data_paket/select_paket.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				//console.log("Data Rent Order Null: ", err.message);
			}
			if(data.length != 0){
				$('.item_paket').remove();
				data.forEach(el => {
					var info = el.paket_info.split('@');
					var hot = "";
					if(el.paket_id == 3){
						hot = "<span class='label label-warning label_new_order'>Hot Offer</span>"
					}

					var panel_order = "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 animated bounceInUp item_paket'>"+
							"<div class='ibox'>"+
								"<div class='ibox-content product-box'>"+
									hot+
									"<div class='product-imitation'>"+el.paket_name+"</div>"+ 								
									"<div class='product-desc'>"+
										"<span class='product-price'>Rp. "+numeral(el.harga).format('0,0')+"</span>"+
										"<a href='#' class='product-name'>Durasi "+el.durasi+" Menit</a>"+ 
										"<a href='#' class='product-name'>"+info[1]+"</a>"+ 
										"<div class='m-t text-righ'>"+
											"<button id='next_tokonsultan' data-paket_id='"+el.paket_id+"' data-paket_name='"+el.paket_name+"' class='btn btn-md btn-outline btn-primary btn_confirm'><i class='fas fa-arrow-right'></i> NEXT</button>"+
										"</div>"+
									"</div>"+
								"</div>"+  
							"</div>"+
						"</div>";
						$('#row_paketorder').append(panel_order);		
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function Get_konsultan(){
	var url = path+"/system/_api/data_konsultan/select_konsultan.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123, 'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				//console.log("Data Rent Order Null: ", err.message);
			}
			if(data.length != 0){
				$('.item_konsultan').remove();
				data.forEach(el => {
					var info = el.info.split('@');

					var link = "http://localhost/tajako/resource/img/user.jpg";
					if(el.photo != ""){
						link = el.photo;
					}

					var detail = "";
					info.forEach(v =>{
						detail = detail+' - '+v+'<br>';
					});

					var panel_item = "<div class='col-lg-3 col-md-3 col-sm-4 col-xs-12 animated bounceInRight item_konsultan'>"+
							"<div class='ibox'>"+
								"<div class='ibox-content product-box'>"+
								"<button id='back_topaket' class='btn btn-md btn-outline btn-warning'><i class='fas fa-arrow-left'></i> BACK</button>"+
									"<div class='product-imitation' style='padding:30px 30px 60px 30px;'><img src='"+link+"' class='img-responsive img-circle img-small' alt='Consultant Image' style='display:inline;'></div>"+ 								
									"<div class='product-desc'>"+
										"<span class='product-price'>"+el.nama+"</span>"+
										"<a href='#' class='product-name' style='font-size:14px;'>"+detail+"</a>"+ 
										"<div class='m-t text-righ'>"+
											"<button id='next_tojadwal' data-konsultan_id='"+el.konsultan_id+"' data-nama='"+el.nama+"' class='btn btn-md btn-outline btn-primary btn_confirm'><i class='fas fa-arrow-right'></i> CONTINUE</button>"+
										"</div>"+
									"</div>"+
								"</div>"+  
							"</div>"+
						"</div>";
						$('#row_konsultan').append(panel_item);		
				});
			}else{
				//console.log("Data Rent Order Empty.: ", data);
			}
		}
	});
}

function load_jadwal(paket,konsultan){
	swal({
		title: "Request Order...",
		text: "Please wait for while...",
		imageUrl: "../../resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	$('.jadwal').empty().trigger("change");
	var url = path+"/system/_api/data_jadwal/select_jadwal.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','paket_id':paket,'konsultan_id':konsultan},
		success: function(msg) {
			try {
				data = JSON.parse(msg);
				var jadwal = $.map(data, function (obj) {
					var jam = obj.timestart.substring(0,2);
					var menit = obj.timestart.substring(2,4);
					var start = jam+":"+menit;

					var jam = obj.timeend.substring(0,2);
					var menit = obj.timeend.substring(2,4);
					var end = jam+":"+menit;
					obj.text = obj.hari_nama+", "+start+" - "+end; 
					obj.id = obj.jadwal_id;				  
					return obj;
				});

				$(".jadwal").select2({
					data: jadwal,
				});
			}
			  catch(err) {
				console.log("Data Jadwal Null: ", err.message);
			}
		}
	});
	swal.close();
}

function order_now(){
	swal({
		title: "Request Order...",
		text: "Please wait for while...",
		imageUrl: "../../resource/img/ajax-loader.gif",
		showConfirmButton: false,
		allowOutsideClick: false
	});
	var jadwal_id = $('#jadwal').val();
	var url = path+"/system/_api/data_order/reg_order.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true','jadwal':jadwal_id,'konsultan_id':konsultan_id,'paket_id':paket_id},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Order Null: ", err.message);
			}
			if(data.note == "Success"){
				swal({title: "Success",text: "Check Your Email (Delay 5 Minute), Please Make Payment Soon.",type: "success"}, 
						function() {window.location = window.location.href;
						});
			}else{
				swal({title: "Error",text: "Request Paket Order Failed - "+data.note,type: "error"}, 
						function() {window.location = window.location.href;
						});
				console.log("Error getting documents: ", data);
			}
		}
	});
}




