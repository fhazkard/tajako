var path = null;

$(document).ready(function(){
	path = getpath();
	//getGraphBill_rent();
	//getGraphOrder_rent();
});	

//generate data graph product
function genData(namalist) {
    var legendData = [];
    var seriesData = [];
    for (var i = 0; i < namalist.length; i++) {
        legendData.push(namalist[i]['nama']);
        seriesData.push({
            name: namalist[i]['nama'],
            value: namalist[i]['qty']
        });
    }

    return {
        legendData: legendData,
        seriesData: seriesData,
    };
}

//get data Bill Tenant CBP
function getGraphBill_rent(){
	var url = "/tajako/system/_api/main/get_graph_bills.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Bill Null: ", err.message);
			}
			if(data.length != 0){
				$('#graph_bill_rent').attr('style','width: 100%; height:400px; margin-top:10px;')
				graphBill_rent(data);
			}else{
				console.log("Data Bill Empty.: ", data);
			}
		}
	});
}

//Display data Bill Tenant CBP
function graphBill_rent(data){
	var graphbar = echarts.init(document.getElementById('graph_bill_rent'), theme);
	var Y = new Date();
	Y = Y.getFullYear();
	var series_category = [];
	var series_total = [];

	for(x=0;x < data.length; x++){
		series_category.push(data[x]['tgl']);
		series_total.push(data[x]['payment']);
	}

	graphbar.setOption({
		title: {
		  text: 'Last 30 Days Billing Rent',
		  subtext: 'Graph '+Y
		},
		tooltip: {
		  trigger: 'axis'
		  },
		/* legend: {
			data: series_category
		}, */
		toolbox: {
			show : true,
			feature : {
				dataView : {show: true, readOnly: false},
				magicType : {show: true, type: ['line', 'bar']},
				restore : {show: true},
				saveAsImage : {show: true}
			}
		},
		calculable: true,
		xAxis: [{
		  type: 'category',
		  data: series_category
		}],
		yAxis: [{
		type: 'value'
		}],
		series : [
			{
				name:'Billing',
				type:'bar',
				data: series_total,
				markPoint : {
					data : [
						{type : 'min', name: 'Payment'},
						{type : 'max', name: 'Payment'}
						
					]
				},
				markLine : {
					data : [
						{type : 'average', name: 'Avg. Payment'}
					]
				}
			}
		]	 
	});//setOption
}

//get data Product Tenant CBP
function getGraphOrder_rent(){
	var url = "/tajako/system/_api/main/get_graph_orders.php";
	$.ajax({
		type: "POST",
		url: url,
		data: {'token_key':123,'web':'true'},
		success: function(msg) {
			var data = [];
			try {
				data = JSON.parse(msg);
			}
			  catch(err) {
				console.log("Data Order Null: ", err.message);
			}
			if(data.length != 0){
				$('#graph_order_vehicle').attr('style','width: 100%; height:400px; margin-top:10px;')
				graphorder_rent(data);
			}else{
				console.log("Data Order Empty.: ", data);
			}
		}
	});
}

//Display data Product Tenant CBP
function graphorder_rent(list){
	var data = genData(list);
	var graphpie = echarts.init(document.getElementById('graph_order_vehicle'), theme);
	var d = new Date();
	d = d.getFullYear();
	
	graphpie.setOption({
		title: {
		  text: 'Most Ordering Vehicle',
		  subtext: 'Graph '+d
		},
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		toolbox: {
			show : true,
			feature : {
				dataView : {show: true, readOnly: false},
				restore : {show: true},
				saveAsImage : {show: true}
			}
		},
		legend: {
			type: 'scroll',
			orient: 'vertical',
			right: 10,
			top: 20,
			bottom: 20,
			data: data.legendData,
		},
		series : [
			{
				name: 'Vehicle',
				type: 'pie',
				radius : '55%',
				center: ['40%', '50%'],
				data: data.seriesData,
				itemStyle: {
					emphasis: {
						shadowBlur: 10,
						shadowOffsetX: 0,
						shadowColor: 'rgba(0, 0, 0, 0.5)'
					}
				}
			}
		]	 
	});
}


