<div class="modal fade confirm_order" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title" id="">Confirm Paket Order</h4>
            </div>
            <div class="modal-body">
				<form id="form_order_paket" method="POST" class="form-horizontal form-label-left">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" style="padding-left:0px;padding-right:0px;text-align:left;">Pilih Jadwal:</label>
						<div class="col-md-9 col-sm-9 col-xs-12"  style="padding-left:0px;padding-right:0px;">
							<select id="jadwal" class="form-control">
							</select>
						</div>						
					</div>					
            </div>
			<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" style="margin:0px;">Exit</button>
						<button type="submit" class="btn btn-success">Order Paket</button>
			</div>
                </form>
        </div>
    </div>
</div>
