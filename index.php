<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TAJAKO - Konsultan Hukum Batam</title>
    <script src="resource/js/head-webpage.js"></script>
    <link href="resource/css/sweetalert.css" rel="stylesheet">
    <style>
    .features-block {
        margin-top: 10px;
    }
    .article_teks p{
        color: #525252;
        font-size: 14px;
    }
    </style>
</head>
<body id="page-top">
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top">TAJAKO</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="#page-top">Home</a></li>
                        <li><a class="page-scroll" href="#features">News</a></li>
                        <li><a class="page-scroll" href="#team">About Us</a></li>
                        <li><a class="page-scroll" href="#pricing">Paket</a></li>
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                        <li><a href="faq.php">FAQ</a></li>         
                        <li><a href="login.php">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
</div>

<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
        <li data-target="#inSlider" data-slide-to="2"></li>
    </ol>
    <div id="row_slide" class="carousel-inner" role="listbox">      
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<section id="features" class="container features">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>News & Article</h1>
        </div>
    </div>
    <div id="row_article">

    </div>
</section>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Visi</h1>
                <p>Menjadi media konsultasi terbaik di Indonesia yang berbasis teknologi.</p>
                <h1>Misi</h1>
                <p>1. Memberikan edukasi kepada masyarakat tentang hukum di Indonesia.</p>
                <p>2. Memberikan fasilitas konsultasi hukum kepada masyarakat dalam memahami hukum, dengan mempertemukan konsultan dengan masyarakat.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 wow fadeInLeft">
                <div class="team-member">
                    <img src="resource/img/konsultan1.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Florianus Yudhi </span> Priyo Amboro</h4>
                    <p>Co Founder Of Tajako.com</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 wow fadeInRight">
                <div class="team-member">
                    <img src="resource/img/konsultan2.jpg" class="img-responsive img-circle img-small" alt="">
                    <h4><span class="navy">Agustianto</span></h4>
                    <p>Founder Of Tajako.com</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/agustianto.wang"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li><a href="https://www.linkedin.com/in/agustianto-wang-63596a6a"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/agustianto.wang/"><i class="fab fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="comments gray-section" style="margin-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Prakata</h1>
                <p>Salam Sejahtera untuk kita semua, semoga Tuhan selalu memberkati kita semua, Amin. Pertama-tama ijinkanlah kami menyampaikan beberapa kata untuk memperkenalkan Web Tajako.com yang baru kami luncurkan untuk membantu masyarakat dalam memberikan saran dan pendapat-pendapat hukum.</p> 
                <p>Tajako.com merupakan web yang akan memberikan para pembaca dan pengakses untuk mendapatkan informasi-informasi yang berkaitan dengan hukum ataupun permasalahan yang membutuhkan kajian hukum. Web Tajako.com diharapkan dapat menjadi salah satu solusi bagi masyarakat jika membutuhkan suatu pendapat hukum dalam penyelesaian permasalahan yang terjadi.</p>
                <p>Konsultan hukum yang tersedia pada layanan konsultasi tajako.com merupakan orang-orang yang berkompeten pada bidangnya masing-masing. Masyarakat yang ingin mengakses berita-berita tentang hukum ataupun ingin menggunakan layanan konsultasi hukum, tentu saja dapat dengan tenang dan tidak perlu khawatir akan kualitas yang disediakan oleh tajako.com.</p> 
                <p>Kami para pendiri Tajako.com pada akhirnya mengucapkan banyak terima kasih atas dukungan seluruh pihak hingga Tajako.com resmi kami luncurkan dan hadir ditengah masyarakat yang modern ini. Semoga Tajako.com dapat menjadi inspirasi dan solusi bagi masyarakat di Indonesia maupun Internasional.</p>
                <p>Atas perhatiannya kami ucapkan banyak terima kasih. Salam sukses dan sejahtera.</p>
                <p>Best Regards, Pendiri Tajako.com</p>
            </div>
        </div>
    </div>

</section>

<section id="konsultan" class="gray-section team" style="margin-top: 10px;">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Top Consultant of The Month</h1>
            </div>
        </div>
        <div id="list_konsultan" class="row">
        </div>
    </div>
</section>

<section id="pricing" class="pricing">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Paket Konsultasi</h1>
                <p>Untuk pertanyaan yang sudah masuk ke dalam pembahasan subtantif dan solusi penyelesaian dengan dasar hukum.</p>
            </div>
        </div>
        <div id="row_paket" class="row">
        </div>
        <div class="row m-t-lg">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">
                <p><span class="navy">*Catatan:</span><br/>1. Jika pihak Konsultan Hukum lama membalas dan lewat waktu batas konsultasi, maka untuk paket 1 dan 2, tetap akan dipenuhi jumlah pertanyaan tersebut atau tetap akan diberikan jawaban atas pertanyaan tersebut.</p>
                <p>2. Jika pihak Konsultan Hukum lama membalas dan lewat waktu batas konsultasi, maka untuk paket 3, Klien bisa memberikan maksimal 7 pertanyaan yang akan dijawab oleh Konsultan Hukum.</p>
            </div>
        </div>
    </div>
   
    <?php include "modal.php";?>

</section>

<section id="contact" class="">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                <p>Silahkan Kontak Dan Datang Ke Kantor Kami, Dibawah ini:</p>
            </div>
        </div>
        <div id="row_kontak" class="row m-b-lg">        
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:support@tajako.com" class="btn btn-primary">Send us mail</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; 2019 Tajako.com</strong><br/> Konsultan Hukum Batam.</p>
            </div>
        </div>
    </div>
</section>

<script src="resource/js/foot-webpage.js"></script>
<script src="generate.js"></script>
</body>
</html>
