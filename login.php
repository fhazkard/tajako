<?php 
	session_start();
	error_reporting(0);
    require 'system/_core/csrf.php';
    if(isset($_SESSION['limit'])&& isset($_SESSION['user_id'])){
        $menus = $_SESSION['menu'];
        $link = $menus[0]->menu_url;
	    echo "<script>window.location = 'dashboard/".$link."/'</script>";	
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta, title, favicons -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tajako - Login Page</title>
  <link rel="icon" href="resource/img/favicon_cbp.png">
  <script src="resource/js/head-login.js"></script>
    <link href="resource/css/sweetalert.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Modak|Montserrat+Alternates:700" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>LOGIN</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">
                    <strong>Login Page</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <a href="#" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Register Account</a>
            </div>
        </div>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name" style="font-family: 'Montserrat Alternates', sans-serif;font-size:40px;letter-spacing: 0px; color:#1ab394;">TAJAKO</h1>
            </div>
			<p>Login Your Account!</p>
            <form class="m-t" role="form" method="POST" action="system/_core/auth.php">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" autofocus required maxlength="16" minlength="4" onblur="this.value=removeSpaces(this.value);">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required maxlength="16" minlength="4">
                </div>
                <!-- <div class="form-group" style="text-align:left;font-size:medium;">
                    <input type="checkbox" name="relogin" value="yes"> Gunakan Relogin!
                </div> -->
				<?php CSRF::init(); CSRF::tokenInput(); ?>
                <button class="btn btn-primary block full-width m-b"><i class="fas fa-sign-in-alt"></i> Login</button>
            </form>
				<p class="m-t"> <small>Fhaz Framework &copy; 2019 -  Build By Fhazkard.</small> </p>
        </div>
    </div>
    <?php include "modal.php";?>
    <script src="resource/js/foot-login.js"></script>
    <script src="resource/js/function.js"></script>
    <script src="generate.js" type="text/javascript"></script>
</body>
</html>
